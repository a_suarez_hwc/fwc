{{-- TODO: replace all major content with language/en calls to make language transitions easier --}}
<!DOCTYPE html>
<html>
<head>
	{{-- TODO: see what meta tags would be appropriate here --}}
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<title>FreeWorkers' Community</title>

	<link rel="shortcut icon" href="{{{ asset('img/favicon.ico') }}}">

	{{-- TODO: decide whether to pull assets from CDN or serve them from server --}}
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" />
	{{ Html::style('css/app.css') }}

	@yield('head')

	{{ Html::script('js/all.js') }}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.ja.min.js"></script>

</head>
<body>
	{{-- Navigation --}}
	@include('site.partials.nav-main')

	<main class="site-main site-main--subnav">
		<div class="container-fluid">
			<div class="row">
				@if(session('ok'))
				@include('site.partials.alert', ['type' => 'success', 'message' => session('ok')])
				@endif

				<div class="col-md-10 col-md-offset-1">
					@if(isset($errors) && count($errors)>0)
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
				</div>

				@yield('content')
			</div>

			@include('site.partials.scrolltop')
		</div>
	</main>

</body>
</html>
