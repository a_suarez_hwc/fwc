<html>
<head></head>
<body>
	<h3>{{$post->user->family_name_kanji}}様</h3>
	<p>
		Your teacher {{$post->teacher->user->family_name_kana}}先生 has submitted a solution to your post <a href="{{ url('posts/'.$post->post_id) }}">"{{$post->title}}"</a>
	</p>
	<p>
		Please review the solution and accept it through your <a href="{{ url('users/'.$post->user->user_id.'/questions') }}">My Questions</a> page or contact {{$post->teacher->user->family_name_kana}}先生 to modify the solution.
	</p>
	<p>
		If there are any issues please report them at 【ＴＯＤＯ】
	</p>
</body>
</html>