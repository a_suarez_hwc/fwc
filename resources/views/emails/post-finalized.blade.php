<html>
<head>
</head>
<body>
	@if($student)
	<h3>{{$post->user->family_name_kanji}}様</h3>
	<p>
		Your post <a href="{{ url('posts/'.$post->post_id) }}">"{{$post->title}}"</a> has been finalized and can no longer be edited.
	</p>
	<p>
		{{$post->price}} points have been deducted from your wallet. To view your receipt please visit your <a href="{{ url('wallets/'.$post->user->user_id) }}">Wallet</a>.
	</p>
	<p>
		Thank you for utilizing the forum!
	</p>
	@else
	<h3>{{$post->teacher->user->family_name_kanji}}様</h3>
	<p>
		Your lesson <a href="{{ url('posts/'.$post->post_id) }}">"{{$post->title}}"</a> has concluded and can no longer be edited.
	</p>
	<p>
		{{.9*$post->price}} points have been added to your wallet. To view your receipt please visit your <a href="{{ url('wallets/'.$post->teacher->user->user_id) }}">Wallet</a>.
	</p>
	<p>
		Thank you for teaching through the forum!
	</p>
	@endif
</body>
</html>