<html>
<head></head>
<body>
	<h3>{{$post->teacher->user->family_name_kanji}}様</h3>
	<p>
		Your student {{$post->user->family_name_kana}}さん has requested a solution to his post <a href="{{ url('posts/'.$post->post_id) }}">"{{$post->title}}"</a>. This signifies that the lesson is over.
	</p>
	<p>
		If you believe this to be in error, please contact {{$post->user->family_name_kana}}さん. Otherwise, please submit a solution through your <a href="{{ url('teachers/'.$post->teacher->teacher_id.'/lessons')}}">My Lessons</a> page.
	</p>
</body>
</html>