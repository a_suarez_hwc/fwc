<html>
<head></head>
<body>
	<h3>{{$post->teacher->user->family_name_kanji}}様</h3>
	<p>
		{{$post->user->family_name_kana}}さん has selected you as the teacher for the post 
		<a href="{{ url('posts/'.$post->post_id) }}">"{{$post->title}}"</a>
		.
	</p>
	<p>
		The price has been decided to be {{$post->price}} points.
	<p>
		Please contact {{$post->user->family_name_kana}}さん by one of the following means to arange the details of the lesson.
	</p>
	<ul>
		<li>Email : {{$post->user->contact->email}}</li>
		<li>Skype : {{$post->user->contact->skype}}</li>
	</ul>
	</p>
	<p>
		If there are any issues please report them at 【ＴＯＤＯ】
	</p>
</body>
</html>