<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title>FWC Admin</title>
    <link rel="shortcut icon" href="{{{ asset('img/favicon.ico') }}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
    @yield('styles')
    {{ Html::style('css/admin.css') }}
    {{ Html::script('js/all.js') }}
</head>
<body>
    @include('admin.partials.header')
    <main class="admin">
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </main>
    @include('admin.partials.footer')
    @yield('scripts')
</body>
</html>