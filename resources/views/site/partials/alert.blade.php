<div class="container-fluid"><br>
    <div class="alert alert-{{ $type }} alert-dismissable col-md-8 col-md-offset-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ $message }}
    </div>
</div>