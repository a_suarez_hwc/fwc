<div class="site-side">
	<div class="site-side__logo">
		<a class="site-side__a" href="{{ url('/') }}">FWC</a>
	</div>
	<nav>
		<a href="#" class="nav-main__toggle" id="js-nav-main-toggle"><i class="fa fa-fw fa-bars"></i> Menu</a>
		<ul class="nav nav-main">
			<li class="nav-main__item">
				<a class="nav-main__a {{ classActiveSegment(1,['posts','wallets'],'nav-main__a') }} {{ classActiveSegment(3,['questions','lessons'],'nav-main__a') }}" href="{{ url('/posts') }}">
					<i class="fa fa-fw fa-list-ul"></i>
					@lang('site.forum')
				</a>
			</li>
			<li class="nav-main__item">
				<a class="nav-main__a {{ classActiveSegment(1,'chat','nav-main__a') }}" href="{{ url('/soon') }}">
					<i class="fa fa-fw fa-comments"></i>
					@lang('site.chat')
				</a>
			</li>
			<li class="nav-main__item">
				<a class="nav-main__a {{ classActiveSegment(1,'events','nav-main__a') }}" href="{{ url('/soon') }}">
					<i class="fa fa-fw fa-calendar"></i>
					@lang('site.events')
				</a>
			</li>
			<li class="nav-main__item">
				<a class="nav-main__a {{ classActiveSegment(1,'something','nav-main__a') }}" href="{{ url('/soon') }}">
					<i class="fa fa-fw fa-users"></i>
					@lang('site.people')
				</a>
			</li>
			@if(Auth::user())
			<li class="nav-main__divider"></li>
			<li class="nav-main__item">
				@if(Auth::user())
				<a class="nav-main__a  {{ classActiveSegment(1,['users','teachers'],'nav-main__a', 3) }}" href="{{ url('/users/'.Auth::user()->user_id) }}">
					<i class="fa fa-fw fa-user-circle-o"></i>
					@lang('site.profile')
				</a>
				@else
				<a class="nav-main__a {{ classActiveSegment(1,'profile','nav-main__a') }}" href="{{ url('/login') }}">
					<i class="fa fa-fw fa-user-circle-o"></i>
					@lang('site.profile')
				</a>
				@endif
			</li>
			<li class="nav-main__item">
				<a class="nav-main__a {{ classActiveSegment(1,'settings','nav-main__a') }}" href="{{ url('/soon') }}">
					<i class="fa fa-fw fa-cog"></i>
					@lang('site.settings')
				</a>
			</li>
			@if(Auth::user() && is_admin(Auth::user()))
			<li class="nav-main__item">
				<a href="{{ url('admin') }}" class="nav-main__a">
					<i class="fa fa-fw fa-dashboard"></i> @lang('site.admin')
				</a>
			</li>
			@endif
			@endif
			<li class="nav-main__divider"></li>
			<li class="nav-main__item">
				@if(Auth::user())
				<a class="nav-main__a {{ classActiveSegment(1,'login','nav-main__a') }}" href="{{ url('/logout') }}">
					<i class="fa fa-fw fa-sign-out"></i>
					@lang('site.logout')
				</a>
				<div class="nav-main__user text-center">
					{{ Auth::user()->user->name }}
				</div>
				@else
				<a class="nav-main__a {{ classActiveSegment(1,'login','nav-main__a') }}" href="{{ url('/login') }}">
					<i class="fa fa-fw fa-sign-in"></i>
					@lang('site.login')
				</a>
				@endif
			</li>
		</ul>
	</nav>
</div>