<nav class="sub-nav navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<ul class="nav navbar-nav">
			<li class="sub-nav__item {{ classActiveSegment(1, 'posts') }}">
				<a class="sub-nav__anchor" href="{{ url('posts') }}"><i class="fa fa-list-ul"></i> @lang('site.forum')</a>
			</li>
			<li class="sub-nav__item {{ classActiveSegment(1, 'users') }}">
				@if(Auth::user()!==null)
				<a class="sub-nav__anchor" href="{{ url('/users/'.Auth::user()->user_id.'/questions') }}"><i class="fa fa-question-circle"></i> @lang('site.my_questions')</a>
				@else
				<a class="sub-nav__anchor" href="{{ url('/login') }}"><i class="fa fa-question-circle"></i>@lang('site.my_questions')</a>
				@endif
			</li>
			@if(Auth::user()!==null && Auth::user()->user->teacher)
			<li class="sub-nav__item {{ classActiveSegment(1,'teachers') }}">
				<a class="sub-nav__anchor" href="{{ url('/teachers/'.Auth::user()->user->teacher->teacher_id.'/lessons') }}"><i class="fa fa-book"></i> @lang('site.my_lessons')</a>
			</li>
			@endif
			<li class="sub-nav__item {{ classActiveSegment(1,'wallets') }}">
				@if(Auth::user()!==null)
				<a class="sub-nav__anchor" href="{{ url('/wallets/'.Auth::user()->user->wallet->wallet_id) }}"><i class="fa fa-yen"></i> @lang('site.wallet')</a>
				@else
				<a class="sub-nav__anchor" href="{{ url('/login') }}"><i class="fa fa-yen"></i> @lang('site.wallet')</a>
				@endif
			</li>
			<li class="sub-nav__item">
				<a href="{{ url('/about') }}" class="sub-nav__anchor"><i class="fa fa-info-circle"></i> @lang('site.about')</a>
			</li>
		</ul>
		@if(Auth::user()!==null)
		<ul class="nav navbar-nav navbar-right">
			<li>
				<a class="nav-wallet" href="{{ url('/wallets/'.Auth::user()->user->wallet->wallet_id) }}">
					{{ Auth::user()->user->wallet->points }}P
				</a>
			</li>
		</ul>
		@endif
	</div>
</nav>