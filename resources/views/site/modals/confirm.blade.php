<button class="btn {{ $class or 'btn-default' }}" id="js-new-question-btn" type="button" data-toggle="modal" data-target="#confirm-modal-{{ str_replace("/", "-", $action) }}">
	@if(!empty($icon))
	<i class="fa fa-{{ $icon }}"></i>
	@endif
	{{ $button_text or 'confirm' }}
</button>

<div class="modal fade" id="confirm-modal-{{ str_replace("/", "-", $action) }}" tabindex="-1" role="dialog" aria-labelledby="confirm-modal-label">
	<div class="modal-dialog" role="document">
		<div class="panel panel-default">
			<div class="panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="new-question-modal-label">Confirm Action</h4>
			</div>
			<div class="panel-body">
				<p>{{ $message or 'Are you sure you want to do that?' }}</p>

				<form action="{{ url($action) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field($method) }}
					<button class="btn {{ $class or 'btn-default' }}" type="submit">
						@if(!empty($icon))
						<i class="fa fa-{{ $icon }}"></i>
						@endif
						{{ $button_text or 'confirm' }}
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
						{{ $cancel_text or 'Cancel' }}
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
