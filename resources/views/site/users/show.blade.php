@extends('site')

@section('content')
<div class="col-md-10 col-md-offset-1">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header">{{ $user->family_name_kanji.$user->first_name_kanji }}</h2>
		</div>
		<div class="col-md-3">
			<div class="text-center">
				<img src="{{ asset($user->profile_image_path) }}" alt="" class="user-profile__img">
			</div>
			<div class="list-group">
				@can('access-user-profile', $user)
				@if($user->teacher)
				<a class="list-group-item" href="{{ url('/teachers/'.$user->teacher->teacher_id) }}">
					<i class="fa fa-user"></i>
					@lang('site.teacher') @lang('site.profile')
				</a>
				@endif
				<a class="list-group-item" href="{{ url('/users/'.$user->user_id.'/edit') }}">
					<i class="fa fa-pencil"></i> Edit @lang('site.profile')
				</a>
				@else
				<button class="btn btn-primary"><i class="fa fa-envelope"></i> Send Email</button>
				<button class="btn btn-disabled"><i class="fa fa-comment"></i> Send Message</button>
				<button class="btn btn-disabled"><i class="fa fa-video-camera"></i> Video Chat</button>
				@endcan
			</div>
		</div>

		<div class="col-md-9">
			<table class="table">
				<tbody>
					<tr>
						<th>@lang('site.name')</th>
						<td>
							{{ $user->family_name_kanji.$user->first_name_kanji }}
						</td>
					</tr>
					<tr>
						<th>@lang('site.name_kana')</th>
						<td>
							{{ $user->family_name_kana.$user->first_name_kana }}
						</td>
					</tr>
					<tr>
						<th>@lang('site.login_email')</th>
						<td>
							{{ $user->login_credential->email}}
						</td>
					</tr>
					<tr>
						<th>@lang('site.email')</th>
						<td>
							{{ $user->contact->email }}
						</td>
					</tr>
					<tr>
						<th>@lang('site.mobile_email')</th>
						<td>
							{{ $user->contact->mobile_email }}
						</td>
					</tr>
					<tr>
						<th>@lang('site.phone_number')</th>
						<td>
							{{ $user->contact->phone_number }}
						</td>
					</tr>
					<tr>
						<th>@lang('site.skype')</th>
						<td>
							{{ $user->contact->skype }}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
@stop