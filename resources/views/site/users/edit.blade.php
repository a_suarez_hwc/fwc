@extends('site')

@section('content')
<div class="col-md-10 col-md-offset-1">
	<h2 class="page-header">Editing User @lang('site.profile')</h2>
	<div class="row">
		<div class="col-md-3 text-center">
			<img src="{{ asset($user->profile_image_path) }}" alt="" class="user-profile__img">
		</div>

		<div class="col-md-9">
			{!! Form::model($user, array('route' => array('users.update', $user->user_id), 'method'=>'PUT', 'files'=>true)) !!}
			<div class="form-group col-md-6">
				{!! Form::label('family_name_kanji', trans('site.family_name_kanji')) !!}
				{!! Form::text('family_name_kanji', null, array('class' => 'form-control')) !!}
			</div>
			<div class="form-group col-md-6">
				{!! Form::label('first_name_kanji', trans('site.first_name_kanji')) !!}
				{!! Form::text('first_name_kanji', null, array('class' => 'form-control')) !!}
			</div>
			<div class="form-group col-md-6">
				{!! Form::label('family_name_kana', trans('site.family_name_kana')) !!}
				{!! Form::text('family_name_kana', null, array('class' => 'form-control')) !!}
			</div>
			<div class="form-group col-md-6">
				{!! Form::label('first_name_kana', trans('site.first_name_kana')) !!}
				{!! Form::text('first_name_kana', null, array('class' => 'form-control')) !!}
			</div>

			<div class="col-md-12">
				<div class="form-group">
					{!! Form::label('email', trans('site.login_email')) !!}
					{!! Form::text('email', isset($user->login_credential->email) ? $user->login_credential->email : null, array('class' => 'form-control', 'readonly')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('email', trans('site.email')) !!}
					{!! Form::text('email', isset($user->contact->email) ? $user->contact->email : null, array('class' => 'form-control')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('mobile_email', trans('site.mobile_email')) !!}
					{!! Form::text('mobile_email', isset($user->contact->mobile_email) ? $user->contact->mobile_email : null, array('class' => 'form-control')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('phone_number', trans('site.phone_number')) !!}
					{!! Form::text('phone_number', isset($user->contact->phone_number) ? $user->contact->phone_number : null, array('class' => 'form-control')) !!}
				</div>

				<div class="form-group">
					{!! Form::label('skype', trans('site.skype')) !!}
					{!! Form::text('skype', isset($user->contact->skype) ? $user->contact->skype : null, array('class' => 'form-control')) !!}
				</div>

				{{-- TODO: add image options --}}
				<div class="form-group">
					{!! Form::label('profile_image_path', trans('site.profile_image_path')) !!}
					{!! Form::file('profile_image_path') !!}
				</div>

				{!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
				<a href="{{ url('/users/'.$user->user_id) }}" class="btn btn-default">@lang('actions.cancel')</a>
			</div>
		</div>
	</div>

	{!! Form::close() !!}
</div>
@endsection