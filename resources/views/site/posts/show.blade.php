@extends('site')

@section('content')
@include('site.partials.nav-forum')
<div class="col-md-10 col-md-offset-1">
    <h1 class="page-header post__title">
        {{ $post->title }}
    </h1>
    <div class="row">
        @include('site.posts.partials.post', [
            'post'=>$post,
            'user'=>$post->user,
            'teacher'=>$teacher,
            'status'=>$post->postStatus,
            'tags'=>$post->tags,
            ])
    </div>
</div>
@stop
