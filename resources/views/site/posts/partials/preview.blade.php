<div class="post-preview">
	<div class="post-preview__header">
		<h2 class="post-preview__title">
			<a href="{{ url('posts/'.$post->post_id) }}">{{ $post->title }}</a>
		</h2>
		<div class="post-preview__status">
			@if($post->post_status_id === 1 || $post->post_status_id === 3)
			<div class="post__status post__status--{{ str_replace(" ","-",strtolower($post->post_status->post_status_text)) }}">
				<i class="fa fa-circle"></i>
			</div>
			@else
			<div class="post__status post__status--in-progress">
				<i class="fa fa-circle"></i>
			</div>
			@endif
		</div>
		<div class="post-preview__user">
			<?php
			\Carbon\Carbon::setLocale('ja');
			echo \Carbon\Carbon::parse($post->created_at)->diffForHumans();
			?>
			by
			<span class="post-preview__user-name">{{ $user->family_name_kanji.$user->first_name_kanji }}</span>
			@if(!empty($teacher))
			<div class="post-preview__teacher">
				<span class="post-preview__teacher-name">
					| <a href="#/" data-toggle="popover" data-placement="bottom" title="{{ $teacher->user->family_name_kanji.$teacher->user->first_name_kanji }}" data-trigger="focus" data-html="true" data-content="@include('site.teachers.partials.popover',['teacher'=>$teacher])" class="a__popover post__teacher-name">
					<i class="fa fa-graduation-cap"></i> {{ $teacher->user->family_name_kanji.$teacher->user->first_name_kanji }}
				</a>
			</span>
		</div>
		@endif
	</div>
</div>
<div class="post-preview__body">
	<ul class="tags">
		@foreach ($tags as $tag)
		<li class="tag">{{ $tag->tag_text }}</li>
		@endforeach
	</ul>
</div>
@if(!isset($show_actions) or $show_actions)
<div class="post-preview__footer cf">
	{{-- Delete Post --}}
	@can('modify-open-post', $post)
	@include('site.modals.confirm', ['action'=>'/users/'.$user->user_id.'/questions/'.$post->post_id, 'method'=>'delete', 'button_text'=>'Delete', 'class'=>'btn-danger btn-sm right', 'icon'=>'trash'])
	@endcan

	{{-- Remove Teacher Application --}}
	@if(!empty($isApplication) && $isApplication)
	@include('site.modals.confirm', ['action'=>'/teachers/'.Auth::user()->user->teacher->teacher_id.'/lessons/'.$post->post_id, 'method'=>'delete', 'button_text'=>'Remove Application', 'class'=>'btn-danger btn-sm right', 'icon'=>'trash'])
	@endif

	{{-- Adjust Price ----- include (modal) method: update price field in post tbl --}}
            {{--
            @if(!empty($isApplication) && $isApplication)
            @include('site.modals.confirm', ['action'=>'/teachers/'.Auth::user()->user->teacher->teacher_id.'/lessons/'.$post->post_id, 'method'=>'', 'button_text'=>'Adjust Price', 'class'=>'btn-info btn-sm right', 'icon'=>'shopping-cart'])
            @endif
            --}}

            {{-- Request Solution from Teacher --}}
            @if($post->post_status_id === 2 && !empty(Auth::user()) && Auth::user()->user_id === $post->user_id)
            @include('site.modals.confirm', ['action'=>'/users/'.$user->user_id.'/questions/'.$post->post_id.'/request', 'method'=>'POST', 'button_text'=>'Request Solution', 'class'=>'btn-info btn-sm right'])
            @endif

            {{-- View/Adjust Price --}}
            @if(isTeacher(Auth::user()) && $post->post_status_id === 1)
            @if(!empty(Auth::user()->user->teacher->applied_posts->where('post_id', $post->post_id)->first()))
            {{ Auth::user()->user->teacher->applied_posts->where('post_id', $post->post_id)->first()->pivot->quoted_price }}P
            @include('site.posts.modals.adjust-price', ['current_price'=>Auth::user()->user->teacher->applied_posts->where('post_id', $post->post_id)->first()->pivot->quoted_price, 'teacher'=>Auth::user()->user->teacher])
            <button class="btn btn-default btn-sm right" type="button" data-toggle="modal" data-target="#adjust-price-modal-{{ $post->post_id }}">Adjust Price</button>
            @endif
            @endif

            {{-- Add/Edit Solution --}}
            @can('update-solution', $post)
            @if(isTeacher(Auth::user()))
            <a href="{{url('/teachers/'.$post->teacher->teacher_id.'/lessons/'.$post->post_id.'/edit')}}" class="btn btn-info btn-sm right">Add/Edit Solution</a>
            @else
            <a href="{{url('/users/'.Auth::user()->user_id.'/questions/'.$post->post_id.'/solution')}}" class="btn btn-info btn-sm right">Edit Solution</a>
            @endif
            @endcan

            {{-- Accept Solution and Review Teacher --}}
            @if($post->post_status_id === 5 && !empty(Auth::user()) && Auth::user()->user_id === $post->user_id)
            @include('site.teachers.modals.review', ['action'=>'/users/'.$user->user_id.'/questions/'.$post->post_id.'/accept', 'method'=>'POST', 'button_text'=>'Accept Solution', 'class'=>'btn-info btn-sm right', 'solution'=>$post->solution])
            @endif

            {{-- Toggle Post Applicants --}}
            @if(!empty($applicants) && count($applicants)!==0)
            <a class="btn btn-primary btn-sm right" role="button" data-toggle="collapse" href="#collapse-{{ $post->post_id }}" aria-expanded="false" aria-controls="collapse-{{ $post->post_id }}">
            	Toggle Requests <i class="fa fa-angle-down fa-lg"></i>
            </a>
        </div>

        <div class="collapse" id="collapse-{{ $post->post_id }}">
        	<br>
        	<ul class="list-group">
        		@foreach ($applicants as $applicant)
        		<li class="list-group-item cf">
        			{{ Html::image($applicant->user->profile_image_thumbnail_path) }}
        			{{ $applicant->user->family_name_kanji.$applicant->user->first_name_kanji }}
        			<div class="right">
        				{{-- TODO: feed the quoted price from the controller as a variable --}}
        				{{ $applicant->user->teacher->applied_posts->where('post_id', $post->post_id)->first()->pivot->quoted_price }}P
        				<a class="btn btn-default btn-sm" href="{{ url('/teachers/'.$applicant->teacher_id) }}">View @lang('site.profile')</a>
        				<form action="{{ url('/users/'.$post->user_id.'/questions/'.$post->post_id) }}" class="inline" method="POST">
        					{{ csrf_field() }}
        					<input type="hidden"  name="teacher_id" value="{{ $applicant->teacher_id }}">
        					<input type="hidden" name="price" value="{{ $applicant->user->teacher->applied_posts->where('post_id', $post->post_id)->first()->pivot->quoted_price }}">
        					<input class="btn btn-primary btn-sm" type="submit" name="apply" value="Accept">
        					{{-- TODO make other things happen --}}
        				</form>
        			</div>
        		</li>
        		@endforeach
        	</ul>
        </div>
        @else
    </div>
    @endif
    @endif

</div>