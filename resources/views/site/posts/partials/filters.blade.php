<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	@foreach($groups as $i=>$group)
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading{{$i}}">
			<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}{{ $namespace or '' }}" aria-expanded="true" aria-controls="collapse{{$i}}{{ $namespace or '' }}">
				{{$group->tag_group_text}}
			</a>
		</div>
		<div id="collapse{{$i}}{{ $namespace or '' }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$i}}">
			<div class="panel-body">
				@foreach($group->tags as $tag) 
				<div class="checkbox">
					<label class="checkbox__label">
					<input type="checkbox" name="tags[{{$tag->tag_id}}]" value="{{$tag->tag_id}}" @if((isset(Input::get('tags')[$tag->tag_id]) && Input::get('tags')[$tag->tag_id]===$tag->tag_id) || (!empty($post) && $post->tags->find($tag->tag_id))) checked @endif>
						{{$tag->tag_text}}
					</label>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	@endforeach
</div>