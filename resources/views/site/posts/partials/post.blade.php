<div class="col-md-8">
	<div class="post__main">
		<p class="post__body">
			{!! $post->body !!}
		</p>
		<ul class="tags cf">
			@foreach($tags as $tag)
			<li class="tag">{{ $tag->tag_text }}</li>
			@endforeach
		</ul>
	</div>
	@can('view-solution', $post)
	<div class="post__solution">
		<h4>@lang('site.solution')</h4>
		<p>
			{!! $post->solution !!}
		</p>
	</div>
	@endcan
</div>
<div class="col-md-4">
	<div class="post__info cf">
		<div class="post__user">
			{{ Html::image($user->profile_image_thumbnail_path) }}
			<span class="post__user-name">{{ $user->family_name_kanji.$user->first_name_kanji }}</span>
		</div>
		<div class="post__status post__status--{{ str_replace(" ","-",strtolower($post->post_status->post_status_text)) }}">
			<i class="fa fa-circle"> </i>
			@if(Auth::user() && Auth::user()->user->teacher != null && Auth::user()->user->teacher->applied_posts->contains($post->post_id))
			Applied
			@else
			{{ $post->post_status->post_status_text }}
			@endif
		</div>

		@if($teacher)
		<div class="post__teacher">
			{{-- TODO: link teacher profile data --}}
			<a href="#/" data-toggle="popover" data-placement="bottom" title="{{ $teacher->user->family_name_kanji.$teacher->user->first_name_kanji }}" data-trigger="focus" data-html="true" data-content="@include('site.teachers.partials.popover',['teacher'=>$teacher])" class="a__popover post__teacher-name">
				<i class="fa fa-graduation-cap"></i>
				{{ $teacher->user->family_name_kanji.$teacher->user->first_name_kanji }}
			</a>
		</div>
		@endif
		@can('update', $post)
		<a href="{{ url('/posts/'.$post->post_id.'/edit') }}" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Edit Post</a>
		@endcan

		@can('cancel-lesson', $post)
		@include('site.modals.confirm', ['action'=>'/posts/'.$post->post_id.'/cancel','method'=>'post','button_text'=>'Cancel Lesson','cancel_text'=>'Nevermind','class'=>'btn-danger btn-sm','icon'=>''])
		@endcan

		@can('apply-post', $post)
		@if(!is_null(Auth::user()->user->teacher))
		@if(!is_null(Auth::user()->user->teacher) && Auth::user()->user->teacher->applied_posts->contains($post->post_id))
		{!! Form::open(array('route' => array('teachers.lessons.destroy', Auth::user()->user->teacher->teacher_id, $post->post_id), 'method'=>'DELETE')) !!}
		<button class="btn btn-danger btn-sm" type="submit">
			<i class="fa fa-trash"></i> @lang('actions.delete')
		</button>
		{!! Form::close() !!}
		@else
		<form action="{{ url('/teachers/'.Auth::user()->user->teacher->teacher_id.'/lessons') }}" method="POST">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="quote">Price: </label>
				<div class="input-group col-xs-6 col-sm-4 col-md-8 col-lg-6">
					<input class="form-control" type="number" step="10" value="{{ $post->price or 100 }}" min="0" name="quote">
					<div class="input-group-addon">P</div>
				</div>
			</div>
			<input value='{{$post->post_id}}' name='post_id' type='hidden'>
			<input class="btn btn-primary btn-" type="submit" name="apply" value="Apply to Teach">
		</form>
		@endif
		@endif
		@endcan
	</div>
</div>