@extends('site')

@section('content')
@include('site.partials.nav-forum')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<div class="col-md-10 col-md-offset-1">
    <h2 class="page-header">Editing Post #{{ $post->post_id }}</h2>

    <div class="col-md-12">
        {!! Form::model($post, array('route' => array('posts.update', $post->post_id), 'method'=>'PUT')) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', null, array('class'=>'form-control')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('body', 'Body') !!}
            {!! Form::textarea('body', null, array('class'=>'form-control js-summernote', 'id'=>'question__text')) !!}
        </div>

        <div class="form-group">
            {!! Form::label('filters', 'Tags') !!}
            @include('site.posts.partials.filters')
        </div>

        {!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
        <a href="{{ url('/posts/'.$post->post_id) }}" class="btn btn-default">Cancel</a>

        {!! Form::close() !!}
    </div>
</div>
@include('site.posts.partials.summernote')

@endsection

