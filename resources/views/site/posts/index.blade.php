@extends('site')

@section('content')
@include('site.partials.nav-forum')
@include('site.posts.modals.new')


<div class="col-md-10 col-md-offset-1">
    <h2 class="page-header">
        @lang('site.forum')
        @can('logged-in')
        <button class="btn btn-primary right" id="js-new-question-btn" type="button" data-toggle="modal" data-target="#new-question-modal" name="ask-question">
            <i class="fa fa-pencil-square-o"></i> @lang('site.ask_question')
        </button>
        @else
        <a href="{{ url("login") }}" class="btn btn-primary right" name="ask-question">
            <i class="fa fa-pencil-square-o"></i> @lang('site.ask_question')
        </a>
        @endcan
    </h2>
</div>

<div class="col-md-3 col-md-offset-1 col-xs-10 col-xs-offset-1 cf">
    {!! Form::open(array('route' => 'posts.index', 'method'=>'GET', 'class'=>'form')) !!}
    <button class="btn btn-default btn-small btn-block forum-tag-filter" type="submit">
        <i class="fa fa-filter"></i> @lang('actions.filter')
    </button>

    <div class="form-group filters__status">
        {!! Form::label('post_status', trans('site.status')) !!}
        <div class="radio">
            <label class="radio__label">
                <input type="radio" name="status" value="0" @if (!Input::get('status'))) checked @endif>
                @lang('statuses.all')
            </label>
        </div>
        <div class="radio">
            <label class="radio__label text-info">
                <input type="radio" name="status" value="1" @if (Input::get('status')==="1")) checked @endif>
                @lang('statuses.open')
            </label>
        </div>
        <div class="radio">
            <label class="radio__label text-primary">
                <input type="radio" name="status" value="2" @if (Input::get('status')==="2")) checked @endif>
                @lang('statuses.progress')
            </label>
        </div>
        <div class="radio">
            <label class="radio__label text-success">
                <input type="radio" name="status" value="3" @if (Input::get('status')==="3")) checked @endif>
                @lang('statuses.solved')
            </label>
        </div>
    </div>

    {!! Form::label('tags', trans('site.tags')) !!}
    @include('site.posts.partials.filters', ['namespace'=>'test'])
    {!!Form::close() !!}
    <hr>
</div>

<div class="col-md-7 col-xs-12 post-previews">
    @foreach ($posts as $post)
    @include('site.posts.partials.preview', ['post'=>$post,
        'user'=>$post->user,
        'status'=>$post->postStatus,
        'tags'=>$post->tags,
        'teacher'=>$post->teacher,
        'show_actions'=> false
        ])
        @endforeach
        <div class="text-center">
            {{ $posts->appends(Input::except('page'))->links() }}
        </div>
    </div>
    @stop