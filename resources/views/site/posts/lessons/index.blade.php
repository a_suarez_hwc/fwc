@extends('site')

@section('content')
@include('site.partials.nav-forum')
<div class="col-md-10 col-md-offset-1">
	<h2 class="page-header">
		@lang('site.my_lessons')
	</h2>
	<div role="tabpanel">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs nav-justified" role="tablist">
			<li class="active" role="presentation">
				<a aria-controls="applications" data-toggle="tab" href="#applications" role="tab">
					@lang('site.applications')
				</a>
			</li>
			<li role="presentation">
				<a aria-controls="progress" data-toggle="tab" href="#progress" role="tab">
					@lang('statuses.progress')
				</a>
			</li>
			<li role="presentation">
				<a aria-controls="solved" data-toggle="tab" href="#solved" role="tab">
					@lang('statuses.solved')
				</a>
			</li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane active" id="applications" role="tabpanel">
				<br>
				@if (count($lessons_applied)!==0)
				@foreach ($lessons_applied as $post)
				@include('site.posts.partials.preview', [
					'post'=>$post,
					'user'=>$post->user,
					'status'=>$post->postStatus,
					'tags'=>$post->tags,
					'teacher'=>$post->teacher,
					'isApplication'=>true
					])
					@endforeach
					@else
					<p class="text-center well well-lg">
						You have no applications
					</p>
					@endif
				</br>
			</div>
			<div class="tab-pane" id="progress" role="tabpanel">
				<br>
				@if (count($lessons_inprogress)!==0)
				@foreach ($lessons_inprogress as $post)
				@include('site.posts.partials.preview', [
					'post'=>$post,
					'user'=>$post->user,
					'status'=>$post->postStatus,
					'tags'=>$post->tags,
					'teacher'=>$post->teacher
					])
					@endforeach
					@else
					<p class="text-center well well-lg">
						You have no lessons in progress
					</p>
					@endif
				</br>
			</div>
			<div class="tab-pane" id="solved" role="tabpanel">
				<br>
				@if (count($lessons_solved)!==0)
				@foreach ($lessons_solved as $post)
				@include('site.posts.partials.preview', [
					'post'=>$post,
					'user'=>$post->user,
					'status'=>$post->postStatus,
					'tags'=>$post->tags,
					'teacher'=>$post->teacher
					])
					@endforeach
					@else
					<p class="text-center well well-lg">
						You have no solved lessons
					</p>
					@endif
				</br>
			</div>
		</div>
	</div>
</div>
@stop
