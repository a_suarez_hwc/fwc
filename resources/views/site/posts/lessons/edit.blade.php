@extends('site')

@section('content')
@include('site.partials.nav-forum')
<div class="col-md-10 col-md-offset-1">
	<h2 class="page-header">
		Editing Lesson Solution
	</h2>
	<div class="col-md-12">
		<div class="row">
			@include('site.posts.partials.post', ['post'=>$post, 'teacher'=>$teacher, 'user'=>$teacher->user, 'tags'=>$post->tags, 'status'=>$post->postStatus])
		</div>
		@if(isset($studentIsEditing))
		{!! Form::model($post, array('action' => array('UserQuestionController@updateSolution', $user->user_id, $post->post_id), 'method'=>'PUT')) !!}
		<div class="form-group">
			{!! Form::label('solution', trans('site.solution')) !!}
			{!! Form::textarea('solution', null, array('class'=>'form-control')) !!}
		</div>
		{!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
		<a class="btn btn-default" href="{{ url('/users/'.$user->user_id.'/questions/') }}">
			Cancel
		</a>
		{!! Form::close() !!}
		@else
		{!! Form::model($post, array('route' => array('teachers.lessons.update', $teacher->teacher_id, $post->post_id), 'method'=>'PUT')) !!}
		<div class="form-group">
			{!! Form::label('solution', 'Solution') !!}
			{!! Form::textarea('solution', null, array('class'=>'form-control js-summernote')) !!}
		</div>
		{!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
		<a class="btn btn-default" href="{{ url('/teachers/'.$teacher->teacher_id.'/lessons/') }}">
			Cancel
		</a>
		{!! Form::close() !!}
		@endif
	</div>
</div>
@include('site.posts.partials.summernote')
@endsection
