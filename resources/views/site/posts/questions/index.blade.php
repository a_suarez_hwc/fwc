@extends('site')

@section('content')
@include('site.partials.nav-forum')
@include('site.posts.modals.new')
<div class="col-md-10 col-md-offset-1">
	<h2 class="page-header">
		@lang('site.my_questions')
		<button class="btn btn-primary right" data-target="#new-question-modal" data-toggle="modal" id="js-new-question-btn" type="button">
			<i class="fa fa-pencil-square-o"></i>
			@lang('site.ask_question')
		</button>
	</h2>
	<div role="tabpanel">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs nav-justified" role="tablist">
			<li class="active" role="presentation">
				<a aria-controls="open" data-toggle="tab" href="#open" role="tab">
					@lang('statuses.open')
				</a>
			</li>
			<li role="presentation">
				<a aria-controls="progress" data-toggle="tab" href="#progress" role="tab">
					@lang('statuses.progress')
				</a>
			</li>
			<li role="presentation">
				<a aria-controls="solved" data-toggle="tab" href="#solved" role="tab">
					@lang('statuses.solved')
				</a>
			</li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane active" id="open" role="tabpanel">
				<br>
				@if(count($open_posts)!==0)
				@foreach ($open_posts as $post)
				@include('site.posts.partials.preview', ['post'=>$post,
					'user'=>$post->user,
					'status'=>$post->postStatus,
					'tags'=>$post->tags,
					'applicants'=>$post->teachers
					])
					@endforeach
					@else
					<p class="text-center well well-lg">
						You have no open questions
					</p>
					@endif
				</br>
			</div>

			<div class="tab-pane" id="progress" role="tabpanel">
				<br>
				@if(count($in_progress_posts)!==0)
				@foreach ($in_progress_posts as $post)
				@include('site.posts.partials.preview',[
					'post'=>$post,
					'user'=>$post->user,
					'status'=>$post->postStatus,
					'tags'=>$post->tags,
					'teacher'=>$post->teacher
					])
					@endforeach
					@else
					<p class="text-center well well-lg">
						You have no questions in progress
					</p>
					@endif
				</br>
			</div>

			<div class="tab-pane" id="solved" role="tabpanel">
				<br>
				@if(count($solved_posts)!==0)
				@foreach ($solved_posts as $post)
				@include('site.posts.partials.preview', [
					'post'=>$post,
					'user'=>$post->user,
					'status'=>$post->postStatus,
					'tags'=>$post->tags,
					'teacher'=>$post->teacher])
					@endforeach
					@else
					<p class="text-center well well-lg">
						You have no solved questions
					</p>
					@endif
				</br>
			</div>
		</div>
	</div>
</div>
@stop
