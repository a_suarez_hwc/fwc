<div class="modal fade" id="new-question-modal" tabindex="-1" role="dialog" aria-labelledby="new-question-modal-label">
	<div class="modal-dialog" role="document">
		<div class="panel panel-default">
			<div class="panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="new-question-modal-label">@lang('site.ask_question')</h4>
			</div>

			<form action="{{ url('posts') }}" method="POST">
				{{ csrf_field() }}
				<div class="modal-body">
					<div class="form-group">
						<label class="form-label--required" for="title">@lang('site.question')</label>
						<input type="text" class="form-control" id="new-question__title" name="title" placeholder="e.g. What is a div?" required>
					</div>
					<div class="form-group">
						<label class="form-label--required" for="body">@lang('site.description')</label>
						<textarea class="form-control js-summernote" id="new-question__text" rows="3" name="body" required></textarea>
					</div>
					<div class="row">
						<div class="form-group col-sm-6">
							<label for="price" class="form-label">@lang('site.points')</label>
							<div class="input-group">
								<input type="number" class="form-control" name="price" step="10" min="0">
								<div class="input-group-addon">P</div>
							</div>
						</div>
						<div class="form-group col-sm-6">
							<label for="target_date" class="form-label">TARGET DATE</label>
							<div class="input-group">
								<input type="text" name="target_date" class="form-control js-datepicker">
								<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="form-label--required" for="">@lang('site.tags')</label>
						@include('site.posts.partials.filters')
					</div>
				</div>
				<div class="modal-footer">
					<a class="btn btn-default" type="button" data-dismiss="modal">@lang('actions.cancel')</a>
					<input class="btn btn-primary" type="submit" value="{{ trans('actions.submit') }}">
				</div>
			</form>
		</div>
	</div>
</div>

@include('site.posts.partials.summernote')
