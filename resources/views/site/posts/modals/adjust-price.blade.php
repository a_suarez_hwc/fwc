<div class="modal fade" id="adjust-price-modal-{{ $post->post_id }}" tabindex="-1" role="dialog" aria-labelledby="review-modal-label">
	<div class="modal-dialog" role="document">
		<div class="panel panel-default">
			<div class="panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="adjust-price-modal-label">Adjust Points</h4>
			</div>
			<div class="panel-body">
				<form action="{{ url('teachers/'.$teacher->teacher_id.'/lessons/'.$post->post_id.'/adjust') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-3 col-sm-offest-3" for="balance">Current Price:</label>
						<div class="col-sm-3">
							<div class="input-group">
								<div class="form-control" readonly>{{ $current_price }}</div>
								<span class="input-group-addon">P</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 col-sm-offest-3" for="points">
							New Price:
						</label>
						<div class="col-sm-3">
							<div class="input-group">
								<input class="form-control" name="points" type="number" step="10" min="10" value='{{ $current_price }}'>
								<span class="input-group-addon">P</span>
							</div>
						</div>
					</div>

					<button class="btn btn-primary" type="submit">Adjust Points</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
						@lang('actions.cancel')
					</button>
				</form>
			</div>
		</div>
	</div>
</div>