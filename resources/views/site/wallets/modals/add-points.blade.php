<div class="modal fade" id="add-points-modal" tabindex="-1" role="dialog" aria-labelledby="review-modal-label">
	<div class="modal-dialog" role="document">
		<div class="panel panel-default">
			<div class="panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="add-points-modal-label">Add Points</h4>
			</div>
			<div class="panel-body">
				<form action="{{ url('/wallets/'.$wallet->wallet_id.'/transactions') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-4" for="balance">@lang('site.balance'):</label>
						<div class="col-sm-8">
							<div class="form-control" readonly>{{ $current_balance }}</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4" for="points">
							Amount to Purchase
						</label>
						<div class="col-sm-8">
							<select name="points" class="form-control">
								<option value="100">100P</option>
								<option value="250">250P</option>
								<option value="500">500P</option>
							</select>
						</div>
					</div>
					<input type="hidden" name="transaction_type" value="1">

					<button class="btn btn-primary" type="submit">Add Points</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
						@lang('actions.cancel')
					</button>
				</form>
			</div>
		</div>
	</div>
</div>