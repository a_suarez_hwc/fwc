<div class="modal fade" id="cash-out-modal" tabindex="-1" role="dialog" aria-labelledby="review-modal-label">
	<div class="modal-dialog" role="document">
		<div class="panel panel-default">
			<div class="panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="cash-out-modal-label">Cash Out</h4>
			</div>
			<div class="panel-body">
				<form action="{{ url('/wallets/'.$wallet->wallet_id.'/transactions') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-4" for="balance">Current Balance:</label>
						<div class="col-sm-8">
							<div class="form-control" readonly>{{ $current_balance }}</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-4" for="balance">
							Amount to Exchange:
							<span id="helpBlock" class="help-block">Minimum of 150P</span>
						</label>
						<div class="col-sm-8">
							<input class="form-control" type="number" name="points" min="150" max={{ $current_balance }}></input>
							<input type="hidden" name="transaction_type" value="2">
						</div>
					</div>

					<button class="btn btn-primary" type="submit">Cash Out</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
						Cancel
					</button>
				</form>
			</div>
		</div>
	</div>
</div>