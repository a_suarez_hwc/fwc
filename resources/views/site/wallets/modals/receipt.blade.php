<a href="#" type="button" data-toggle="modal" data-target="#receipt-modal-{{ str_replace("/", "-", $key) }}">
	View Receipt
</a>

<div class="modal fade" id="receipt-modal-{{ str_replace("/", "-", $key) }}" tabindex="-1" role="dialog" aria-labelledby="review-modal-label">
	<div class="modal-dialog" role="document">
		<div class="panel panel-default">
			<div class="panel-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<br>
				<h4 class="modal-title receipt__title text-center" id="new-question-modal-label">@lang('site.receipt')</h4>
				<!-- for HWC transactions -->
				@if (!empty($ticket->transaction_id))
				<div class="receipt__date">{{ date('y/m/j - G:i',strtotime($ticket->date)) }}</div>

				<!-- Cashing Out -->
				@if ($ticket->transaction_type_id == '2')
				<table class="table receipt__table">
					<tr>
						<td>@lang('site.balance')</td>
						<td class="receipt__amount">{{ $ticket->new_wallet_balance + $ticket->points }}P</td>
					</tr>
					<tr>
						<td>Cashed Out</td>
						<td class="receipt__amount">-{{ $ticket->points }}P</td>
					</tr>
					<tr class="receipt__total active">
						<td>@lang('site.new_balance')</td>
						<td class="receipt__amount">{{ $ticket->new_wallet_balance }}P</td>
					</tr>
				</table>
				<hr>
				<div class="text-center">
					<p>Bank Account: XXXX-XXXX-XXXX</p>
					<p>Status: <span class="label label-info">Pending</span></p>
				</div>
				@endif

				<!-- Adding Points -->
				@if ($ticket->transaction_type_id == '1')
				<table class="table receipt__table">
					<tr>
						<td>@lang('site.balance')</td>
						<td class="receipt__amount">{{ $ticket->new_wallet_balance - $ticket->points }}P</td>
					</tr>
					<tr>
						<td>Points Added</td>
						<td class="receipt__amount">+{{ $ticket->points }}P</td>
					</tr>
					<tr class="receipt__total active">
						<td>@lang('site.new_balance')</td>
						<td class="receipt__amount">{{ $ticket->new_wallet_balance }}P</td>
					</tr>
				</table>
				<hr>
				<div class="text-center">
					<p>Credit Card: XXXX-XXXX-XXXX-1234</p>
					<p>Status: <span class="label label-success">Complete</span></p>
				</div>
				@endif
				@endif

				<!-- for teacher/student exchanges -->
				@if (!empty($ticket->exchange_id))

				<!-- Asking Questions -->
				@if ($ticket->paying_wallet_id == $wallet->wallet_id)
				<table class="table receipt__table">
					<tr>
						<td>@lang('site.balance')</td>
						<td class="receipt__amount">{{ $ticket->new_paying_wallet_balance + $ticket->points_paid }}P</td>
					</tr>
					<tr>
						<td>Asked Question</td>
						<td class="receipt__amount">-{{ $ticket->points_paid }}P</td>
					</tr>
					<tr class="receipt__total active">
						<td>@lang('site.new_balance')</td>
						<td class="receipt__amount">{{ $ticket->new_paying_wallet_balance }}P</td>
					</tr>
				</table>
				<div class="text-center">
					<a href="{{ url('/posts/'.$ticket->post_id) }}" class="btn btn-default text-">View Question</a>
				</div>
				@endif

				<!-- Solving Questions -->
				@if ($ticket->receiving_wallet_id == $wallet->wallet_id)
				<table class="table receipt__table">
					<tr>
						<td>@lang('site.balance')</td>
						<td class="receipt__amount">{{ $ticket->new_receiving_wallet_balance - $ticket->points_received }}P</td>
					</tr>
					<tr>
						<td>Solved Question</td>
						<td class="receipt__amount">+{{ $ticket->points_received }}P</td>
					</tr>
					<tr class="receipt__total active">
						<td>@lang('site.new_balance')</td>
						<td class="receipt__amount">{{ $ticket->new_receiving_wallet_balance }}P</td>
					</tr>
				</table>
				<div class="text-center">
					<a href="{{ url('/posts/'.$ticket->post_id) }}" class="btn btn-default text-">View Question</a>
				</div>

				@endif
				@endif
			</div>
		</div>
	</div>
</div>