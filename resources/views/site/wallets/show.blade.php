@extends('site')

@section('content')
@include('site.partials.nav-forum')
@include('site.wallets.modals.add-points')
@include('site.wallets.modals.redeem')


<div class="col-md-10 col-md-offset-1">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header">@lang('site.wallet')</h2>
		</div>
		
		<div class="col-md-3">
			<div class="wallet__balance">@lang('site.balance'): <strong>{{ $current_balance }}P</strong></div>
			<br>
			<div class="list-group">
				<button type="button" class="list-group-item" data-toggle="modal" data-target="#add-points-modal">
					<i class="fa fa-fw fa-plus"></i> @lang('actions.add_points')
				</button>
				@if (isTeacher(Auth::user()))
				<button type="button" class="list-group-item" data-toggle="modal" data-target="#cash-out-modal">
					<i class="fa fa-fw fa-yen"></i> @lang('actions.redeem_points')
				</button>
				@endif
			</div>
			<br>
		</div>

		<div class="col-md-9">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>@lang('site.date')</th>
							<th>@lang('site.type')</th>
							<th>@lang('site.amount')</th>
							<th>@lang('site.new_balance')</th>
							<th>@lang('site.receipt')</th>
						</tr>
					</thead>

					<tbody>
						@foreach ($tickets as $key=>$ticket)
						<tr>
							<td>{{ date('y年m月j日 - G:i', strtotime($ticket->created_at)) }}</td>

							@if (!empty($ticket->transaction_id))
							<!-- Added Points-->
							@if ($ticket->transaction_type_id == '1')
							<td>Added Points</td>
							<td>
								<div class="label label-success ">+{{ $ticket->points }}P</div>
							</td>
							<td>{{ $ticket->new_wallet_balance }}P</td>
							<td>@include('site.wallets.modals.receipt', ['ticket'=>$ticket,'key'=>$key])</td>
							@endif

							<!-- Cashed Out -->
							@if ($ticket->transaction_type_id == '2')
							<td>Cashed Out</td>
							<td>
								<div class="label label-danger ">-{{ $ticket->points }}P</div>
							</td>
							<td>{{ $ticket->new_wallet_balance }}P</td>
							<td>@include('site.wallets.modals.receipt', ['ticket'=>$ticket,'key'=>$key])</td>
							@endif

							<!-- Adjustment from Admin -->
							@if ($ticket->transaction_type_id == '3')
							<td>Adjustment</td>
							<td>
								<div class="label label-warning ">
									@if ((float)$ticket->points > 0)
									+
									@else
									-
									@endif
									{{ $ticket->points }}P
								</div>
							</td>
							<td>{{ $ticket->new_wallet_balance }}P</td>
							<td></td>
							@endif
							@endif

							@if (!empty($ticket->exchange_id))
							<!-- Asked Question -->
							@if ($ticket->paying_wallet_id == $wallet->wallet_id)
							<td>Asked Question</td>
							<td>
								<div class="label label-danger ">-{{ $ticket->points_paid }}P</div>
							</td>
							<td>{{ $ticket->new_paying_wallet_balance }}P</td>
							@endif

							<!-- Solved Question -->
							@if ($ticket->receiving_wallet_id == $wallet->wallet_id)
							<td>Solved Question</td>
							<td>
								<div class="label label-success ">+{{ $ticket->points_received }}P</div>
							</td>
							<td>{{ $ticket->new_receiving_wallet_balance }}P</td>
							@endif
							<td>@include('site.wallets.modals.receipt', ['ticket'=>$ticket,'key'=>$key])</td>
							@endif
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop

