<img src='{{ asset($teacher->user->profile_image_thumbnail_path) }}'>
@include('site.teachers.partials.rating', ['rating'=>$teacher->rating])
<ul>
	@foreach($teacher->certified_fields as $field)
	<li>{{ $field->field_text }}</li>
	@endforeach
</ul>
<a href='{{ url('/teachers/'.$teacher->teacher_id) }}' class='btn btn-info btn-block btn-sm'>View @lang('site.profile')</a>