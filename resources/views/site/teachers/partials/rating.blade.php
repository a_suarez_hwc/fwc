@if(!isset($rating)) <?php $rating = 5; ?> @endif
<span class='rating @if(isset($name))rating--form @endif'>
	@for($i = 10; $i > 0; $i--)
	@if(isset($name))
	<input type='radio' class='rating__input' id='{{ $name.'-'.$i }}' name='{{ $name }}' @if($i == round($rating)) checked @endif >
	<label for='{{ $name.'-'.$i }}' class='rating__star @if($i%2==1) rating__star--half @endif'></label>
	@else
	<input type='radio' class='rating__input' disabled @if($i == round($rating)) checked @endif >
	<label class='rating__star @if($i%2==1) rating__star--half @endif'></label>
	@endif
	@endfor
</span>