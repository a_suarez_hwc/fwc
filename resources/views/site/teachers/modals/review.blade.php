<button class="btn {{ $class or 'btn-default' }}" type="button" data-toggle="modal" data-target="#review-modal-{{ str_replace("/", "-", $action) }}">
	@if(!empty($icon))
	<i class="fa fa-{{ $icon }}"></i>
	@endif
	{{ $button_text or 'confirm' }}
</button>
<div class="modal fade" id="review-modal-{{ str_replace("/", "-", $action) }}" tabindex="-1" role="dialog" aria-labelledby="review-modal-label">
	<div class="modal-dialog" role="document">
		<div class="panel panel-default">
			<div class="panel-heading">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="new-question-modal-label">Review Teacher</h4>
			</div>
			<div class="panel-body">
				<form action="{{ url($action) }}" method="POST" class="form">
					{{ csrf_field() }}
					<div class="form-group">
						{!! Form::label(null,'Solution') !!}
						<br>
						{{ $solution }}
					</div>
					<div class="form-group">
						{!! Form::label('knowledge_score', 'Knoweldge') !!}
						{!! Form::select('knowledge_score', array('1' => '1', '2' => '2', '3' => '3' , '4' => '4', '5' => '5' , '6' => '6', '7' => '7' , '8' => '8', '9' => '9', '10' => '10'), null, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('clarity_score', 'Clarity') !!}
						{!! Form::select('clarity_score', array('1' => '1', '2' => '2', '3' => '3' , '4' => '4', '5' => '5' , '6' => '6', '7' => '7' , '8' => '8', '9' => '9', '10' => '10'), null, array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('overall_satisfaction_score', 'Overall Satisfaction') !!}
						{!! Form::select('overall_satisfaction_score', array('1' => '1', '2' => '2', '3' => '3' , '4' => '4', '5' => '5' , '6' => '6', '7' => '7' , '8' => '8', '9' => '9', '10' => '10'),null,array('class'=>'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('comments', 'Comments') !!}
						{!! Form::textarea('comments', null, array('class'=>'form-control')) !!}
					</div>

					<button class="btn {{ $class or 'btn-default' }}" type="submit">
						@if(!empty($icon))
						<i class="fa fa-{{ $icon }}"></i>
						@endif
						{{ $button_text or 'confirm' }}
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">
						Cancel
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
