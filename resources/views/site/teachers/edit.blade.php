@extends('site')

@section('content')
<div class="col-md-10 col-md-offset-1">
	<h2 class="page-header">Editing Teacher @lang('site.profile')</h2>

	{!! Form::model($teacher, array('route' => array('teachers.update', $teacher->teacher_id), 'method'=>'PUT')) !!}
	<div class="form-group col-md-4">
		{!! Form::label('years_of_industry_experience', trans('site.years_of_industry_experience')) !!}
		{!! Form::number('years_of_industry_experience', null, array('class' => 'form-control')) !!}
	</div>

	<div class="form-group col-md-4">
		{!! Form::label('years_of_freelance_experience', trans('site.years_of_freelance_experience')) !!}
		{!! Form::number('years_of_freelance_experience', null, array('class' => 'form-control')) !!}
	</div>

	<div class="form-group col-md-4">
		{!! Form::label('rating', trans('site.rating')) !!}
		{!! Form::number('rating', null, array('class' =>'form-control', 'readonly')) !!}
	</div>

	<div class="col-md-12">
		<div class="form-group">
			{!! Form::label('skills', trans('site.skills')) !!}
			{!! Form::text('skills', null, array('class' => 'form-control')) !!}
		</div>

		<div class="form-group">
			{!! Form::label('description', trans('site.description')) !!}
			{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
		</div>

		{!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
		<a href="{{ url('/teachers/'.$teacher->teacher_id) }}" class="btn btn-default">@lang('actions.cancel')</a>
	</div>

	{!! Form::close() !!}
</div>
@endsection