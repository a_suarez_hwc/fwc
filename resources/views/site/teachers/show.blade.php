@extends('site')

@section('content')
<div class="col-md-10 col-md-offset-1">
	<div class="row">
		<div class="col-xs-12">
			<h2 class="user__title page-header">
				{{ $teacher->user->name_kanji }}
			</h2>
		</div>
		<div class="col-md-3">
			<div class="text-center">
				<img src="@if($teacher->user->profile_image_path===null){{ asset('img/default.jpg')}} @else {{ asset($teacher->user->profile_image_path) }} @endif" alt="" class="user-profile__img">
			</div>
			<div class="list-group">
				{{-- TODO: set up email --}}
				@can('access-teacher-profile', $teacher)
				<a class="list-group-item" href="{{ url('/users/'.$teacher->user->user_id) }}">
					<i class="fa fa-user"></i> @lang('site.user')@lang('site.profile')
				</a>
				<a class="list-group-item" href="{{ url('/teachers/'.$teacher->teacher_id.'/edit') }}">
					<i class="fa fa-pencil"></i> Edit @lang('site.profile')
				</a>
				@else
				<button class="btn btn-primary"><i class="fa fa-envelope"></i> Send Email</button>
				<button class="btn btn-disabled"><i class="fa fa-comment"></i> Send Message</button>
				<button class="btn btn-disabled"><i class="fa fa-video-camera"></i> Video Chat</button>
				@endcan
			</div>
		</div>
		<div class="col-md-9">
			<table class="table">
				<tbody>
					<tr>
						<th>@lang('site.years_of_industry_experience')</th>
						<td>{{ $teacher->years_of_industry_experience }}</td>
					</tr>
					<tr>
						<th>@lang('site.years_of_freelance_experience')</th>
						<td>{{ $teacher->years_of_freelance_experience }}</td>
					</tr>
					<tr>
						<th class="col-xs-2">@lang('site.certified_fields')</th>
						<td colspan="3">
							<ul>
								@foreach($teacher->certified_fields as $field)
								<li>{{ $field->field_text }}</li>
								@endforeach
							</ul>
						</td>
					</tr>
					<tr>
						<th>@lang('site.skills')</th>
						<td>{{ $teacher->skills }}</td>
					</tr>
					<tr>
						<th>@lang('site.rating')</th>
						<td>@include('site.teachers.partials.rating', ['rating'=>$teacher->rating])</td>
					</tr>
					<tr>
						<th>@lang('site.description')</th>
						<td>
							{{ $teacher->description }}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop