@extends('admin')

@section('content')
<div class="col-sm-3">
    <div class="text-center"><img class="profile-img" src="{{ asset($user->profile_image_path) }}"></div>
    <hr>
    <div class="list-group">
        <a class="list-group-item" href="{{ url('admin/users/'.$user->user_id) }}"><i class="fa fa-fw fa-user"></i> @lang('site.user') @lang('site.profile')</a>
        @if($user->teacher)
        <a class="list-group-item" href="{{ url('admin/teachers/'.$user->teacher->teacher_id) }}"><i class="fa fa-fw fa-graduation-cap"></i> @lang('site.teacher') @lang('site.profile')</a>
        @endif
        <a class="list-group-item" href="{{ url('admin/wallets/'.$user->wallet->wallet_id) }}"><i class="fa fa-fw fa-credit-card"></i> @lang('site.wallet')</a>
    </div>
    <hr>
</div>
<div class="col-xs-9">
    <div class="page-header"><h1>Editing @lang('site.user') #{{ $user->user_id }}</h1></div>
    {{ Form::model($user, ['action'=>['Admin\UserController@update',  $user->user_id],'method'=>'PUT']) }}
    <fieldset class="row">
        <div class="col-sm-12"><legend>@lang('site.info')</legend></div>
        <div class="col-sm-6">
            <div class="form-group" @if($errors->has('family_name_kanji')) has-error @endif>
                {{ Form::label('family_name_kanji', trans('site.family_name_kanji'), ['class'=>'control-label']) }}
                {{ Form::text('family_name_kanji', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('family_name_kanji') }}</div>
            </div>
            <div class="form-group" @if($errors->has('family_name_kana')) has-error @endif>
                {{ Form::label('family_name_kana', trans('site.family_name_kana'), ['class'=>'control-label']) }}
                {{ Form::text('family_name_kana', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('family_name_kana') }}</div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group" @if($errors->has('first_name_kanji')) has-error @endif>
                {{ Form::label('first_name_kanji', trans('site.first_name_kanji'), ['class'=>'control-label']) }}
                {{ Form::text('first_name_kanji', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('first_name_kanji') }}</div>
            </div>
            <div class="form-group" @if($errors->has('first_name_kana')) has-error @endif>
                {{ Form::label('first_name_kana', trans('site.first_name_kana'), ['class'=>'control-label']) }}
                {{ Form::text('first_name_kana', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('first_name_kana') }}</div>
            </div>
        </div>
    </fieldset>
    <fieldset class="row">
        <div class="col-sm-12"><legend>@lang('site.contact')</legend></div>
        <div class="col-sm-6">

            <div class="form-group @if($errors->has('contact.email')) has-error @endif ">
                {{ Form::label('contact[email]', trans('site.email'), ['class'=>'control-label']) }}
                {{ Form::text('contact[email]', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('contact.email') }}</div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group @if($errors->has('contact.mobile_email')) has-error @endif ">
                {{ Form::label('contact[mobile_email]', trans('site.mobile_email'), ['class'=>'control-label']) }}
                {{ Form::text('contact[mobile_email]', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('contact.mobile_email') }}</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group @if($errors->has('contact.phone_number')) has-error @endif ">
                {{ Form::label('contact[phone_number]', trans('site.phone_number'), ['class'=>'control-label']) }}
                {{ Form::text('contact[phone_number]', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('contact.phone_number') }}</div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group @if($errors->has('contact.skype')) has-error @endif ">
                {{ Form::label('contact[skype]', trans('site.skype'), ['class'=>'control-label']) }}
                {{ Form::text('contact[skype]', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('contact.skype') }}</div>
            </div>
        </div>
    </fieldset>
    <fieldset class="row">
        <div class="col-xs-12"><legend>@lang('site.login_credentials')</legend></div>
        <div class="col-sm-10">
            <div class="form-group" @if($errors->has('login_credential.email')) has-error @endif>
                {{ Form::label('login_credential[email]', trans('site.email'), ['class'=>'control-label']) }}
                {{ Form::text('login_credential[email]', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('login_credential.email') }}</div>
            </div>
        </div>
        <div class="col-sm-2">
             <div class="form-group" @if($errors->has('login_credential.status')) has-error @endif>
                {{ Form::label('login_credential[status]', trans('site.status'), ['class'=>'control-label']) }}
                {{ Form::text('login_credential[status]', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('login_credential.status') }}</div>
            </div>
        </div>
    </fieldset>

    <div class="right">
        <a href="{{ url('admin/users/'.$user->user_id) }}" class="btn btn-default">@lang('actions.cancel')</a>
        {{ Form::submit(trans('actions.update'), ['class'=> 'btn btn-default']) }}
    </div>
    {{ Form::close() }}
</div>
@stop