@extends('admin')
@section('content')
<div class="col-xs-12">
    <div class="page-header"><h1>@lang('site.users')</h1></div>
    <div class="well well-sm admin-actions">
        <a href="{{ url('admin/users/create') }}" class="btn btn-default"><i class="fa fa-plus"></i> @lang('actions.create')</a>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>@lang('site.id')</th>
                <th>@lang('site.name')</th>
                <th>@lang('site.email')</th>
                <th>@lang('site.mobile_email')</th>
                <th>@lang('site.last_logged_in')</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr class="js-table-row">
                <td>
                    <a class="table-row-link" href="{{ url('admin/users/'.$user->user_id) }}">
                        {{ $user->user_id }}
                    </a>
                </td>
                <td>{{ $user->name_kanji }}</td>
                <td>{{ $user->contact->email }}</td>
                <td>{{ $user->contact->mobile_email or '-' }}</td>
                <td>{{ $user->login_credential->last_logged_in }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop
