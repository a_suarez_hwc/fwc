@extends('admin')
@section('styles')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@stop
@section('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@stop
@section('content')
<div class="col-sm-12">
	<div class="page-header"><h1>Creating New User</h1></div>
	{{ Form::open(['action' => 'Admin\UserController@store']) }}
	<fieldset class="row">
		<div class="col-sm-12"><legend>@lang('site.info')</legend></div>
		<div class="col-sm-6">
			<div class="form-group @if($errors->has('family_name_kanji')) has-error @endif">
				{{ Form::label('family_name_kanji', trans('site.family_name_kanji'), ['class'=>'control-label']) }}
				{{ Form::text('family_name_kanji', null, ['class'=>'form-control']) }}
				<div class="text-danger">{{ $errors->first('family_name_kanji') }}</div>
			</div>
			<div class="form-group @if($errors->has('family_name_kana')) has-error @endif">
				{{ Form::label('family_name_kana', trans('site.family_name_kana'), ['class'=>'control-label']) }}
				{{ Form::text('family_name_kana', null, ['class'=>'form-control']) }}
				<div class="text-danger">{{ $errors->first('family_name_kana') }}</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group @if($errors->has('first_name_kanji')) has-error @endif">
				{{ Form::label('first_name_kanji', trans('site.first_name_kana'), ['class'=>'control-label']) }}
				{{ Form::text('first_name_kanji', null, ['class'=>'form-control']) }}
				<div class="text-danger">{{ $errors->first('first_name_kanji') }}</div>
			</div>
			<div class="form-group @if($errors->has('first_name_kana')) has-error @endif">
				{{ Form::label('first_name_kana', trans('first_name_kana'), ['class'=>'control-label']) }}
				{{ Form::text('first_name_kana', null, ['class'=>'form-control']) }}
				<div class="text-danger">{{ $errors->first('first_name_kana') }}</div>
			</div>
		</div>
	</fieldset>
	<fieldset >
		<div class="row">
			<div class="col-xs-12"><legend>@lang('site.login_credentials')</legend></div>
			<div class="col-sm-6">
				<div class="form-group @if($errors->has('login_credential.email')) has-error @endif">
					{{ Form::label('login_credential[email]', trans('site.email'), ['class'=>'control-label']) }}
					{{ Form::text('login_credential[email]', null, ['class'=>'form-control']) }}
					<div class="text-danger">{{ $errors->first('login_credential.email') }}</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group @if($errors->has('login_credential.password')) has-error @endif">
					{{ Form::label('login_credential[password]', trans('site.password'), ['class'=>'control-label']) }}
					{{ Form::password('login_credential[password]', ['class'=>'form-control']) }}
					<div class="text-danger">{{ $errors->first('login_credential.password') }}</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-2">
				<div class="form-group @if($errors->has('login_credential.status')) has-error @endif">
					{{ Form::label('login_credential[status]', trans('site.status'), ['class'=>'control-label']) }}
					{{ Form::text('login_credential[status]', 'ok', ['class'=>'form-control']) }}
					<div class="text-danger">{{ $errors->first('login_credential.status') }}</div>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label class="">@lang('site.teacher')?</label>
					<div class="checkbox checkbox-toggle">
						{{ Form::checkbox('is_teacher',null,false,['class'=>'js-checkbox-is-teacher','data-toggle'=>'toggle','data-on'=>'Yes','data-off'=>'No']) }}
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset class="js-user-create-teacher" style="display: none;">
			<div class="row">
				<div class="col-sm-12"><legend>@lang('site.teacher') @lang('site.info')</legend></div>
				<div class="col-sm-4">
					<div class="form-group @if($errors->has('rating')) has-error @endif">
						{{ Form::label('rating', trans('site.rating'), ['class'=>'control-label']) }}
						{{ Form::number('rating', 5, ['class'=>'form-control', 'max'=>10, 'min'=>0]) }}
						<div class="text-danger">{{ $errors->first('rating') }}</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group @if($errors->has('years_of_industry_experience')) has-error @endif">
						{{ Form::label('years_of_industry_experience', trans('site.years_of_industry_experience'), ['class'=>'control-label']) }}
						{{ Form::number('years_of_industry_experience', 0, ['class'=>'form-control', 'min'=>0]) }}
						<div class="text-danger">{{ $errors->first('years_of_industry_experience') }}</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group @if($errors->has('years_of_freelance_experience')) has-error @endif">
						{{ Form::label('years_of_freelance_experience', trans('site.years_of_freelance_experience'), ['class'=>'control-label']) }}
						{{ Form::number('years_of_freelance_experience', 0, ['class'=>'form-control', 'min'=>0]) }}
						<div class="text-danger">{{ $errors->first('years_of_freelance_experience') }}</div>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="form-group @if($errors->has('skills')) has-error @endif">
						{{ Form::label('skills', trans('site.skills'), ['class'=>'control-label']) }}
						{{ Form::text('skills', null, ['class'=>'form-control']) }}
						<div class="text-danger">{{ $errors->first('skills') }}</div>
					</div>
					<div class="form-group @if($errors->has('description')) has-error @endif">
						<div class="text-danger">{{ $errors->first('description') }}</div>
						{{ Form::label('description', trans('site.description'), ['class'=>'control-label']) }}
						{{ Form::textarea('description', null, ['class'=>'form-control']) }}
					</div>
					<div class="form-group @if($errors->has('certified_fields')) has-error @endif">
						<label class="certfield-label control-label">@lang('site.certified_fields')</label>
						<div class="text-danger">{{ $errors->first('certified_fields') }}</div>
						@foreach($cert_fields as $key=>$field)
						<div class="checkbox certfield-checkbox">
							<label>
								{{ Form::checkbox('certified_fields[]', $field->certified_field_id) }}
								{{ $field->field_text }}
							</label>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<div class="row">
				<div class="col-sm-12"><legend>@lang('site.contact')</legend></div>
				<div class="col-sm-6">
					<div class="form-group @if($errors->has('contact.email')) has-error @endif ">
						{{ Form::label('contact[email]', trans('site.email'), ['class'=>'control-label']) }}
						{{ Form::text('contact[email]', null, ['class'=>'form-control']) }}
						<div class="text-danger">{{ $errors->first('contact.email') }}</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group @if($errors->has('contact.mobile_email')) has-error @endif ">
						{{ Form::label('contact[mobile_email]', trans('site.mobile_email'), ['class'=>'control-label']) }}
						{{ Form::text('contact[mobile_email]', null, ['class'=>'form-control']) }}
						<div class="text-danger">{{ $errors->first('contact.mobile_email') }}</div>
					</div>
				</div></div>
				<div class="row"><div class="col-sm-3">
					<div class="form-group @if($errors->has('contact.phone_number')) has-error @endif ">
						{{ Form::label('contact[phone_number]', trans('site.phone_number'), ['class'=>'control-label']) }}
						{{ Form::text('contact[phone_number]', null, ['class'=>'form-control']) }}
						<div class="text-danger">{{ $errors->first('contact.phone_number') }}</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group @if($errors->has('contact.skype')) has-error @endif ">
						{{ Form::label('contact[skype]', trans('site.skype'), ['class'=>'control-label']) }}
						{{ Form::text('contact[skype]', null, ['class'=>'form-control']) }}
						<div class="text-danger">{{ $errors->first('contact.skype') }}</div>
					</div>
				</div>
			</div>
		</fieldset>
		<div class="right">
			<a href="{{ url('admin/users/') }}" class="btn btn-default">@lang('actions.cancel')</a>
			{{ Form::submit(trans('actions.save'), ['class'=> 'btn btn-default']) }}
		</div>
		{{ Form::close() }}
	</div>
	@stop