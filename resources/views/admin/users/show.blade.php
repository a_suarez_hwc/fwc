@extends('admin')

@section('content')
<div class="col-sm-3">
    <div class="text-center"><img class="profile-img" src="{{ asset($user->profile_image_path) }}"></div>
    <hr>
    <div class="list-group">
        <a class="list-group-item" href="{{ url('admin/users/'.$user->user_id) }}"><i class="fa fa-fw fa-user"></i> @lang('site.user') @lang('site.profile')</a>
        @if($user->teacher)
        <a class="list-group-item" href="{{ url('admin/teachers/'.$user->teacher->teacher_id) }}"><i class="fa fa-fw fa-graduation-cap"></i> @lang('site.teacher') @lang('site.profile')</a>
        @endif
        <a class="list-group-item" href="{{ url('admin/wallets/'.$user->wallet->wallet_id) }}"><i class="fa fa-fw fa-credit-card"></i> @lang('site.wallet')</a>
    </div>
    <hr>
</div>

<div class="col-sm-9">
    <div class="page-header"><h1>@lang('site.user') #{{ $user->user_id }}</h1></div>
    <div class="well well-sm admin-actions">
        <a href="{{ url('admin/users/'.$user->user_id.'/edit') }}" class="btn btn-default"><i class="fa fa-edit"></i> @lang('actions.edit')</a>
        <a href="{{ url('') }}" class="btn btn-danger btn-sm right"><i class="fa fa-trash"></i> @lang('actions.delete')</a>
    </div>
    <table class="table table-hover table-condensed table-striped">
        <caption>@lang('site.info')</caption>
        <tbody>
            <tr>
                <td>@lang('site.name')</td>
                <td>{{ $user->name_kanji or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.name_kana')</td>
                <td>{{ $user->name_kana or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.email')</td>
                <td>{{ $user->login_credential->email or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.status')</td>
                <td>{{ $user->login_credential->status or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.admin')</td>
                <td>{{ $user->is_admin ? "True" : "False" }}</td>
            </tr>
            <tr>
                <td>@lang('site.created_at')</td>
                <td>{{ $user->created_at or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.last_updated')</td>
                <td>{{ $user->last_updated or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.last_logged_in')</td>
                <td>{{ $user->login_cred->last_logged_in or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.profile_image_path')</td>
                <td>{{ $user->profile_image_path or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.profile_image_thumbnail_path')</td>
                <td>{{ $user->profile_image_thumbnail_path or '-' }}</td>
            </tr>
        </tbody>
    </table>

    <table class="table table-hover table-condensed table-striped">
        <caption>@lang('site.contact')</caption>
        <tbody>
            <tr>
                <td>@lang('site.email')</td>
                <td>{{ $user->contact->email or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.mobile_email')</td>
                <td>{{ $user->contact->mobile_email or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.skype')</td>
                <td>{{ $user->contact->skype or '-' }}</td>
            </tr>
            <tr>
                <td>@lang('site.phone_number')</td>
                <td>{{ $user->contact->phone_number or '-' }}</td>
            </tr>
        </tbody>
    </table>
</div>
@stop