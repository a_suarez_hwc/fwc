@extends('admin')
@section('content')
<div class="col-xs-12">
    <div class="page-header"><h1>@lang('site.tags')</h1></div>
{{--     <div class="well well-sm admin-actions">
        <a href="{{ url('admin/teachers/create') }}" class="btn btn-default"><i class="fa fa-plus"></i> @lang('actions.create')</a>
    </div> --}}

    <table class="table">
        <tbody>
            @foreach($groups as $group)
            @foreach($group->tags as $i=>$tag)
            <tr>
                @if($i==0)
                <td rowspan={{ count($group->tags) }}>
                    {{ $group->tag_group_text }}
                </td>
                @endif
                <td>{{ $tag->tag_text }}</td>
            </tr>
            @endforeach
            @endforeach
        </tbody>
    </table>
</div>
@stop