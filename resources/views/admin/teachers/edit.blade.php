@extends('admin')

@section('content')
<div class="col-sm-3">
    <div class="text-center"><img class="profile-img" src="{{ asset($teacher->user->profile_image_path) }}"></div>
    <hr>
    <div class="list-group">
        <a class="list-group-item" href="{{ url('admin/users/'.$teacher->user->user_id) }}"><i class="fa fa-fw fa-user"></i> @lang('site.user') @lang('site.profile')</a>
        <a class="list-group-item" href="{{ url('admin/teachers/'.$teacher->teacher_id) }}"><i class="fa fa-fw fa-graduation-cap"></i> @lang('site.teacher') @lang('site.profile')</a>
        <a class="list-group-item" href="{{ url('admin/wallets/'.$teacher->user->wallet->wallet_id) }}"><i class="fa fa-fw fa-credit-card"></i> @lang('site.wallet')</a>
    </div>
    <hr>
</div>
<div class="col-sm-9">
    <div class="page-header"><h1>Editing @lang('site.teacher') #{{ $teacher->teacher_id }}</h1></div>
    {{ Form::model($teacher, ['action'=>['Admin\TeacherController@update',  $teacher->teacher_id],'method'=>'PUT']) }}
    <fieldset class="row">
        <div class="col-sm-4">
            <div class="form-group @if($errors->has('rating')) has-error @endif">
                {{ Form::label('rating', trans('site.rating'), ['class'=>'control-label']) }}
                {{ Form::text('rating', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('rating') }}</div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group @if($errors->has('years_of_industry_experience')) has-error @endif">
                {{ Form::label('years_of_industry_experience', trans('site.years_of_industry_experience'), ['class'=>'control-label']) }}
                {{ Form::number('years_of_industry_experience', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('years_of_industry_experience') }}</div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group @if($errors->has('years_of_freelance_experience')) has-error @endif">
                {{ Form::label('years_of_freelance_experience', trans('site.years_of_freelance_experience'), ['class'=>'control-label']) }}
                {{ Form::number('years_of_freelance_experience', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('years_of_freelance_experience') }}</div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group @if($errors->has('skills')) has-error @endif">
                {{ Form::label('skills', trans('site.skills'), ['class'=>'control-label']) }}
                {{ Form::text('skills', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('skills') }}</div>
            </div>
            <div class="form-group @if($errors->has('description')) has-error @endif">
                {{ Form::label('description', trans('skills.description'), ['class'=>'control-label']) }}
                {{ Form::textarea('description', null, ['class'=>'form-control']) }}
                <div class="text-danger">{{ $errors->first('description') }}</div>
            </div>
            <div class="form-group @if($errors->has('certified_fields')) has-error @endif">
                <label class="certfield-label control-label">@lang('site.certified_fields')</label>
                <div class="text-danger">{{ $errors->first('certified_fields') }}</div>
                @foreach($cert_fields as $key=>$field)
                <div class="checkbox certfield-checkbox">
                    <label>
                        <input type="checkbox" name="certified_fields[]" value="{{ $field->certified_field_id }}"" @if($teacher->certified_fields->contains($field)) checked @endif>
                        {{ $field->field_text }}
                    </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>
<div class="right">
    <a href="{{ url('admin/teachers/'.$teacher->teacher_id) }}" class="btn btn-default">@lang('actions.cancel')</a>
    {{ Form::submit(trans('actions.update'), ['class'=> 'btn btn-default']) }}
</div>
</fieldset>

{{ Form::close() }}
</div>
@stop