@extends('admin')
@section('content')
<div class="col-xs-12">
    <div class="page-header"><h1>@lang('site.teachers')</h1></div>
    <div class="well well-sm admin-actions">
        <a href="{{ url('admin/teachers/create') }}" class="btn btn-default"><i class="fa fa-plus"></i> @lang('actions.create')</a>
    </div>

    <table class="table table-hover">
        <thead>
            <tr>
                <th>@lang('site.id')</th>
                <th>@lang('site.rating')</th>
                <th>@lang('site.name')</th>
                <th>@lang('site.email')</th>
                <th>@lang('site.mobile_email')</th>
                <th>@lang('site.phone_number')</th>
                <th>@lang('site.last_logged_in')</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($teachers as $teacher)
            <tr class="js-table-row">
                <td>
                    <a class="table-row-link" href="{{ url('admin/teachers/'.$teacher->teacher_id) }}">
                        {{ $teacher->teacher_id }}
                    </a>
                </td>
                <td>{{ $teacher->rating }}</td>
                <td>{{ $teacher->user->name_kanji }}</td>
                <td>{{ $teacher->contact->email }}</td>
                <td>{{ $teacher->contact->mobile_email or '-' }}</td>
                <td>{{ $teacher->contact->phone_number or '-' }}</td>
                <td>{{ $teacher->user->login_credential->last_logged_in }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop