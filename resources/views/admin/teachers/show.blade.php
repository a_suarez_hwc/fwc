@extends('admin')

@section('content')
<div class="col-sm-3">
    <div class="text-center"><img class="profile-img" src="{{ asset($teacher->user->profile_image_path) }}"></div>
    <hr>
    <div class="list-group">
        <a class="list-group-item" href="{{ url('admin/users/'.$teacher->user->user_id) }}"><i class="fa fa-fw fa-user"></i> @lang('site.user') @lang('site.profile')</a>
        <a class="list-group-item" href="{{ url('admin/teachers/'.$teacher->teacher_id) }}"><i class="fa fa-fw fa-graduation-cap"></i> @lang('site.teacher') @lang('site.profile')</a>
        <a class="list-group-item" href="{{ url('admin/wallets/'.$teacher->user->wallet->wallet_id) }}"><i class="fa fa-fw fa-credit-card"></i> @lang('site.wallet')</a>
    </div>
    <hr>
</div>
<div class="col-sm-9">
    <div class="page-header"><h1>@lang('site.teacher') #{{ $teacher->teacher_id }}</h1></div>
    <div class="well well-sm admin-actions">
        <a href="{{ url('admin/teachers/'.$teacher->teacher_id.'/edit') }}" class="btn btn-default"><i class="fa fa-edit"></i> @lang('actions.edit')</a>
        <a href="{{ url('') }}" class="btn btn-danger btn-sm right"><i class="fa fa-trash"></i> @lang('actions.delete')</a>
    </div>
    <table class="table table-hover table-condensed table-striped">
        <caption>@lang('site.teacher') @lang('site.info')</caption>
        <tbody>
            <tr>
                <td>@lang('site.rating')</td>
                <td>@include('site.teachers.partials.rating', ['rating'=>$teacher->rating]) ({{ $teacher->rating }})</td>
            </tr>
            <tr>
                <td>@lang('site.years_of_industry_experience')</td>
                <td>{{ $teacher->years_of_industry_experience }}</td>
            </tr>
            <tr>
                <td>@lang('site.years_of_freelance_experience')</td>
                <td>{{ $teacher->years_of_freelance_experience }}</td>
            </tr>
            <tr>
                <td>@lang('site.skills')</td>
                <td>{{ $teacher->skills }}</td>
            </tr>
            <tr>
                <td>@lang('site.description')</td>
                <td>{{ $teacher->description }}</td>
            </tr>
            <tr>
                <td>@lang('site.certified_fields')</td>
                <td>
                    <ul>
                        @foreach($teacher->certified_fields as $field)
                        <li>{{ $field->field_text }}</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@stop