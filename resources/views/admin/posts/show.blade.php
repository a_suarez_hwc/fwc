@extends('admin')

@section('content')
<div class="col-sm-12">
    <div class="page-header"><h1>@lang('site.post') #{{ $post->post_id }}</h1></div>
    <div class="well well-sm admin-actions">
        <a href="{{ url('admin/posts/'.$post->post_id.'/edit') }}" class="btn btn-default"><i class="fa fa-fw fa-edit"></i> @lang('actions.edit')</a>
        <a href="" class="btn btn-danger btn-sm right"><i class="fa fa-fw fa-trash"></i> @lang('actions.delete')</a>
    </div>
    <table class="table table-hover">
        <tr>
            <td class="col-xs-2">@lang('site.user')</td>
            <td><a href="{{ url('admin/users/'.$post->user_id) }}">{{ $post->user->name_kanji }}</a></td>
        </tr>
        <tr>
            <td>@lang('site.teacher')</td>
            <td>
                @if($post->teacher_id)
                <a href="{{ url('admin/teachers/'.$post->teacher_id) }}">{{ $post->teacher->user->name_kanji }}</a>
                @else
                -
                @endif
            </td>
        </tr>
        <tr>
            <td>@lang('site.status')</td>
            <td>{{ $post->post_status->post_status_text }}</td>
        </tr>
        <tr>
            <td>@lang('site.price')</td>
            <td>{{ $post->price or '-' }}</td>
        </tr>
        <tr>
            <td>@lang('site.created_at')</td>
            <td>{{ $post->created_at }}</td>
        </tr>
        <tr>
            <td>@lang('site.updated_at')</td>
            <td>{{ $post->updated_at }}</td>
        </tr>
        <tr>
            <td>@lang('site.title')</td>
            <td>{{ $post->title }}</td>
        </tr>
        <tr>
            <td>@lang('site.body')</td>
            <td>{!! $post->body !!}</td>
        </tr>
        <tr>
            <td>@lang('site.solution')</td>
            <td>{!! $post->solution or '-' !!}</td>
        </tr>
    </table>
</div>
@stop
