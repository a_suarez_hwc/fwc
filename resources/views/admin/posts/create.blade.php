@extends('admin')

@section('content')
<div class="col-xs-12">
    <div class="page-header"><h1>Creating New @lang('site.post')</h1></div>
    
    {{ Form::open(['action'=>'Admin\PostController@store']) }}
    <div class="row">
        <div class="form-group col-sm-3 @if($errors->has("user_id")) has-error @endif">
            {{ Form::label('user_id', trans('site.id'), ['class'=>'control-label']) }}
            {{ Form::select('user_id', $users, null, ['placeholder'=>'Select user...', 'class'=>'form-control']) }}
            <div class="text-danger">{{ $errors->first('user_id') }}</div>
        </div>
        <div class="form-group col-sm-3 @if($errors->has("teacher_id")) has-error @endif">
            {{ Form::label('teacher_id', trans('site.teacher'), ['class'=>'control-label']) }}
            {{ Form::select('teacher_id', $teachers, null, ['placeholder'=>'Select teacher...', 'class'=>'form-control']) }}
            <div class="text-danger">{{ $errors->first('teacher_id') }}</div>
        </div>
        <div class="form-group col-sm-3 @if($errors->has("post_status_id")) has-error @endif">
            {{ Form::label('post_status_id', trans('site.status'), ['class'=>'control-label']) }}
            {{ Form::select('post_status_id', $statuses, null, ['class'=>'form-control']) }}
            <div class="text-danger">{{ $errors->first('post_status_id') }}</div>
        </div>
        <div class="form-group col-sm-3 @if($errors->has("target_date")) has-error @endif">
            {{ Form::label('target_date', trans('site.status'), ['class'=>'control-label']) }}
            <div class="input-group">
                {{ Form::text('target_date', null, ['class'=>'form-control js-datepicker']) }}
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            </div>
            <div class="text-danger">{{ $errors->first('target_date') }}</div>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-8 @if($errors->has("title")) has-error @endif">
            {{ Form::label('title', trans('site.title'), ['class'=>'control-label']) }}
            {{ Form::text('title', null, ['class'=>'form-control']) }}
            <div class="text-danger">{{ $errors->first('title') }}</div>
        </div>
        <div class="form-group col-sm-4 @if($errors->has("price")) has-error @endif">
            {{ Form::label('price', trans('site.price'), ['class'=>'control-label']) }}
            <div class="input-group">
                {{ Form::number('price', null, ['class'=>'form-control']) }}
                <div class="input-group-addon">P</div> 
            </div>
            <div class="text-danger">{{ $errors->first('price') }}</div>
        </div>
    </div>
    
    <div class="form-group @if($errors->has("body")) has-error @endif">
        {{ Form::label('body', trans('site.body'), ['class'=>'control-label']) }}
        <div class="text-danger">{{ $errors->first('body') }}</div>
        {{ Form::textarea('body', null, ['class'=>'form-control js-summernote']) }}
    </div>
    <div class="form-group @if($errors->has("solution")) has-error @endif">
        {{ Form::label('solution', trans('site.solution'), ['class'=>'control-label']) }}
        <div class="text-danger">{{ $errors->first('solution') }}</div>
        {{ Form::textarea('solution', null, ['class'=>'form-control js-summernote']) }}
    </div>
    <div class="right">
        <a href="{{ url('admin/posts') }}" class="btn btn-default">@lang('actions.cancel')</a>
        {{ Form::submit(trans('actions.save'), ['class'=> 'btn btn-default']) }}
    </div>
    {{ Form::close() }}
</div>
@include('site.posts.partials.summernote')
@stop