@extends('admin')

@section('content')
<div class="col-xs-12">
    <div class="page-header"><h1>@lang('site.posts')</h1></div>
    <div class="well well-sm admin-actions">
        <a href="{{ url('admin/posts/create') }}" class="btn btn-default"><i class="fa fa-plus"></i> @lang('actions.create')</a>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>@lang('site.id')</th>
                <th>@lang('site.user')</th>
                <th>@lang('site.teacher')</th>
                <th>@lang('site.status')</th>
                <th>@lang('site.solution')</th>
                <th>@lang('site.price')</th>
                <th>@lang('site.created_at')</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
            <tr class="js-table-row">
                <td>
                    <a class='table-row-link' href="{{ url('admin/posts/'.$post->post_id) }}">
                        {{ $post->post_id }}
                    </a>
                </td>
                <td>{{ $post->user_id }}</td>
                <td>{{ $post->teacher_id or '-'}}</td>
                <td>{{ $post->post_status->post_status_text }}</td>
                <td>{{ $post->solution ? trans('site.yes') : trans('site.no') }}</td>
                <td>{{ $post->price or '-' }}</td>
                <td>{{ $post->created_at }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop