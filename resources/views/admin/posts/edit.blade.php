@extends('admin')

@section('content')
<div class="col-xs-12">
    <div class="page-header"><h1>Editing @lang('site.post') #{{ $post->post_id }}</h1></div>
    {{ Form::model($post, ['action'=>['Admin\PostController@update', $post->post_id],'method'=>'PUT']) }}
    <div class="row">
        <div class="form-group col-sm-3">
            {{ Form::label('post_id', trans('site.id'), ['class'=>'control-label']) }}
            <div class="form-control">{{ $post->post_id }}</div>
        </div>
        <div class="form-group col-sm-3 @if($errors->has("user_id")) has-error @endif">
            {{ Form::label('user_id', trans('site.user'), ['class'=>'control-label']) }}
            {{ Form::select('user_id', $users, null, ['placeholder'=>'Select user...', 'class'=>'form-control']) }}
            <div class="text-danger">{{ $errors->first('user_id') }}</div>
        </div>
        <div class="form-group col-sm-3 @if($errors->has("teacher_id")) has-error @endif">
            {{ Form::label('teacher_id', trans('site.teacher'), ['class'=>'control-label']) }}
            {{ Form::select('teacher_id', $teachers, null, ['placeholder'=>'Select teacher...', 'class'=>'form-control']) }}
            <div class="text-danger">{{ $errors->first('teacher_id') }}</div>
        </div>
        <div class="form-group col-sm-3 @if($errors->has("post_status_id")) has-error @endif">
            {{ Form::label('post_status_id', trans('site.status'), ['class'=>'control-label']) }}
            {{ Form::select('post_status_id', $statuses, null, ['class'=>'form-control']) }}
            <div class="text-danger">{{ $errors->first('post_status_id') }}</div>
        </div>
    </div>
    <div class="form-group @if($errors->has("title")) has-error @endif">
        {{ Form::label('title', trans('site.title'), ['class'=>'control-label']) }}
        <div class="text-danger">{{ $errors->first('title') }}</div>
        {{ Form::text('title', null, ['class'=>'form-control']) }}
    </div>
    <div class="form-group @if($errors->has("body")) has-error @endif">
        {{ Form::label('body', trans('site.body'), ['class'=>'control-label']) }}
        <div class="text-danger">{{ $errors->first('body') }}</div>
        {{ Form::textarea('body', null, ['class'=>'form-control js-summernote']) }}
    </div>
    <div class="form-group @if($errors->has("solution")) has-error @endif">
        {{ Form::label('solution', trans('site.solution'), ['class'=>'control-label']) }}
        <div class="text-danger">{{ $errors->first('solution') }}</div>
        {{ Form::textarea('solution', null, ['class'=>'form-control js-summernote']) }}
    </div>
    <div class="right">
        <a href="{{ url('admin/posts/'.$post->post_id) }}" class="btn btn-default">@lang('actions.cancel')</a>
        {{ Form::submit(trans('actions.save'), ['class'=> 'btn btn-default']) }}
    </div>
    {{ Form::close() }}
</div>
@include('site.posts.partials.summernote')
@stop