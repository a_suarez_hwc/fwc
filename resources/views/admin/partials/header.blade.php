<header>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#admin-navbar" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('admin') }}">FWC @lang('site.admin')</a>
			</div>

			<div class="collapse navbar-collapse" id="admin-navbar">
				<ul class="nav navbar-nav">
					<li>
						<a href="{{ url('admin/posts') }}">@lang('site.posts')</a>
					</li>
					<li>
						<a href="{{ url('admin/users') }}">@lang('site.users')</a>
					</li>
					<li>
						<a href="{{ url('admin/teachers') }}">@lang('site.teachers')</a>
					</li>
					<li>
						<a href="{{ url('admin/wallets') }}">@lang('site.wallets')</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="{{ url('/') }}"><i class="fa fa-fw fa-angle-left"></i> FWC</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>