@extends('admin')
@section('content')
<div class="col-xs-12">
    <div class="page-header"><h1>@lang('site.wallets')</h1></div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>@lang('site.id')</th>
                <th>@lang('site.user')</th>
                <th>@lang('site.amount')</th>
            </tr>
        </thead>
        <tbody>
            @foreach($wallets as $wallet)
            <tr class="js-table-row">
                <td>
                    <a class="table-row-link" href="{{ url('admin/wallets/'.$wallet->wallet_id) }}">
                        {{ $wallet->wallet_id }}
                    </a>
                </td>
                <td>{{ $wallet->user->name_kanji }}</td>
                <td>{{ $wallet->points }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop
