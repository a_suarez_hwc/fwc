@extends('admin')

@section('content')
@include('admin.wallets.addAdjustment', ['wallet'=>$wallet])
<div class="col-sm-3">
	<div class="text-center"><img class="profile-img" src="{{ asset($wallet->user->profile_image_path) }}"></div>
	<hr>
	<div class="list-group">
		<a class="list-group-item" href="{{ url('admin/users/'.$wallet->user->user_id) }}"><i class="fa fa-fw fa-user"></i> @lang('site.user') @lang('site.profile')</a>
		@if($wallet->user->teacher)
		<a class="list-group-item" href="{{ url('admin/teachers/'.$wallet->user->teacher->teacher_id) }}"><i class="fa fa-fw fa-graduation-cap"></i> @lang('site.teacher') @lang('site.profile')</a>
		@endif
		<a class="list-group-item" href="{{ url('admin/wallets/'.$wallet->user->wallet->wallet_id) }}"><i class="fa fa-fw fa-credit-card"></i> @lang('site.wallet')</a>
	</div>
	<hr>
</div>
<div class="col-sm-9">
	<div class="page-header"><h1>@lang('site.wallet') #{{ $wallet->wallet_id }}</h1></div>
	<div class="well well-sm admin-actions">
		<button type="button" class="btn btn-default" data-toggle="modal" data-target="#adminAddAdjustment">
			<i class="fa fa-plus"></i> Add Adjustment
		</button>
	</div>
	<div>
		<p><strong>@lang('site.points'):</strong> {{ $wallet->points }}P</p>
	</div>

	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>@lang('site.date')</th>
					<th>@lang('site.type')</th>
					<th>@lang('site.amount')</th>
					<th>@lang('site.new_balance')</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($tickets as $key=>$ticket)
				@if (!empty($ticket->transaction_id))
				<!-- Added Points-->
				@if ($ticket->transaction_type_id == '1')
				<tr>
					<td>{{ date('y年m月j日 - G:i',strtotime($ticket->created_at)) }}</td>
					<td>Added Points</td>
					<td><div class="label label-success ">+{{ $ticket->points }}P</div></td>
					<td>{{ $ticket->new_wallet_balance }}P</td>
				</tr>
				@endif

				<!-- Cashed Out -->
				@if ($ticket->transaction_type_id == '2')
				<tr>
					<td>{{ date('y年m月j日 - G:i',strtotime($ticket->created_at)) }}</td>
					<td>Cashed Out</td>
					<td><div class="label label-danger ">-{{ $ticket->points }}P</div></td>
					<td>{{ $ticket->new_wallet_balance }}P</td>
				</tr>
				@endif


				@if ($ticket->transaction_type_id == '3')
				<tr>
					<td>{{ date('y年m月j日 - G:i',strtotime($ticket->created_at)) }}</td>
					<td>Adjustment</td>
					<td>
						@if ((float)$ticket->points > 0)
						<div class="label label-warning ">+{{ $ticket->points }}P</div>
						@else
						<div class="label label-warning ">{{ $ticket->points }}P</div>
						@endif
					</td>
					<td>{{ $ticket->new_wallet_balance }}P</td>
				</tr>
				@endif
				@endif

				@if (!empty($ticket->exchange_id))
				<!-- Asked Question -->
				@if ($ticket->paying_wallet_id == $wallet->wallet_id)
				<tr>
					<td>{{ date('y年m月j日 - G:i',strtotime($ticket->created_at)) }}</td>
					<td>Asked Question</td>
					<td><div class="label label-danger ">-{{ $ticket->points_paid }}P</div></td>
					<td>{{ $ticket->new_paying_wallet_balance }}P</td>
				</tr>
				@endif

				<!-- Solved Question -->
				@if ($ticket->receiving_wallet_id == $wallet->wallet_id)
				<tr>
					<td>{{ date('y年m月j日 - G:i',strtotime($ticket->created_at)) }}</td>
					<td>Solved Question</td>
					<td><div class="label label-success ">+{{ $ticket->points_received }}P</div></td>
					<td>{{ $ticket->new_receiving_wallet_balance }}P</td>
				</tr>
				@endif
				@endif
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop