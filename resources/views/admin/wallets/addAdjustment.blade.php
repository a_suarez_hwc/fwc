<div aria-labelledby="Add Adjustment" class="modal fade" id="adminAddAdjustment" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="panel panel-default">
			<div class="panel-heading">
				<button aria-label="Close" class="close" data-dismiss="modal" type="button">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="add-points-modal-label">Add Adjustment</h4>
			</div>
			<div class="panel-body">
				{{ Form::open(['action' => ['Admin\WalletController@addAdjustment', $wallet->wallet_id]]) }}
				<div class="form-group">
					<div class="row">
						<div class="col-xs-12">
							<label for="points">
								@lang('site.points')
							</label>
						</div>
						<div class="col-sm-4">
							<div class="input-group">
								{{ Form::number('points', null, ['class' => 'form-control']) }}
								<div class="input-group-addon">P</div>
							</div>
						</div>
					</div>
				</div>
				<button class="btn btn-primary" type="submit">
					Add Adjustment
				</button>
				<button aria-label="Close" class="btn btn-default" data-dismiss="modal" type="button">
					@lang('actions.cancel')
				</button>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
