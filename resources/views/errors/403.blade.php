@extends('site')

@section('head')
<style>
	img {
		max-width: 30em;
		margin: 0 auto;
		display: block;
	}
	.vc-container {
		min-height: 100vh;
		position: relative;
	}
	.vc {
		position: absolute;
		top: 50%;
		left: 50%;
		-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}
</style>
@stop

@section('content')

<div class="col-md-10 col-md-offset-1">
	<h2 class="text-center">403 Error:<br><small>Access Denied</small></h2>
	{{ Html::image('img/403.jpg') }}
</div>

@endsection