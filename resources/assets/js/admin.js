(function($, window, document) {
    "use strict";

    $(function() {
        $('.js-table-row').on('mouseover', function(e) {
            $(this).find('.js-table-actions').css('opacity', 1);
        });

        $('.js-table-row').on('mouseout', function(e) {
            $(this).find('.js-table-actions').css('opacity', 0);
        });

        if ($('.js-checkbox-is-teacher:checked').length > 0) {
            console.log('hi');
            $('.js-user-create-teacher').css('display', 'block');
        }

        $('.js-checkbox-is-teacher').on('change', function() {
            var $teacher = $('.js-user-create-teacher');
            if (this.checked) {
                $teacher.slideDown();
            } else {
                $teacher.slideUp();
            }
        });
    });
}(window.jQuery, window, document));