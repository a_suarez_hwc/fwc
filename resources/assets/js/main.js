(function($, window, document) {
    "use strict";

    // Execute code after DOM load
    $(function() {
        // Prevent disabled anchors from following href link
        $('.a--disabled').on('click', function(e) {
            e.preventDefault();
        });

        // active bootstrap popovers
        $('[data-toggle="popover"]').popover()

        $('.js-datepicker').datepicker({
            language: 'ja',
            widgetPositioning: {
                vertical: 'top',
                horizontal: 'left'
            }
        });

        // Toggling responsive navigation for mobile
        var $navToggle = $('#js-nav-main-toggle');
        slideOnClick($navToggle, $('.nav-main'), 'nav-main__toggle--active');

        // Javascript to enable link to tab
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        }

        // Change hash for page-reload
        $('.nav-tabs a').on('shown.bs.tab', function(e) {
            window.location.hash = e.target.hash;
            window.scrollTo(0, 0);
        });

        // On scrolltop button click, scroll to top
        var $scrolltop = $('#js-scrolltop');
        $scrolltop.on('click', function(e) {
            $('html, body').animate({
                scrollTop: 0
            }, 'fast');
        });

        // Toggle scrolltop button based on location in window
        toggleOnScroll($scrolltop);

        // functions that execute on window scroll
        // must be throttled using throttle() function below
        $(window).scroll(function() {
            t_toggleOnScroll($scrolltop)
        });
    });

    // toggle element view based on distance from top of window
    function toggleOnScroll(element, distance = 200) {
        if ($(window).scrollTop() < distance) {
            if (element.css('display') === 'block') element.fadeOut();
        } else {
            if (element.css('display') === 'none') element.fadeIn();
        }
    };
    var t_toggleOnScroll = throttle(toggleOnScroll, 275);

    // slide targetEl in or out of view when clicking on toggleEl
    function slideOnClick(toggleEl, targetEl, toggleClass = 'active') {
        toggleEl.on('click', function(e) {
            if (toggleEl.hasClass(toggleClass)) {
                targetEl.slideUp();
            } else {
                targetEl.slideDown();
            }
            toggleEl.toggleClass(toggleClass);
            e.preventDefault();
        });
    }

    // throttle function to execute once an interval
    // taken from Underscore.js' implementation of throttle
    function throttle(func, wait, options) {
        var context, args, result;
        var timeout = null;
        var previous = 0;
        if (!options) options = {};
        var later = function() {
            previous = options.leading === false ? 0 : new Date().getTime();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        };
        return function() {
            var now = new Date().getTime();
            if (!previous && options.leading === false) previous = now;
            var remaining = wait - (now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);
                if (!timeout) context = args = null;
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        };
    };
}(window.jQuery, window, document));