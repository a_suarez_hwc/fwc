<?php
return [
    'filter'        => 'Filter',
    'create'        => 'Create',
    'edit'          => 'Edit',
    'delete'        => 'Delete',
    'ask_question'  => 'Ask Question',
    'add_points'    => 'Add Points',
    'redeem_points' => 'Redeem Points',
    'cancel'        => 'Cancel',
    'submit'        => 'Submit',
    'save'          => 'Save',
    'update'        => 'Update',
];
