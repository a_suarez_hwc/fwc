<?php
return [
    'all'      => 'All',
    'open'     => 'Open',
    'pending'  => 'Pending',
    'progress' => 'In Progress',
    'solved'   => 'Solved',
];
