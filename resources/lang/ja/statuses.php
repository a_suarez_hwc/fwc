<?php
return [
    'all'      => '全部',
    'open'     => '公然',
    'pending'  => '未解決',
    'progress' => '進行中',
    'solved'   => '解ける',
];
