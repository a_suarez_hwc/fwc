<?php
return [
    'filter'        => '濾す',
    'create'        => '作る',
    'edit'          => '編集',
    'delete'        => '消す',
    'ask_question'  => '質問をする',
    'add_points'    => '足す',
    'redeem_points' => '買い戻す',
    'cancel'        => 'キャンセル',
    'submit'        => '提出',
    'save'          => '保存する',
    'update'        => '変更',
];
