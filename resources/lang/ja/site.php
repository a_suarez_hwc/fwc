<?php

return [
    'title'                         => 'FWC',
    'forum'                         => 'フォーラム',
    'chat'                          => 'トーク',
    'events'                        => '催し',
    'people'                        => '人民',
    'profile'                       => 'プロフィール',
    'settings'                      => '設定',
    'about'                         => '会社案内',
    'admin'                         => 'アドミン',
    'login'                         => 'ログイン',
    'logout'                        => 'ログアウト',
    'password'                      => 'パスワード',
    'confirm_password'              => 'パスワード確認',
    'forgot_password'               => 'パスワードの再設定',
    'register'                      => '会員登録',
    'remember_me'                   => '覚えています',
    'my_questions'                  => '私の質問',
    'my_lessons'                    => '私の授業',
    'ask_question'                  => '質問する',
    'question'                      => '質問',
    'questions'                     => '質問',
    'applications'                  => '申請',
    'post'                          => 'ポスト',
    'posts'                         => 'ポスト',
    'user'                          => 'ユーザー',
    'users'                         => 'ユーザー',
    'teacher'                       => '先生',
    'teachers'                      => '先生',
    'wallet'                        => '財布',
    'wallets'                       => '財布',
    'id'                            => 'イド',
    'status'                        => '状態',
    'tag'                           => '荷札',
    'tags'                          => '荷札',
    'yes'                           => 'はい',
    'no'                            => 'いいえ',
    'price'                         => '価格',
    'created_at'                    => '作成日',
    'updated_at'                    => '日更新',
    'title'                         => '題名',
    'body'                          => '本文',
    'solution'                      => '解答',
    'profile'                       => 'プロフィール',
    "name"                          => "氏名",
    "name_kana"                     => "氏名（カナ）",
    "first_name_kanji"              => "名",
    "family_name_kanji"             => "姓",
    "first_name_kana"               => "めい",
    "family_name_kana"              => "せい",
    'email'                         => 'メールアドレス',
    'login_email'                   => 'ログインメールアドレス',
    'mobile_email'                  => '携帯メールアドレス',
    'last_logged_in'                => '最後にログイン',
    'login_credentials'             => 'ログインの詳細',
    'contact'                       => '連絡先編集',
    'info'                          => '基本情報編集',
    'profile_image_path'            => 'プロフィール画像',
    'profile_image_thumbnail_path'  => 'プロフィールサムネール',
    'skype'                         => 'Skype名',
    'phone_number'                  => '携帯番号',
    'rating'                        => '格付け',
    'years_of_industry_experience'  => '現場経験年数',
    'years_of_freelance_experience' => 'フリー経験年数',
    'skills'                        => '芸域',
    'description'                   => '記述',
    'certified_fields'              => '認証',
    'transaction'                   => 'トランザクション',
    'transactions'                  => 'トランザクション',
    'exchange'                      => '交換',
    'exchanges'                     => '交換',
    'date'                          => '日付',
    'type'                          => '型',
    'points'                        => 'ポイント',
    'amount'                        => '分量',
    'balance'                       => 'バランス',
    'new_balance'                   => '新しいバランス',
    'receipt'                       => '受取り',
];
