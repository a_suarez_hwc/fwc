<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /**
     * Test login with premade user account
     * @return void
     */
    public function testLoginPremadeUser()
    {
    	$this->visit('/login')
	    	->type('user@fwc.com', 'email')
	    	->type('user','password')
	    	->press('Login')
	    	->seePageIs('/posts')
	    	->visit('/logout');
    }

    /**
     * Test login with premade teacher account
     * @return void
     */
    public function testLoginPremadeTeacher()
    {
    	$this->visit('/login')
	    	->type('teacher@fwc.com', 'email')
	    	->type('teacher','password')
	    	->press('Login')
	    	->seePageIs('/posts')
	    	->visit('/logout');
    }

    /**
     * Test register and login with new user
     * @return void
     */
    public function testUserRegister()
    {
    	$this->visit('/register')
    		->type('a@a.com', 'email')
    		->type('a','password')
    		->type('a','password_confirmation')
    		->press('Register')
    		->visit('/login')
    		->type('a@a.com', 'email')
    		->type('a', 'password')
    		->press('Login')
    		->seePageIs('/posts')
    		->visit('/logout');
    }
}
