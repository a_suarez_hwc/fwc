<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Post;
use App\Models\User;
use App\Models\Teacher;

class PostTest extends TestCase
{
	protected $post;
	protected $user;
	protected $teacher;

    /**
     * Test post creation
     * @return void
     */
    public function testCreateNewPost()
    {
    	$this->user = User::find(1);
        $this->visit('/login')
	    	->type('user@fwc.com', 'email')
	    	->type('user','password')
	    	->press('Login')
	    	->seePageIs('/posts')
        	->type('Test','new-question-title')
        	->type('Text', 'new-question-text')
        	->check('tags[1]')
        	->press('Submit')
        	->seeInDatabase('post', ['title' => 'Test', 'body' => 'Text']);
    }

    public function testApplyPost()
    {
    	$this->post = Post::orderBy('date_created','desc')->first();
    	$this->teacher = Teacher::first();

    	$this->visit('/login')
	    	->type('teacher@fwc.com', 'email')
	    	->type('teacher','password')
	    	->press('Login')
	    	->seePageIs('/posts')
	    	->visit('/posts/'.$this->post->post_id)
	    	->press('Apply to Teach')
	    	->seeInDatabase('teacher_to_post',['teacher_id'=>$this->teacher->teacher_id, 'post_id'=>$this->post->post_id]);
    }

    // public function testApproveTeacher()
    // {
    // 	$this->visit('/login')
	   //  	->type('user@fwc.com', 'email')
	   //  	->type('user','password')
	   //  	->press('Login')
	   //  	->visit('/users/'.$this->user->user_id.'/questions');
    // }
}
