<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\Post;
use App\Models\Teacher;
use App\Models\User;
use App\Models\Contact;
use Mail;

class SendSolutionPostedEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $post;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $mailer->send('emails.solution-posted', ['post'=>$this->post], function($message)
        {
            $message->to($this->post->user->contact->email);
            $message->sender('fowaco.test@gmail.com');
            $message->subject("Your teacher has submitted a solution!");
        });
    }
}
