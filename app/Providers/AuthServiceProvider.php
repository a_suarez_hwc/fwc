<?php

namespace App\Providers;

use App\Models\Post;
use App\Models\Teacher;
use App\Models\User;
use App\Models\Wallet;
use App\Policies\Site\PostPolicy;
use App\Policies\Site\TeacherPolicy;
use App\Policies\Site\UserPolicy;
use App\Policies\Site\WalletPolicy;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model'    => 'App\Policies\ModelPolicy',
        Post::class    => PostPolicy::class,
        User::class    => UserPolicy::class,
        Teacher::class => TeacherPolicy::class,
        Wallet::class  => WalletPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //Is a user
        $gate->define('logged-in', function ($user) {
            return $user !== null;
        });

        $gate->define('is-admin', function ($user) {
            return is_admin($user);
        });

        //Is a teacher
        $gate->define('is-teacher', function ($user) {
            return isTeacher($user);
        });
    }
}
