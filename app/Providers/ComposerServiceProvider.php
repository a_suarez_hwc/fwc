<?php

namespace App\Providers;

use App\Models\TagGroup;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        view()->composer('site.posts.partials.filters', function ($view) {
            $groups = TagGroup::all();
            $view->with('groups', $groups);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
