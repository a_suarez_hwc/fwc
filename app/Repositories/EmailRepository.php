<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Models\Teacher;
use App\Models\User;
use Mail;

class EmailRepository
{
    public function sendPostFinalized($post)
    {
        Mail::send('emails.post-finalized', ['post' => $post, 'student' => 1], function ($message) use ($post) {
            $message->to($post->user->contact->email);
            $mobile = $post->user->contact->mobile_email;
            if (!is_null($mobile) && !empty($mobile)) {
                $message->cc($mobile);
            }
            $message->subject("Your post has been concluded");
            $message->from('fowaco.test@gmail.com', 'Your post has been concluded');
        });
        Mail::send('emails.post-finalized', ['post' => $post, 'student' => 0], function ($message) use ($post) {
            $message->to($post->teacher->user->contact->email);
            $mobile = $post->teacher->user->contact->mobile_email;
            if (!is_null($mobile) && !empty($mobile)) {
                $message->cc($mobile);
            }
            $message->subject("Your lesson has concluded");
            $message->from('fowaco.test@gmail.com', 'Your lesson has concluded');
        });
    }

    public function sendSolutionPosted($post)
    {
        Mail::send('emails.solution-posted', ['user' => $post->user, 'teacher' => $post->teacher, 'post' => $post], function ($message) use ($post) {
            $message->to($post->user->contact->email);
            $mobile = $post->user->contact->mobile_email;
            if (!is_null($mobile) && !empty($mobile)) {
                $message->cc($mobile);
            }
            $message->subject("Your teacher has submitted a solution!");
            $message->from('fowaco.test@gmail.com', 'Your teacher has submitted a solution!');
        });
    }

    public function sendSolutionRequested($post)
    {
        Mail::send('emails.solution-requested', ['post' => $post], function ($message) use ($post) {
            $message->to($post->teacher->user->contact->email);
            $mobile = $post->teacher->user->contact->mobile_email;
            if (!is_null($mobile) && !empty($mobile)) {
                $message->cc($mobile);
            }
            $message->subject("Your student has requested a solution");
            $message->from('fowaco.test@gmail.com', 'Your student has requested a solution');
        });
    }

    public function sendTeacherChosen($post)
    {
        Mail::send('emails.teacher-chosen', ['post' => $post], function ($message) use ($post) {
            $message->to($post->teacher->user->contact->email);
            $mobile_t = $post->teacher->user->contact->mobile_email;
            if (!is_null($mobile_t) && !empty($mobile_t)) {
                $message->cc($mobile_t);
            }
            $message->cc($post->user->contact->email);
            $mobile = $post->user->contact->mobile_email;
            if (!is_null($mobile) && !empty($mobile)) {
                $message->cc($mobile);
            }
            $message->subject("You've been accepted to teach a lesson!");
            $message->from('fowaco.test@gmail.com', "You've been accepted to teach a lesson!");
        });
    }

    public function sendPriceAdjusted($post)
    {
        Mail::send('emails.price-adjusted', ['post' => $post], function ($message) use ($post) {
            $message->to($post->user->contact->email);
            $mobile = $post->user->contact->mobile_email;
            if (!is_null($mobile) && !empty($mobile)) {
                $message->cc($mobile);
            }
            $message->subject("An application price has been updated!");
            $message->from('fowaco.test@gmail.com', "An application price has been updated!");
        });
    }
}
