<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Models\LoginCredential;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Teacher;

class UserRepository
{
    /**
     * Contact instance
     * @var App\Models\Contact
     */
    protected $contact;

    /**
     * LoginCredential instance
     * @var App\Models\LoginCredential
     */
    protected $login_credential;

    /**
     * Wallet instance
     * @var App\Models\Wallet
     */
    protected $wallet;

    /**
     * Create a new UserRepository instance
     * @param User            $user
     * @param Contact         $contact
     * @param LoginCredential $LoginCredential
     * @param Wallet           $wallet
     * @return  void
     */
    public function __construct(
    	User $user,
    	Contact $contact,
    	LoginCredential $loginCred,
    	Wallet $wallet) {
    	$this->model     = $user;
    	$this->contact   = $contact;
    	$this->loginCred = $loginCred;
    	$this->wallet    = $wallet;
    }

    /**
     * Save the user
     * TODO: make operations atomic
     *
     * @param  Array $inputs
     * @return void
     */
    public function save($inputs)
    {
    	$user      = new $this->model;
    	$contact   = new $this->contact;
    	$loginCred = new $this->loginCred;
    	$wallet    = new $this->wallet;

    	$imageName                          = 'default-' . mt_rand(1, 10) . '.jpg';
    	$user->profile_image_path           = 'img/profiles/' . $imageName;
    	$user->profile_image_thumbnail_path = 'img/thumbs/' . $imageName;
    	$user->save();

    	$contact->email   = $inputs['email'];
    	$contact->user_id = $user->user_id;
    	$contact->user()->associate($user);

    	$loginCred->email = $inputs['email'];
    	$loginCred->password    = bcrypt($inputs['password']);
    	$loginCred->user_id     = $user->user_id;
    	$loginCred->user()->associate($user);

    	$wallet->user_id = $user->user_id;
    	$wallet->points  = 0;
    	$wallet->user()->associate($user);

    	$contact->save();
    	$loginCred->save();
    	$wallet->save();
    }

    public function admin_store($request)
    {
    	$user      = new $this->model;
    	$contact   = new $this->contact;
    	$loginCred = new $this->loginCred;
    	$wallet    = new $this->wallet;


    	$imageName                          = 'default-' . mt_rand(1, 10) . '.jpg';
    	$user->profile_image_path           = 'img/profiles/' . $imageName;
    	$user->profile_image_thumbnail_path = 'img/thumbs/' . $imageName;
    	$user->save();
    	$user->update($request->all());

    	if($request->is_teacher == "on") {
    		$teacher = $user->teacher()->create($request->all());
    		if (is_null($request->certified_fields)) {
    			$request->certified_fields = [];
    		}
    		$teacher->certified_fields()->sync($request->certified_fields);
    	}

    	$contact = $user->contact()->create($request->contact);

    	$loginCred->email = $request->login_credential['email'];
    	$loginCred->password    = bcrypt($request->login_credential['password']);
    	$loginCred->status      = $request->login_credential['status'];
    	$loginCred->user_id     = $user->user_id;
    	$loginCred->user()->associate($user);
    	$loginCred->save();

    	$wallet->user_id = $user->user_id;
    	$wallet->points  = 0;
    	$wallet->user()->associate($user);
    	$wallet->save();


    }
}
