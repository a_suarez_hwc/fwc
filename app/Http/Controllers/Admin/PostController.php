<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Post\StoreRequest;
use App\Http\Requests\Admin\Post\UpdateRequest;
use App\Models\Post;
use App\Models\PostStatus;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('admin.posts.index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = Teacher::pluck('teacher_id', 'teacher_id')->sort();
        $teachers->transform(function ($teacher_id) {
            return Teacher::find($teacher_id)->user->name_kanji;
        });
        $users = User::pluck('user_id', 'user_id');
        $users->transform(function ($user_id) {
            return User::find($user_id)->name_kanji;
        });
        $statuses = PostStatus::pluck('post_status_text', 'post_status_id');
        return view('admin.posts.create', ['users' => $users, 'teachers' => $teachers, 'statuses' => $statuses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Post::create($request->all());
        return redirect('admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post     = Post::findOrFail($id);
        $teachers = Teacher::pluck('teacher_id', 'teacher_id')->sort();
        $teachers->transform(function ($teacher_id) {
            return Teacher::find($teacher_id)->user->name_kanji;
        });
        $users = User::pluck('user_id', 'user_id');
        $users->transform(function ($user_id) {
            return User::find($user_id)->name_kanji;
        });
        $statuses = PostStatus::pluck('post_status_text', 'post_status_id');
        return view('admin.posts.edit', ['post' => $post, 'statuses' => $statuses, 'users' => $users, 'teachers' => $teachers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->update($request->all());
        return redirect('admin/posts/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
