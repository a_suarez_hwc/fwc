<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Wallet;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wallets = Wallet::all();
        return view('admin.wallets.index', ['wallets' => $wallets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/wallets/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $wallet          = Wallet::findOrFail($id);
        $current_balance = $wallet->points;

        // get transaction data & sort
        $transactions = $wallet->transactions;

        // get exchange data & sort
        $incomes  = $wallet->receivementExchanges;
        $payments = $wallet->paymentExchanges;

        // merge all transactions and sort
        $all            = $transactions->merge($incomes)->merge($payments);
        $sorted_tickets = $all->sortBy('created_at');

        return view('admin.wallets.show', [
            'wallet'          => $wallet,
            'current_balance' => $current_balance,
            'tickets'         => $sorted_tickets,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addAdjustment(Request $request, $id)
    {
        $wallet                           = Wallet::findOrFail($id);
        $request['transaction_status_id'] = 2;
        $request['transaction_type_id']   = 3;
        $request['new_wallet_balance']    = $wallet->points + (float) $request->points;

        $t              = $wallet->transactions()->create($request->all());
        $wallet->points = $t->new_wallet_balance;
        $wallet->save();
        return redirect()->back()->with('success');
    }
}
