<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\Posts\UpdateRequest;
use App\Models\Post;
use App\Models\PostToTag;
use Auth;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{
    /**
     * Display a listing of all posts.
     * Authorization : None
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postsPerPage = 10;
        $query        = Post::query();
        $tags         = Input::get('tags');
        $status       = Input::get('status');

        // filter by tag
        if (!empty($tags)) {
            $posts = Post::all();
            $query->where(function ($query) use ($tags, $posts) {
                foreach ($posts as $post) {
                    $postTags = PostToTag::where('post_id', $post->post_id)->get();

                    // execute when a post id matches filter tag id
                    if (!empty(array_intersect($tags, array_column($postTags->toArray(), 'tag_id')))) {
                        $query->orWhere('post_id', $post->post_id);
                    }
                }
            });
        }

        // filter by status
        if (!empty($status)) {
            if ($status === 2) {
                $query->where(function ($query) use ($status) {
                    $query->orWhere('post_status_id', 3);
                    $query->orWhere('post_status_id', 4);
                    $query->orWhere('post_status_id', 5);
                });
            }
            $query->where('post_status_id', $status);
        }

        $posts = $query->orderBy('created_at', 'desc')->paginate($postsPerPage);

        return view('site.posts.index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // N/A
    }

    /**
     * Store a newly created post in the post and post_to_tag tables.
     * Authorization : User
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO Make middleware protection
        if (Gate::denies('logged-in')) {
            abort(403);
        }

        $this->validate($request, [
            'title' => 'required',
            'body'  => 'required',
            'tags'  => 'required',
        ]);

        $user        = Auth::user()->user_id;
        $now         = Carbon::now();
        $target_date = !empty($request->target_date) ? Carbon::createFromFormat('Y/m/d', $request->target_date) : null;

        $post                 = new Post;
        $post->user_id        = $user;
        $post->post_status_id = 1;
        $post->title          = $request->title;
        $post->body           = $request->body;
        $post->price          = !empty($request->price) ? $request->price : null;
        $post->target_date    = $target_date;
        $post->save();

        // TODO: enforce checkbox requirement
        $checkboxes = Input::get('tags');
        foreach ($checkboxes as $tag) {
            PostToTag::create([
                'post_id' => $post->post_id,
                'tag_id'  => $tag,
            ]);
        }

        return redirect('/users/' . $user . '/questions')->with('ok', "Post submitted!");
    }

    /**
     * Display the specified post.
     * Authorization : None
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post    = Post::findOrFail($id);
        $teacher = ($post->teacher) ? $post->teacher : null;

        return view('site.posts.show', [
            'post'    => $post,
            'user'    => $post->user,
            'teacher' => $teacher]);
    }

    /**
     * Show the form for editing the specified post.
     * Authorization : Admin, Owner
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $this->authorize('update', $post);
        return view('site.posts.edit', ['post' => $post]);
    }

    /**
     * Update the specified post in post and post_to_tag tables.
     * Authorization : Admin, Owner
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $post = Post::findOrFail($id);
        $this->authorize('update', $post);
        $post->update($request->all());
        $post->tags()->sync($request->tags, false);
        return redirect('/posts/' . $id)->with('ok', 'Post succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // N/A
    }

    public function cancel($id)
    {
        $post    = Post::findOrFail($id);
        $teacher = $post->teacher;
        $post->teacher()->dissociate();
        $post->post_status_id = 1;
        $post->save();
        return redirect()->back()->with('ok', 'Lesson has been cancelled');
    }
}
