<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
// use Validator;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
     */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/posts';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Handle a login request
     * @param  App\Http\Requests\LoginRequest $request
     * @param  Guard        $auth
     * @return Response
     */
    public function postLogin(LoginRequest $request, Guard $auth)
    {
        $credentials = [
            'email'    => $request->input('email'),
            'password' => $request->input('password'),
        ];

        if (!$auth->validate($credentials)) {
            return redirect('/login')->with('error', 'Credentials do not match our records.');
        }

        $user = $auth->getLastAttempted();
        if ($user) {
            $auth->login($user);
            if ($user->is_admin) {
                return redirect('/admin');
            }
            return redirect('/posts')->with('ok', 'Welcome!');
        }
        return redirect('/login')->with('ok', $user->user_id);
    }

    /**
     * Handle a logout request
     * @param  Guard  $auth
     * @return [type]
     */
    public function logout(Guard $auth)
    {
        Auth::logout();
        // Session::flush();
        return redirect('/login')->with('ok', 'Successfully logged out');
    }

    /**
     * Handle a register request
     * @param  App\Http\Requests\RegisterRequest $request
     * @param  App\Repositories\UserRepository  $user_repo
     * @return Response
     */
    public function postRegister(RegisterRequest $request, UserRepository $user_repo)
    {
        $user = $user_repo->save(
            $request->all()
        );

        return redirect('/login')->with('ok', 'Account successfully created! Please log in.');
    }
}
