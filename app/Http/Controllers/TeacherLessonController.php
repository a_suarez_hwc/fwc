<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Teacher;
use App\Models\TeacherToPost;
use Auth;
use Illuminate\Http\Request;

class TeacherLessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $tid
     * @return \Illuminate\Http\Response
     */
    public function index($tid)
    {
        $teacher = Teacher::findOrFail($tid);
        $this->authorize('accessTeacherProfile', $teacher);

        // lessons applied: get array from TtP, pluck postId to search them in Post tbl.
        $lessons               = Post::where('teacher_id', $tid)->get();
        $lessons_applied_array = TeacherToPost::where('teacher_id', $tid)->get();

        // pending lessons exist
        if (!empty($lessons_applied_array)) {
            // to use in my-lesson view 'if' for printing
            $pendingexists = true;

            $lessons_applied_postids = $lessons_applied_array->pluck('post_id');
            $lessons_applied         = Post::whereIn('post_id', $lessons_applied_postids)->get();

            $lessons_inprogress = Post::where('teacher_id', $tid)
                ->where(function ($query) {
                    $query->where('post_status_id', 2)
                        ->orWhere('post_status_id', 4)
                        ->orWhere('post_status_id', 5);
                })
                ->get();
            $lessons_solved = $lessons->where('post_status_id', 3);

            return view('site.posts.lessons.index', [
                'pendingexists'      => $pendingexists,
                'lessons_applied'    => $lessons_applied,
                'lessons_inprogress' => $lessons_inprogress,
                'lessons_solved'     => $lessons_solved,
            ]);
        }
        // no pending lessons exist
        else {
            $pendingexists = false;

            $lessons_inprogress = Post::where('teacher_id', $tid)->where('post_status_id', 2)->orWhere('post_status_id', 4)->orWhere('post_status_id', 5)->get();
            $lessons_solved     = $lessons->where('post_status_id', 3);

            return view('site.posts.lessons.index', [
                'pendingexists'      => $pendingexists,
                'lessons_inprogress' => $lessons_inprogress,
                'lessons_solved'     => $lessons_solved,
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // N/A
    }

    /**
     * Apply to teach a lesson ('create' new lesson)
     * Authorization : Teacher
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post    = Post::findOrFail($request->post_id);
        $teacher = Auth::user()->user->teacher->teacher_id;
        $this->authorize('applyPost', $post);

        $teacher_to_post               = new TeacherToPost;
        $teacher_to_post->teacher_id   = $teacher;
        $teacher_to_post->quoted_price = $request->quote;
        $teacher_to_post->post_id      = $request->post_id;
        $teacher_to_post->save();

        return redirect('/posts/' . $request->post_id)->with('ok', "Applied to answer post!");
    }

    /**
     * Adjusts the quoted price of the application
     * Authorization : Teacher
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function adjustQuote(Request $request, $tid, $pid)
    {
        $post = Post::findOrFail($pid);
        $this->authorize('removeApplication', $post);
        $teacher = Teacher::findOrFail($tid);

        TeacherToPost::where('teacher_id', $teacher->teacher_id)->where('post_id', $post->post_id)->delete();

        $app               = new TeacherToPost;
        $app->teacher_id   = $teacher->teacher_id;
        $app->quoted_price = $request->points;
        $app->post_id      = $post->post_id;
        $app->save();
        app('App\Repositories\EmailRepository')->sendPriceAdjusted($post);

        return redirect('/teachers/' . $tid . '/lessons');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $tid
     * @param  int  $pid
     * @return \Illuminate\Http\Response
     */
    public function show($tid, $pid)
    {
        // TODO: implement
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tid, $pid)
    {
        $teacher = Teacher::findOrFail($tid);
        $post    = Post::findOrFail($pid);

        return view('site.posts.lessons.edit', ['teacher' => $teacher, 'post' => $post]);
    }

    /**
     * Update the lesson in the post table.
     * Authorization : Specific teacher
     *
     * @param  int  $tid
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $pid
     * @return \Illuminate\Http\Response
     */
    public function update($tid, Request $request, $pid)
    {
        $post = Post::findOrFail($pid);
        $this->authorize('updateSolution', $post);
        if ($post->post_status_id == 4) {
            app('App\Repositories\EmailRepository')->sendSolutionPosted($post);
        }
        $post->solution       = $request->solution;
        $post->post_status_id = 5;
        $post->save();
        return redirect('/teachers/' . $tid . '/lessons')->with('ok', 'Solution submitted. Please wait for your student to submit a teacher review or request an edit to your solution.');
    }

    /**
     * Remove application to teach lesson.
     * Authorization : Specific Teacher
     *
     * @param  int  $tid
     * @param  int  $pid
     * @return \Illuminate\Http\Response
     */
    public function destroy($tid, $pid)
    {
        $post = Post::findOrFail($pid);
        $this->authorize('removeApplication', $post);
        TeacherToPost::where('teacher_id', $tid)->where('post_id', $pid)->delete();
        return redirect('/teachers/' . $tid . '/lessons')->with('ok', 'Application successfully removed');
    }
}
