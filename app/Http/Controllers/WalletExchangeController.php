<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WalletExchangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $walletId
     * @return \Illuminate\Http\Response
     */
    public function index($walletId)
    {
        // TODO: implement
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // N/A
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $walletId
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $walletId)
    {
        // TODO: implement
    }

    /**
     * Display the specified resource.
     *
     * @param int $walletId
     * @param int $exchangeId
     * @return \Illuminate\Http\Response
     */
    public function show($walletId, $exchangeId)
    {
        // TODO: implement
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // N/A
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // N/A
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // N/A
    }
}
