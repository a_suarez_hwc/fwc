<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Teacher;
use App\Models\User;
use Auth;
use Gate;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * TODO
     * Display a listing of all the teachers if admin
     * Authorization : Admin
     *
     * Display teacher's profile if teacher else redirect to posts
     * Authorization : Teacher
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('is-teacher')) {
            if (Gate::denies('logged-in')) {
                return redirect('/posts')->with('ok', 'Please log in to view teacher profiles.');
            }
            return redirect('/posts')->with('ok', 'Please specify a teacher');
        }
        $teacher = Auth::user()->user->teacher;
        return redirect('/teachers/' . $teacher->teacher_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // N/A
    }

    /**
     * Store a newly created teacher in teacher table
     * Authorization : Admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: implement for admin
    }

    /**
     * Display the specified teacher profile.
     * Authorization : User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('logged-in')) {
            return redirect('/posts')->with('ok', 'Please log in to view teacher profiles');
        }

        $teacher = Teacher::findOrFail($id);
        return view('site.teachers.show', ['teacher' => $teacher]);
    }

    /**
     * Show the form for editing teacher profile
     * Authorization Admin, Specific Teacher
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::findOrFail($id);
        $this->authorize('accessTeacherProfile', $teacher);
        return view('site.teachers.edit', ['teacher' => $teacher]);
    }

    /**
     * Update the specified teacher information in the teacher table.
     * Authorization : Admin, Specific Teacher
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = Teacher::findOrFail($id);
        $this->authorize('accessTeacherProfile', $teacher);
        $teacher->update($request->all());
        return redirect('teachers/' . $id)->with('ok', 'Teacher has been successfully updated.');
    }

    /**
     * Remove the specified teacher's teacher designation from the database
     * Authorization : Admin
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // TODO: implement for admin
    }
}
