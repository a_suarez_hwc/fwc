<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\Wallet;
use Auth;
use Gate;
use Illuminate\Http\Request;

class WalletTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $wid
     * @return \Illuminate\Http\Response
     */
    public function index($wid)
    {
        // TODO: implement
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // N/A
    }

    /**
     * Store a newly created transaction in the transaction table and update
     * wallet value accordingly.
     *
     * Authorization : User
     *
     * @param int $wid
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $wid)
    {
        $wallet = Wallet::findOrFail($wid);
        $this->authorize('accessWallet', $wallet);

        if ($request->transaction_type === "1") {
            $wallet->points = $wallet->points + $request->points;
            $type           = "purchased";
        } else if ($request->transaction_type === "2") {
            if (Gate::denies('is-teacher')) {
                abort(403);
            }
            $wallet->points = $wallet->points - $request->points;
            $type           = "redeemed";
        } else {
            abort(403);
        }

        $wallet->save();

        $transaction                        = new Transaction;
        $transaction->wallet_id             = $wallet->wallet_id;
        $transaction->transaction_type_id   = $request->transaction_type;
        $transaction->transaction_status_id = 1;
        $transaction->amount                = $request->points * .1;
        $transaction->points                = $request->points;
        $transaction->new_wallet_balance    = $wallet->points;
        $transaction->save();

        return redirect('/wallets/' . $wid)->with('ok', "You've " . $type . " " . $transaction->points . " points!");
    }

    /**
     * Display the specified resource.
     *
     * @param int $wid
     * @param int $tid
     * @return \Illuminate\Http\Response
     */
    public function show($wid, $tid)
    {
        // TODO: implement
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // N/A
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // N/A
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // N/A
    }
}
