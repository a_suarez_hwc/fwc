<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Exchange;
use App\Models\Post;
use App\Models\PostToTag;
use App\Models\Review;
use App\Models\Teacher;
use App\Models\TeacherToPost;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserQuestionController extends Controller
{
    /**
     * Display a listing of the specific user's questions.
     * Authorization : Admin, Specific User
     *
     * @param int $uid
     * @return \Illuminate\Http\Response
     */
    public function index($uid)
    {
        $user = User::findOrFail($uid);
        $this->authorize('accessUserProfile', $user);

        $posts = Post::where('user_id', $uid)->get();
        $open  = $posts->where('post_status_id', 1);

        $in_progress = Post::where('user_id', $uid)
            ->where(function ($query) {
                $query->where('post_status_id', 2)
                    ->orWhere('post_status_id', 4)
                    ->orWhere('post_status_id', 5);
            })
            ->get();
        $solved = $posts->where('post_status_id', 3);

        return view('site.posts.questions.index', [
            'open_posts'        => $open,
            'in_progress_posts' => $in_progress,
            'solved_posts'      => $solved,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // N/A
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // N/A
    }

    /**
     * Display the specified user's specific question.
     * **Not needed because redirect to posts/post_id
     *
     * @param int $uid
     * @param int $pid
     * @return \Illuminate\Http\Response
     */
    public function show($uid, $pid)
    {
        // N/A
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // N/A
    }

    /**
     * Update the specified question in the post table.
     * **Not needed because implemented in PostController@update
     *
     * @param  int  $uid
     * @param  \Illuminate\Http\Request $request
     * @param  int  $pid
     * @return \Illuminate\Http\Request
     */
    public function update($uid, Request $request, $pid)
    {
        // N/A
    }

    /**
     * Update the status of the post and prompt teacher for a solution
     * Authorization : Owner
     *
     * @param  int $uid
     * @param  int $pid
     * @return \Illuminate\Http\Request
     */
    public function requestSolution(Request $request, $uid, $pid)
    {
        $user = User::findOrFail($uid);
        $post = Post::findOrFail($pid);
        $this->authorize('updatePost', $post);
        $post->post_status_id = 4;
        $post->save();

        app('App\Repositories\EmailRepository')->sendSolutionRequested($post);

        return redirect('/users/' . $uid . '/questions')->with('ok', 'Solution successfully requested');
    }

    /**
     * Accept the teacher's solution and finalize the post
     * Authorization : Owner
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $uid
     * @param  int  $pid
     * @return \Illuminate\Http\Request
     */
    public function acceptSolution(Request $request, $uid, $pid)
    {
        $post = Post::findOrFail($pid);
        $this->authorize('finalizePost', $post);
        if ($post->price > $post->user->wallet->points) {
            return redirect('/users/' . $uid . '/questions')->with('ok', 'Insufficient points to pay teacher. Please purchase more points');
        }
        $post->post_status_id = 3;
        $post->save();

        $review = $this->createReview($request, $pid);
        $this->updateRating($post, $review);
        $this->createExchange($post);
        app('App\Repositories\EmailRepository')->sendPostFinalized($post);

        return redirect('/users/' . $uid . '/questions')->with('ok', 'Solution accepted');
    }

    private function createReview(Request $request, $pid)
    {
        $review                             = new Review;
        $review->post_id                    = $pid;
        $review->knowledge_score            = $request->input('knowledge_score');
        $review->clarity_score              = $request->input('clarity_score');
        $review->overall_satisfaction_score = $request->input('overall_satisfaction_score');
        $review->comments                   = $request->input('comments');
        $review->date                       = Carbon::now();
        $review->save();

        return $review;
    }

    private function updateRating($post, $review)
    {
        $teacher           = $post->teacher;
        $number_of_reviews = $teacher->posts->where('post_status_id', 3)->count();
        $rating            = (float) $teacher->rating;
        $old_sum           = $rating * 3 * ($number_of_reviews - 1);
        $new_sum           = $old_sum + $review->knowledge_score + $review->clarity_score + $review->overall_satisfaction_score;
        $new_rating        = $new_sum / (3 * $number_of_reviews);
        $teacher->rating   = $new_rating;
        $teacher->save();
    }

    private function createExchange($post)
    {
        $payer              = $post->user->wallet;
        $receiver           = $post->teacher->user->wallet;
        $teacher_percentage = .9;

        $exchange                               = new Exchange;
        $exchange->paying_wallet_id             = $payer->wallet_id;
        $exchange->receiving_wallet_id          = $receiver->wallet_id;
        $exchange->post_id                      = $post->post_id;
        $exchange->points_paid                  = $post->price;
        $exchange->new_paying_wallet_balance    = $payer->points - $post->price;
        $exchange->points_received              = $post->price * $teacher_percentage;
        $exchange->new_receiving_wallet_balance = $receiver->points + $post->price * $teacher_percentage;
        $exchange->date                         = Carbon::now();
        $exchange->save();

        $payer->points = $exchange->new_paying_wallet_balance;
        $payer->save();
        $receiver->points = $exchange->new_receiving_wallet_balance;
        $receiver->save();
    }

    /**
     * Remove question iff post status is open.
     * Authorization : Owner
     *
     * @param int $uid
     * @param int $pid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uid, $pid)
    {
        $post = Post::findOrFail($pid);
        $this->authorize('modifyOpenPost', $post);

        TeacherToPost::where('post_id', $pid)->delete();
        PostToTag::where('post_id', $pid)->delete();
        $post->delete();

        return redirect('/users/' . $uid . '/questions')->with('ok', 'Applicantion successfully removed');
    }

    /**
     * Accept an applicant to become the teacher for the post
     * Authorization : Specific User
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $uid
     * @param  int $pid
     * @return \Illuminate\Http\Response
     */
    public function acceptApplicant(Request $request, $uid, $pid)
    {
        $post = Post::findOrFail($pid);
        $this->authorize('modifyOpenPost', $post);

        $user_id    = $post->user_id;
        $teacher_id = $request->teacher_id;
        $teacher    = Teacher::findOrFail($teacher_id);
        $balance    = User::where('user_id', $user_id)->first()->wallet->points;
        $applicant  = TeacherToPost::where('teacher_id', $request->teacher_id)
            ->where('post_id', $post->post_id)
            ->first();

        if ($applicant == null) {
            return redirect('/users/' . $user_id . '/questions')->with('ok', 'Application was cancelled, please check again');
        } else if ($applicant->quoted_price > $balance) {
            return redirect('/users/' . $user_id . '/questions')->with('ok', 'Insufficient points to accept this teacher. Please purchase more points');
        } else if ($applicant->quoted_price != $request->price) {
            return redirect('/users/' . $user_id . '/questions')->with('ok', 'The quoted price has been updated. Please try again');
        }
        $post->post_status_id = 2;
        $post->teacher_id     = $request->teacher_id;
        $post->price          = $applicant->quoted_price;
        $post->save();

        $applicants = TeacherToPost::where('post_id', $post->post_id)->get();

        foreach ($applicants as $application) {
            if (!($application->teacher_id == $request->teacher_id)) {
                $inactive_applicant             = new TeacherToPost;
                $inactive_applicant->teacher_id = $application->teacher_id;
                $inactive_applicant->inactive   = 1;
                $inactive_applicant->post_id    = $post->post_id;
                $inactive_applicant->save();
            }
        }
        TeacherToPost::where('post_id', $post->post_id)->where('inactive', 0)->delete();

        app('App\Repositories\EmailRepository')->sendTeacherChosen($post);

        return redirect('/users/' . $user_id . '/questions')->with('ok', 'Teacher selected! View your question in the \'In Progress\' tab');
    }

    public function getSolution(Request $request, $uid, $pid)
    {
        $user = User::findOrFail($uid);
        $post = Post::findOrFail($pid);
        return view('site.posts.lessons.edit', [
            'post'             => $post,
            'teacher'          => $post->teacher,
            'studentIsEditing' => true,
            'user'             => $user,
        ]);
    }

    public function updateSolution(Request $request, $uid, $pid)
    {
        $user           = User::findOrFail($uid);
        $post           = Post::findOrFail($pid);
        $post->solution = $request->solution;
        $post->save();
        return redirect('posts/' . $pid)->with('ok', 'solution updated');
    }
}
