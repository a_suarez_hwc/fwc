<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use File;
use Gate;
use Illuminate\Http\Request;
use Image;

class UserController extends Controller
{
    /**
     * TODO
     * Display a listing of all users if admin
     * Authorization : Admin
     *
     * Display user's profile if logged in else redirect to login
     * Authorization : User
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('logged-in')) {
            return redirect('/login');
        }
        $user_id = Auth::user()->user_id;
        return redirect('/users/' . $user_id);
    }

    /**
     * Show the form for creating a new user.
     * **Taken care of in AuthController
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // N/A
    }

    /**
     * Store a newly created user in storage.
     * **Taken care of in AuthController
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // N/A
    }

    /**
     * Display the specified user.
     * Authorization : Admin, Specific User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('accessUserProfile', $user);
        return view('site.users.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified user.
     * Authorization : Admin, Specific User
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('accessUserProfile', $user);
        return view('site.users.edit', ['user' => $user]);
    }

    /**
     * Update the specified user information in the user and contact tables
     * Authorization : Admin, Specific User
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->authorize('accessUserProfile', $user);

        $user->update($request->all());
        $this->generateThumb($request, $user);

        $user->contact->update($request->all());

        return redirect('/users/' . $id)->with('ok', 'User profile successfully updated');
    }

    /**
     * Remove the specified user from the database (user, contact, login_credential tables)
     * Authorization : Admin
     *
     * Note : Future possible feature, soft delete only
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // N/A
    }

    // TODO: make work with Japanese named files, protect from large files, and clean this thing up
    private function generateThumb(Request $request, User $user)
    {
        $fileInput = $request->files->get('profile_image_path');
        if ($fileInput) {
            $img                          = Image::make($fileInput);
            $date                         = date('Y-m-d-H-i-s') . "-";
            $fileInput                    = $fileInput->getClientOriginalName();
            $profile_image_path           = public_path('img/profiles/' . $user->login_credential->email . "-" . $fileInput);
            $profile_image_thumbnail_path = public_path('img/thumbs/' . $user->login_credential->email . "-" . $fileInput);

            // save the profiles to the public folder
            if (substr($user->profile_image_path, 0, strlen('img/profiles/default')) !== 'img/profiles/default') {
                File::delete($user->profile_image_path);
            }
            $img->save($profile_image_path);
            $user->profile_image_path = 'img/profiles/' . $user->login_credential->email . "-" . $fileInput;

            // create 32x32 thumbnails with black backgrounds and save
            $canvas = Image::canvas(32, 32, '#000000');
            $img->resize(32, 32, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });
            $canvas->insert($img, 'center');

            if (substr($user->profile_image_thumbnail_path, 0, strlen('img/thumbs/default')) !== 'img/thumbs/default') {
                File::delete($user->profile_image_thumbnail_path);
            }
            $canvas->save($profile_image_thumbnail_path);
            $user->profile_image_thumbnail_path = 'img/thumbs/' . $user->login_credential->email . "-" . $fileInput;
            $user->save();
        }
    }
}
