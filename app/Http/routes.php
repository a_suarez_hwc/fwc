<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

////////////////////
// General Routes //
////////////////////
Route::get('/', function () {
    return redirect('/posts');
});

Route::get('soon', function () {
    return view('site.soon');
});

Route::get('about', function () {
    return view('site.about');
});

// Post Routes
Route::post('posts/{id}/cancel', 'PostController@cancel');
Route::resource('posts', 'PostController');

// User Routes
Route::post('users/{uid}/questions/{pid}', 'UserQuestionController@acceptApplicant');
Route::post('users/{uid}/questions/{pid}/request', 'UserQuestionController@requestSolution');
Route::post('users/{uid}/questions/{pid}/accept', 'UserQuestionController@acceptSolution');
Route::get('users/{uid}/questions/{pid}/solution', 'UserQuestionController@getSolution');
Route::put('users/{uid}/questions/{pid}/solution', 'UserQuestionController@updateSolution');
Route::resource('users', 'UserController');
Route::resource('users.questions', 'UserQuestionController');

// Teacher routes
Route::post('teachers/{tid}/lessons/{pid}/adjust', 'TeacherLessonController@adjustQuote');
Route::resource('teachers', 'TeacherController');
Route::resource('teachers.lessons', 'TeacherLessonController');

// Wallet routes
Route::resource('wallets', 'WalletController');
Route::resource('wallets.transactions', 'WalletTransactionController');
Route::resource('wallets.exchanges', 'WalletExchangeController');

//////////////////
// Admin Routes //
//////////////////
Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', function () {
        return redirect('admin/posts');
    });
    Route::resource('posts', 'PostController');
    Route::resource('users', 'UserController');
    Route::resource('teachers', 'TeacherController');
    Route::post('wallets/{id}/addTransaction', 'WalletController@addAdjustment');
    Route::resource('wallets', 'WalletController');
    Route::resource('tags', 'TagController');
});

///////////////////////////
// Authentication Routes //
///////////////////////////
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');

// Register routes
Route::get('/register', 'Auth\AuthController@getRegister');
Route::post('/register', 'Auth\AuthController@postRegister');

// Password routes
Route::controllers(['password' => 'Auth\PasswordController']);
