<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    public function authorize()
    {
    	// TODO: learn about honeypot and see what we can do here
    	// e.g. return  $this->input('address') == '';
    	return true;
    }
}
