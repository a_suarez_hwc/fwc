<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class LoginRequest extends Request {

	/**
	 * Get validation rules for request
	 * @return array
	 */
	public function rules()
	{
		return [
			'email'		=> 'required',
			'password' 	=> 'required',
		];
	}
}