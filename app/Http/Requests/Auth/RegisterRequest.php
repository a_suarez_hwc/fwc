<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class RegisterRequest extends Request {

	/**
	 * Get validation rules for request
	 * @return array
	 */
	public function rules()
	{
		return [
			'email'		=> 'required|email|max:64|unique:login_credential,email',
			'password' 	=> 'required|confirmed'
		];
	}
}