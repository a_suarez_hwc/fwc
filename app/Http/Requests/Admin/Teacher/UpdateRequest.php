<?php

namespace App\Http\Requests\Admin\Teacher;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating'                        => 'required|numeric',
            'years_of_industry_experience'  => 'required|numeric',
            'years_of_freelance_experience' => 'required|numeric',
            'skills'                        => '',
            'description'                   => '',
            'certified_fields'               => 'required',
        ];
    }
}
