<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'family_name_kanji'            => 'required',
            'first_name_kanji'             => 'required',
            'family_name_kana'             => 'required',
            'first_name_kana'              => 'required',
            'contact.email'                => 'required|email',
            'contact.mobile_email'         => 'required|email',
            'contact.phone_number'         => '',
            'contact.skype'                => 'required',
            'login_credential.email' => 'required|email',
            'login_credential.status'      => 'required',
        ];
    }
}
