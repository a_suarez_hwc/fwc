<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Request;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'family_name_kanji'            => 'required',
            'first_name_kanji'             => 'required',
            'family_name_kana'             => 'required',
            'first_name_kana'              => 'required',
            'contact.email'                => 'required|email',
            'contact.mobile_email'         => 'email',
            'contact.phone_number'         => '',
            'contact.skype'                => '',
            'login_credential.email' => 'required|email',
            'login_credential.password'    => 'required',
            'login_credential.status'      => 'required',
        ];

        if ($this->has('is_teacher')) {
            $rules = array_merge($rules, [
                'rating'                        => 'required',
                'years_of_industry_experience'  => 'required',
                'years_of_freelance_experience' => 'required',
                'skills'                        => 'required',
                'description'                   => 'required',
                'certified_fields'              => 'required',
            ]);
        }
        return $rules;
    }
}
