<?php

namespace App\Http\Requests\Admin\Post;

use App\Http\Requests\Request;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'        => 'required|numeric',
            'teacher_id'     => 'numeric',
            'post_status_id' => 'required',
            'title'          => 'required',
            'body'           => 'required',
            'solution'       => '',
            'price'          => 'numeric|required',
            'target_date'    => 'date|required',
        ];
    }
}
