<?php

namespace App\Policies\Site;

use App\Models\LoginCredential;
use App\Models\Teacher;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($cred, $ability)
    {
        if (is_admin($cred)) {
            return true;
        }
    }

    public function accessTeacherProfile(LoginCredential $cred, Teacher $teacher)
    {
        return isTeacher($cred) && $cred->user->teacher->teacher_id == $teacher->teacher_id;
    }
}
