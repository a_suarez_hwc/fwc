<?php

namespace App\Policies\Site;

use App\Models\LoginCredential;
use App\Models\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($cred, $ability)
    {
        if (is_admin($cred)) {
            return true;
        }
    }

    public function update(LoginCredential $cred, Post $post)
    {
        $valid_statuses = [1, 2];
        return isOwnerOfPost($cred, $post) && in_array($post->post_status_id, $valid_statuses);
    }

    public function updatePost(LoginCredential $cred, Post $post)
    {
        $valid_statuses = [1, 2];
        return isOwnerOfPost($cred, $post) && in_array($post->post_status_id, $valid_statuses);
    }

    public function modifyOpenPost(LoginCredential $cred, Post $post)
    {
        return isOwnerOfPost($cred, $post) && $post->post_status_id == 1;
    }

    public function viewSolution(LoginCredential $cred, Post $post)
    {
        return $post->solution && ($post->post_status_id == 3 || isOwnerOfPost($cred, $post) || isTeacherOfPost($cred, $post));
    }

    public function updateSolution(LoginCredential $cred, Post $post)
    {
        $valid_statuses = [4, 5];
        return in_array($post->post_status_id, $valid_statuses) && (isOwnerOfPost($cred, $post) || isTeacherOfPost($cred, $post));
    }

    public function finalizePost(LoginCredential $cred, Post $post)
    {
        return isOwnerOfPost($cred, $post) && $post->post_status_id == 5;
    }

    public function applyPost(LoginCredential $cred, Post $post)
    {
        return isTeacher($cred) && !isOwnerOfPost($cred, $post) && $post->post_status_id == 1;
    }

    public function removeApplication(LoginCredential $cred, Post $post)
    {
        return $cred->user->teacher->applied_posts->contains($post) && !isTeacherOfPost($cred, $post);
    }

    public function cancelLesson(LoginCredential $cred, Post $post)
    {
        return $post->post_status_id == 2 && (isOwnerOfPost($cred, $post) || isTeacherOfPost($cred, $post));
    }
}
