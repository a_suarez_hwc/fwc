<?php

namespace App\Policies\Site;

use App\Models\LoginCredential;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($cred, $ability)
    {
        if (is_admin($cred)) {
            return true;
        }
    }

    public function accessUserProfile(LoginCredential $cred, User $user)
    {
        return $cred->user_id == $user->user_id;
    }
}
