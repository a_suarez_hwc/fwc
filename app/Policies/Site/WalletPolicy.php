<?php

namespace App\Policies\Site;

use App\Models\LoginCredential;
use App\Models\Wallet;
use Illuminate\Auth\Access\HandlesAuthorization;

class WalletPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($cred, $ability)
    {
        if (is_admin($cred)) {
            return true;
        }
    }

    public function accessWallet(LoginCredential $cred, Wallet $wallet)
    {
        return $cred->user->wallet->wallet_id == $wallet->wallet_id;
    }
}
