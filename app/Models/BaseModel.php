<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    /**
     * Fields which have to be converted to null in case of empty input
     * @var array
     */
    protected $nullable = [];

    /**
     * Boot from parent and listen on save event
     * @return [type]
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->setNullables();
        });
    }

    /**
     * Set empty nullable fields to null
     */
    protected function setNullables()
    {
        foreach ($this->nullable as $field) {
            if (!array_key_exists($field, $this->attributes) || is_null($this->$field) || (!is_numeric($this->attributes[$field]) && empty($this->attributes[$field]))) {
                $this->attributes[$field] = null;
            }
        }
    }
}
