<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table      = 'user';
    protected $primaryKey = 'user_id';
    public $timestamps    = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'family_name_kanji', 'first_name_kanji', 'family_name_kana', 'first_name_kana', 'date_created', 'last_updated',
    ];

    public $nullable = [
        'family_name_kanji',
        'first_name_kanji',
        'profile_image_path',
        'profile_image_thumbnail_path',
    ];

    public function getNameKanjiAttribute()
    {
        return $this->family_name_kanji . $this->first_name_kanji;
    }

    public function getNameKanaAttribute()
    {
        return $this->family_name_kana . $this->first_name_kana;
    }

    /**
     * Get the contact record associated with the user.
     */
    public function contact()
    {
        return $this->hasOne('App\Models\Contact');
    }

    /**
     * Get the login credential record associated with the user.
     */
    public function login_credential()
    {
        return $this->hasOne('App\Models\LoginCredential');
    }

    /**
     * Get the wallet record associated with the user.
     */
    public function wallet()
    {
        return $this->hasOne('App\Models\Wallet');
    }

    /**
     * Get the posts associated with the user.
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
     * Get the teacher record associated with the user.
     * This may not be correct
     */
    public function teacher()
    {
        return $this->hasOne('App\Models\Teacher');
    }
}
