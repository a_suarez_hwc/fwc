<?php

namespace App\Models;

class Subscription extends BaseModel
{
    protected $table      = 'subscription';
    protected $primaryKey = 'subscription_id';
    public $timestamps    = false;

    protected $fillable = [
        'teacher_id', 'frequency_id', 'last_sent',
    ];

    /*
     * Get the teacher record associated with the teacher
     */
    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    /*
     * Get the frequency record associated with the subscription
     */
    public function frequency()
    {
        return $this->belongsTo('App\Models\Frequency');
    }

    /*
     * Get the posts associated with the subscription
     */
    public function posts()
    {
        return $this->belongsToMany('App\Models\Post', 'subscription_to_post', 'subscription_id', 'post_id');
    }

    /*
     * Get the tags associated with the subscription
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'subscription_to_tag', 'subscription_id', 'tag_id');
    }
}
