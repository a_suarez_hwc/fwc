<?php

namespace App\Models;

class Tag extends BaseModel
{
    protected $table      = 'tag';
    protected $primaryKey = 'tag_id';
    public $timestamps    = false;

    protected $fillable = [
        'tag_group_id',
        'tag_text',
    ];

    /**
     * Get the posts that have the tag
     */
    public function posts()
    {
        return $this->belongsToMany('App\Models\Post', 'post_to_tag', 'tag_id', 'post_id');
    }

    /*
     * Get the subscriptions associated with the tag
     */
    public function subscriptions()
    {
        return $this->belongsToMany('App\Models\Subscription', 'subscription_to_tag', 'tag_id', 'subscription_id');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\TagGroup');
    }
}
