<?php

namespace App\Models;

class TeacherToPost extends BaseModel
{
    protected $table   = 'teacher_to_post';
    public $timestamps = false;

    protected $fillable = [
        'teacher_id', 'inactive', 'quoted_price', 'post_id',
    ];
}
