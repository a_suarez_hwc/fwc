<?php

namespace App\Models;

class TransactionType extends BaseModel
{
    protected $table      = 'transaction_type';
    protected $primaryKey = 'transaction_type_id';
    public $timestamps    = false;

    protected $fillable = ['transaction_type_text'];

    /**
     * Get the transaction records associated with the given transaction type
     */
    public function transactions()
    {
        return $this->hasMany('App\Model\Transaction');
    }
}
