<?php

namespace App\Models;

class Frequency extends BaseModel
{
    protected $table      = 'frequency';
    protected $primaryKey = 'frequency_id';
    public $timestamps    = false;

    protected $fillable = ['frequency_text'];

    /**
     * Get the subscription records associated with the given frequency
     */
    public function subscriptions()
    {
        return $this->hasMany('App\Model\Subscription');
    }
}
