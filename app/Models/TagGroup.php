<?php

namespace App\Models;

class TagGroup extends BaseModel
{
    protected $table      = 'tag_group';
    protected $primaryKey = 'tag_group_id';
    public $timestamps    = false;
    
    protected $fillable   = ['tag_group_text'];

    /**
     * Get the posts that have the tag
     */
    public function tags()
    {
        return $this->hasMany('App\Models\Tag');
    }
}
