<?php

namespace App\Models;

class Exchange extends BaseModel
{
    protected $table      = 'exchange';
    protected $primaryKey = 'exchange_id';
    public $timestamps    = true;

    protected $fillable = [
        'paying_wallet_id',
        'receiving_wallet_id',
        'post_id',
        'points',
    ];
    /**
     * Get the paying wallet associated with the exchange.
     */
    public function payingWallet()
    {
        return $this->belongsTo('App\Models\Wallet', 'paying_wallet_id');
    }

    /**
     * Get the receiving wallet associated with the exchange.
     */
    public function receivingWallet()
    {
        return $this->belongsTo('App\Models\Wallet', 'receiving_wallet_id');
    }

    /**
     * Get the post associated with the exchange.
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
