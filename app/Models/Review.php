<?php

namespace App\Models;

class Review extends BaseModel
{
    protected $table      = 'review';
    protected $primaryKey = 'review_id';
    public $timestamps    = true;

    protected $fillable = [
        'post_id', 'knowledge_score', 'clarity_score', 'overall_satisfaction_score', 'comments', 'date',
    ];

    /**
     * Get the post associated with the review.
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
