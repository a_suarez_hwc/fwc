<?php

namespace App\Models;

class Teacher extends BaseModel
{
    protected $table      = 'teacher';
    protected $primaryKey = 'teacher_id';
    public $timestamps    = true;

    protected $fillable = [
        'user_id', 'years_of_industry_experience', 'years_of_freelance_experience', 'skills', 'description', 'rating', 'date_created', 'last_updated',
    ];

    /**
     * Get the user record associated with the teacher.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the certified fields that the teacher is certified in.
     */
    public function certified_fields()
    {
        return $this->belongsToMany('App\Models\CertifiedField', 'teacher_to_certified_field', 'teacher_id', 'certified_field_id');
    }

    /*
     * Get the subscription record associated with the teacher
     */
    public function subscription()
    {
        return $this->hasOne('App\Models\Subscription');
    }

    /**
     * Get the posts the teacher is assigned to
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    /**
     * Get the posts the teacher has applied to
     */
    public function applied_posts()
    {
        return $this->belongsToMany('App\Models\Post', 'teacher_to_post', 'teacher_id', 'post_id')->withPivot('inactive', 'quoted_price');
    }

    public function getContactAttribute() {
        return $this->user->contact;
    }
}
