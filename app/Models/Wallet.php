<?php

namespace App\Models;

class Wallet extends BaseModel
{
    protected $table      = 'wallet';
    protected $primaryKey = 'wallet_id';
    public $timestamps    = false;

    protected $fillable = [
        'user_id',
        'points',
    ];

    /**
     * Get the user record associated with the wallet.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the transactions associated with the wallet.
     */
    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    /*
     * Get the payment exchanges made by the wallet
     */
    public function paymentExchanges()
    {
        return $this->hasMany('App\Models\Exchange', 'paying_wallet_id');
    }

    /*
     * Get the receivement exchanges made by the wallet
     */
    public function receivementExchanges()
    {
        return $this->hasMany('App\Models\Exchange', 'receiving_wallet_id');
    }

    public function getExchangesAttribute()
    {
        $incomes   = $this->receivementExchanges;
        $payments  = $this->paymentExchanges;
        $exchanges = $incomes->merge($payments);
        return $exchanges->sortByDesc('data');
    }

    public function getActivityAttribute()
    {
        // get transaction data & sort
        $added_transactions   = $this->transactions->where('transaction_type_id', 1)->sortByDesc('date');
        $cashout_transactions = $this->transactions->where('transaction_type_id', 2)->sortByDesc('date');

        // get exchange data & sort
        $incomes  = $this->receivementExchanges->sortByDesc('date');
        $payments = $this->paymentExchanges->sortByDesc('date');

        // merge all transactions and sort
        $all = $added_transactions->merge($cashout_transactions)->merge($incomes)->merge($payments);
        return $all->sortByDesc('date');
    }
}
