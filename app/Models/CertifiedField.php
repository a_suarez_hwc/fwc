<?php

namespace App\Models;

class CertifiedField extends BaseModel
{
    protected $table      = 'certified_field';
    protected $primaryKey = 'certified_field_id';
    public $timestamps    = false;

    protected $fillable = ['certified_field_text'];

    /**
     * Get the teachers that are certified to teach the field.
     */
    public function certified_fields()
    {
        return $this->belongsToMany('App\Models\Teacher', 'teacher_to_certified_field', 'certified_field_id', 'teacher_id');
    }
}
