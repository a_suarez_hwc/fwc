<?php

namespace App\Models;

class PostStatus extends BaseModel
{
    protected $table      = 'post_status';
    protected $primaryKey = 'post_status_id';
    public $timestamps    = false;

    protected $fillable = ['post_status_text'];

    /**
     * Get the post records associated with the given post status
     */
    public function posts()
    {
        return $this->hasMany('App\Model\Post');
    }
}
