<?php

namespace App\Models;

class Contact extends BaseModel
{
    protected $table      = 'contact';
    protected $primaryKey = 'contact_id';
    public $timestamps    = false;

    protected $fillable = [
        'user_id', 'email', 'mobile_email', 'phone_number', 'skype',
    ];

    public $nullable = [
        'mobile_email',
        'skype',
    ];

    /**
     * Get the user tied to the contact.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
