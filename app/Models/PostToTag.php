<?php

namespace App\Models;

class PostToTag extends BaseModel
{
    protected $table   = 'post_to_tag';
    public $timestamps = false;

    protected $fillable = [
        'post_id', 'tag_id',
    ];
}
