<?php

namespace App\Models;

class TeacherToCertifiedField extends BaseModel
{
    protected $table   = 'teacher_to_certified_field';
    public $timestamps = false;

    protected $fillable = [
        'teacher_id', 'certified_field_id',
    ];
}
