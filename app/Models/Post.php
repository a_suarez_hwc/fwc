<?php

namespace App\Models;

class Post extends BaseModel
{
    protected $table      = 'post';
    protected $primaryKey = 'post_id';
    public $timestamps    = true;

    protected $fillable = [
        'user_id',
        'teacher_id',
        'post_status_id',
        'title',
        'body',
        'solution',
        'target_date',
        'price',
    ];

    public $nullable = [
        'teacher_id',
        'solution',
        'price',
        'target_date',
    ];

    /**
     * Get the user associated with the post.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the teacher associated with the post.
     */
    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    /*
     * Get the exchange record associated with the post
     */
    public function exchange()
    {
        return $this->hasOne('App\Models\Exchange');
    }

    /*
     * Get the review record associated with the post
     */
    public function review()
    {
        return $this->hasOne('App\Models\Review');
    }

    /*
     * Get the status associated with the post
     */
    public function post_status()
    {
        return $this->belongsTo('App\Models\PostStatus', 'post_status_id');
    }

    /*
     * Get the tags associated with the post
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'post_to_tag', 'post_id', 'tag_id');
    }

    /*
     * Get the subscriptions associated with the post
     */
    public function subscriptions()
    {
        return $this->belongsToMany('App\Models\Subscription', 'subscription_to_post', 'post_id', 'subscription_id');
    }

    /**
     * Get the teachers applying to the post
     */
    public function teachers()
    {
        return $this->belongsToMany('App\Models\Teacher', 'teacher_to_post', 'post_id', 'teacher_id')->withPivot('inactive', 'quoted_price');
    }

    /**
     * Get the body text of the post.
     */
    public function body()
    {
        return $this->hasOne('App\Models\Body');
    }
}
