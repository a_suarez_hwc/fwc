<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class LoginCredential extends Authenticatable
{
    protected $table      = 'login_credential';
    protected $primaryKey = 'login_credential_id';
    public $timestamps    = false;

    protected $fillable = [
        'user_id', 'email', 'password', 'status', 'reset_token', 'reset_token_timestamp', 'token_used', 'last_logged_in',
    ];

    public $nullable = [
        'reset_token',
        'reset_token_timestamp',
    ];

    /**
     * Get the user tied to the login credential.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
