<?php

namespace App\Models;

class SubscriptionToTag extends BaseModel
{
    protected $table   = 'subscription_to_tag';
    public $timestamps = false;

    protected $fillable = [
        'subscription_id', 'tag_id',
    ];
}
