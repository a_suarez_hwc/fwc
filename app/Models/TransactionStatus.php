<?php

namespace App\Models;

class TransactionStatus extends BaseModel
{
    protected $table      = 'transaction_status';
    protected $primaryKey = 'transaction_status_id';
    public $timestamps    = false;

    protected $fillable = ['transaction_status_text'];

    /**
     * Get the transaction records associated with the given transaction status
     */
    public function transactions()
    {
        return $this->hasMany('App\Model\Transaction');
    }
}
