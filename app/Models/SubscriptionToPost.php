<?php

namespace App\Models;

class SubscriptionToPost extends BaseModel
{
    protected $table   = 'subscription_to_post';
    public $timestamps = false;

    protected $fillable = [
        'subscription_id', 'post_id',
    ];
}
