<?php

namespace App\Models;

class Transaction extends BaseModel
{
    protected $table      = 'transaction';
    protected $primaryKey = 'transaction_id';
    public $timestamps = true;

    protected $fillable = [
        'wallet_id',
        'transaction_type_id',
        'transaction_status_id',
        'new_wallet_balance',
        'amount',
        'points',
    ];

    protected $dates = ['date'];

    /**
     * Get the transaction status record associated with the transaction.
     */
    public function transactionStatus()
    {
        return $this->belongsTo('App\Models\TransactionStatus', 'transaction_status_id');
    }

    /*
     * Get the transaction type record associated with the transaction.
     */
    public function transactionType()
    {
        return $this->belongsTo('App\Models\TransactionType', 'transaction_type_id');
    }

    /**
     * Get the wallet record associated with the transaction.
     */
    public function wallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }
}
