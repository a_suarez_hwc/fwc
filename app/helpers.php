<?php

if (!function_exists('classActivePath')) {
    function classActivePath($value, $class = '')
    {
        $text = $class ? $class . '--active' : 'active';
        if (!is_array($value)) {
            return Request::is($value) ? $text : '';
        }
        return in_array(Request::path(), $value) ? $text : '';
    }
}

if (!function_exists('classActiveSegment')) {
    function classActiveSegment($segment, $value, $class = '', $emptySegment = 0)
    {
        $text = $class ? $class . '--active' : 'active';
        if (!is_array($value)) {
            return Request::segment($segment) === $value && Request::segment($emptySegment) === null ? $text : '';
        }
        foreach ($value as $v) {
            if (Request::segment($segment) === $v && Request::segment($emptySegment) === null) {
                return $text;
            }

        }
        return '';
    }
}

if (!function_exists('isTeacher')) {
    function isTeacher($cred)
    {
        return !empty($cred->user->teacher) && $cred->user->teacher !== null;
    }
}

if (!function_exists('isTeacherOfPost')) {
    function isTeacherOfPost($cred, $post)
    {
        return isTeacher($cred) && $cred->user->teacher->teacher_id === $post->teacher_id;
    }
}

if (!function_exists('isOwnerOfPost')) {
    function isOwnerOfPost($cred, $post)
    {
        return $cred->user_id === $post->user_id;
    }
}

if (!function_exists('is_admin')) {
    function is_admin($cred)
    {
        return $cred->is_admin;
    }
}

if (!function_exists('rip_tags')) {
    function rip_tags($string)
    {

        // ----- remove HTML TAGs -----
        $string = preg_replace('/<[^>]*>/', ' ', $string);

        // ----- remove control characters -----
        $string = str_replace("\r", '', $string); // --- replace with empty space
        $string = str_replace("\n", ' ', $string); // --- replace with space
        $string = str_replace("\t", ' ', $string); // --- replace with space

        // ----- remove multiple spaces -----
        $string = trim(preg_replace('/ {2,}/', ' ', $string));

        return $string;
    }
}
