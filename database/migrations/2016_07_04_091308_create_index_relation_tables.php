<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndexRelationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_to_certified_field', function (Blueprint $table) {
            $table->integer('teacher_id')->unsigned()->index();
            $table->tinyInteger('certified_field_id')->unsigned()->index();

            $table->foreign('teacher_id')->references('teacher_id')->on('teacher')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('certified_field_id')->references('certified_field_id')->on('certified_field')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('subscription_to_tag', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->index();
            $table->tinyInteger('tag_id')->unsigned()->index();

            $table->foreign('subscription_id')->references('subscription_id')->on('subscription')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tag_id')->references('tag_id')->on('tag')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('subscription_to_post', function (Blueprint $table) {
            $table->integer('subscription_id')->unsigned()->index();
            $table->integer('post_id')->unsigned()->index();

            $table->foreign('subscription_id')->references('subscription_id')->on('subscription')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('post_id')->references('post_id')->on('post')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('post_to_tag', function (Blueprint $table) {
            $table->integer('post_id')->unsigned()->index();
            $table->tinyInteger('tag_id')->unsigned()->index();

            $table->foreign('post_id')->references('post_id')->on('post')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tag_id')->references('tag_id')->on('tag')->onDelete('cascade')->onUpdate('cascade');
        });

        // TODO: Update to account for denies without deletion
        Schema::create('teacher_to_post', function (Blueprint $table) {
            $table->integer('teacher_id')->unsigned()->index();
            $table->boolean('inactive')->default(0);
            $table->integer('quoted_price')->unsigned();
            $table->integer('post_id')->unsigned()->index();

            $table->foreign('teacher_id')->references('teacher_id')->on('teacher')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('post_id')->references('post_id')->on('post')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teacher_to_certified_field');
        Schema::drop('subscription_to_tag');
        Schema::drop('subscription_to_post');
        Schema::drop('post_to_tag');
        Schema::drop('teacher_to_post');
    }
}
