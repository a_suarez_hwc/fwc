<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->increments('post_id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('teacher_id')->unsigned()->index()->nullable();
            $table->tinyInteger('post_status_id')->unsigned()->index();
            $table->string('title', 256);
            $table->string('body', 1024);
            $table->string('solution', 4096)->nullable();
            $table->integer('price')->nullable();
            $table->date('target_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('user_id')->on('user')->onUpdate('cascade');
            $table->foreign('teacher_id')->references('teacher_id')->on('teacher')->onUpdate('cascade');
            $table->foreign('post_status_id')->references('post_status_id')->on('post_status')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post');
    }
}
