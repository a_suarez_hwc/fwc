<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIndexTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certified_field', function (Blueprint $table) {
            $table->tinyInteger('certified_field_id')->unsigned()->autoIncrement();
            $table->string('field_text', 64);
        });

        Schema::create('frequency', function (Blueprint $table) {
            $table->tinyInteger('frequency_id')->unsigned()->autoIncrement();
            $table->string('frequency_text', 32);
        });

        Schema::create('tag_group', function(Blueprint $table) {
            $table->increments('tag_group_id');
            $table->string('tag_group_text', 64);
        });

        Schema::create('tag', function (Blueprint $table) {
            $table->tinyInteger('tag_id')->unsigned()->autoIncrement();
            $table->integer('tag_group_id')->unsigned()->index();
            $table->string('tag_text', 64);

            $table->foreign('tag_group_id')->references('tag_group_id')->on('tag_group')->onDelete('cascade')->onUpdate('cascade');
        });
        
        Schema::create('post_status', function (Blueprint $table) {
            $table->tinyInteger('post_status_id')->unsigned()->autoIncrement();
            $table->string('post_status_text', 64);
        });

        Schema::create('transaction_status', function (Blueprint $table) {
            $table->tinyInteger('transaction_status_id')->unsigned()->autoIncrement();
            $table->string('transaction_status_text', 32);
        });

        Schema::create('transaction_type', function (Blueprint $table) {
            $table->tinyInteger('transaction_type_id')->unsigned()->autoIncrement();
            $table->string('transaction_type_text', 64);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('certified_field');
        Schema::drop('frequency');
        Schema::drop('tag');
        Schema::drop('tag_group');
        Schema::drop('post_status');
        Schema::drop('transaction_status');
        Schema::drop('transaction_type');
    }
}
