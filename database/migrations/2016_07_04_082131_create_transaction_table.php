<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('transaction_id');
            $table->integer('wallet_id')->unsigned()->index();
            $table->tinyInteger('transaction_type_id')->unsigned()->index();
            $table->tinyInteger('transaction_status_id')->unsigned()->index();
            $table->integer('amount');
            $table->integer('points');
            $table->integer('new_wallet_balance');
            $table->timestamps();

            $table->foreign('wallet_id')->references('wallet_id')->on('wallet')->onUpdate('cascade');
            $table->foreign('transaction_type_id')->references('transaction_type_id')->on('transaction_type')->onUpdate('cascade');
            $table->foreign('transaction_status_id')->references('transaction_status_id')->on('transaction_status')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction');
    }
}
