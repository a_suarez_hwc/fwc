<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExchangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange', function (Blueprint $table) {
            $table->increments('exchange_id');
            $table->integer('paying_wallet_id')->unsigned()->index();
            $table->integer('receiving_wallet_id')->unsigned()->index();
            $table->integer('post_id')->unsigned()->index();
            $table->integer('points_paid');
            $table->integer('new_paying_wallet_balance');
            $table->integer('points_received');
            $table->integer('new_receiving_wallet_balance');
            $table->timestamps();

            $table->foreign('paying_wallet_id')->references('wallet_id')->on('wallet')->onUpdate('cascade');
            $table->foreign('receiving_wallet_id')->references('wallet_id')->on('wallet')->onUpdate('cascade');
            $table->foreign('post_id')->references('post_id')->on('post')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exchange');
    }
}
