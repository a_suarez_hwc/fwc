<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription', function (Blueprint $table) {
            $table->increments('subscription_id');
            $table->integer('teacher_id')->unsigned()->index();
            $table->tinyInteger('frequency_id')->unsigned()->index();
            $table->dateTime('last_sent'); //TODO: Default

            $table->foreign('teacher_id')->references('teacher_id')->on('teacher')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('frequency_id')->references('frequency_id')->on('frequency')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscription');
    }
}
