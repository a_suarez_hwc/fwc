<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher', function (Blueprint $table) {
            $table->increments('teacher_id');
            $table->integer('user_id')->unsigned()->index();
            $table->tinyInteger('years_of_industry_experience');
            $table->tinyInteger('years_of_freelance_experience');
            $table->string('skills', 512);
            $table->string('description', 1024);
            $table->float('rating'); //TODO: Default
            $table->timestamps();

            $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teacher');
    }
}
