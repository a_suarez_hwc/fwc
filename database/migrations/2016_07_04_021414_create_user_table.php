<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('family_name_kanji', 64)->nullable();
            $table->string('first_name_kanji', 64)->nullable();
            $table->string('family_name_kana', 64);
            $table->string('first_name_kana', 64);
            $table->string('profile_image_path')->nullable(); //TODO: Give Default
            $table->string('profile_image_thumbnail_path')->nullable(); //TODO: Give Default
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
