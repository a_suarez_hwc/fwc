<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoginCredentialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_credential', function (Blueprint $table) {
            $table->increments('login_credential_id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('email');
            $table->char('password', 60);
            $table->string('status', 32);
            $table->boolean('is_admin')->default(false);
            $table->char('reset_token', 60)->nullable();
            $table->dateTime('reset_token_timestamp')->nullable();
            $table->boolean('reset_token_used');
            $table->dateTime('last_logged_in');
            $table->string('remember_token', 100);

            $table->foreign('user_id')->references('user_id')->on('user')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('password_resets');
        Schema::drop('login_credential');
    }
}
