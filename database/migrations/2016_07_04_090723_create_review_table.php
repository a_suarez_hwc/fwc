<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review', function (Blueprint $table) {
            $table->increments('review_id');
            $table->integer('post_id')->unsigned()->index();
            $table->tinyInteger('knowledge_score');
            $table->tinyInteger('clarity_score');
            $table->tinyInteger('overall_satisfaction_score');
            $table->string('comments', 256);
            $table->timestamps();

            $table->foreign('post_id')->references('post_id')->on('post')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('review');
    }
}
