<?php

use App\Models\TeacherToCertifiedField;
use Illuminate\Database\Seeder;

class TeacherToCertifiedFieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 20;

        for ($i = 1; $i <= $limit; $i++) {
            $number_of_fields = $faker->numberBetween(3, 5);
            for ($j = 1; $j <= $number_of_fields; $j++) {
                TeacherToCertifiedField::firstOrCreate([
                    'teacher_id'         => $i,
                    'certified_field_id' => $faker->numberBetween(1, 60),
                ]);
            }
        }
    }
}
