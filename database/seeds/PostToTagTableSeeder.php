<?php

use App\Models\PostToTag;
use Illuminate\Database\Seeder;

class PostToTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 200;

        for ($i = 1; $i <= $limit; $i++) {
            $post = DB::table('post')->where('post_id', $i)->first();

            if ($post->post_status_id != 1) {
                $teacher_id       = $post->teacher_id;
                $teacher_fields   = DB::table('teacher_to_certified_field')->where('teacher_id', $teacher_id);
                $number_of_fields = $teacher_fields->count();
                $fields           = $teacher_fields->take($faker->numberBetween(1, $number_of_fields))->get();

                foreach ($fields as $field) {
                    PostToTag::firstOrCreate([
                        'post_id' => $i,
                        'tag_id'  => $field->certified_field_id,
                    ]);
                }
            } else {
                $number_to_assign = $faker->numberBetween(2, 4);

                for ($j = 1; $j <= $number_to_assign; $j++) {
                    PostToTag::firstOrCreate([
                        'post_id' => $i,
                        'tag_id'  => $faker->numberBetween(1, 60),
                    ]);
                }
            }
        }
    }
}
