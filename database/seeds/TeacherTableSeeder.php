<?php

use App\Models\Teacher;
use Illuminate\Database\Seeder;

class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 20;

        for ($i = 1; $i <= $limit; $i++) {
            $random_id = $faker->unique()->numberBetween(1, 50);
            Teacher::create([
                'user_id'                       => $random_id,
                'years_of_industry_experience'  => $faker->numberBetween(3, 9),
                'years_of_freelance_experience' => $faker->numberBetween(3, 9),
                'skills'                        => $faker->realText(30),
                'description'                   => $faker->realText(60),
                'rating'                        => 0,
                'created_at'                    => $faker->dateTimeThisYear->format('Y-m-d H:i:s'),
                'updated_at'                    => $faker->dateTimeThisMonth->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
