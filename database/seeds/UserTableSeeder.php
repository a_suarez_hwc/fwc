<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 50;

        for ($i = 1; $i <= $limit; $i++) {
            $imageName = 'default-' . $faker->numberBetween(1, 10) . '.jpg';
            $img       = Image::make('public/img/profiles/' . $imageName);
            $img->resize(32, 32);
            $img->save('public/img/thumbs/' . $imageName, 100);

            User::create([
                'family_name_kanji'            => $faker->lastName,
                'first_name_kanji'             => $faker->firstName,
                'family_name_kana'             => $faker->lastKanaName,
                'first_name_kana'              => $faker->firstKanaName,
                'profile_image_path'           => 'img/profiles/' . $imageName,
                'profile_image_thumbnail_path' => 'img/thumbs/' . $imageName,
            ]);
        }
    }
}
