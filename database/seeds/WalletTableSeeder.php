<?php

use App\Models\Wallet;
use Illuminate\Database\Seeder;

class WalletTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 50;

        for ($i = 1; $i <= $limit; $i++) {
            Wallet::create([
                'user_id' => $i,
                'points'  => 0,
            ]);
        }
    }
}
