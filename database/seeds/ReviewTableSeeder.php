<?php

use App\Models\Review;
use Illuminate\Database\Seeder;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 200;

        for ($i = 1; $i <= $limit; $i++) {
            $post        = DB::table('post')->where('post_id', $i)->first();
            $post_status = $post->post_status_id;
            if ($post_status == 3) {
                $score1 = $faker->numberBetween(7, 10);
                $score2 = $faker->numberBetween(7, 10);
                $score3 = $faker->numberBetween(7, 10);
                Review::create([
                    'post_id'                    => $i,
                    'knowledge_score'            => $score1,
                    'clarity_score'              => $score2,
                    'overall_satisfaction_score' => $score3,
                    'comments'                   => $faker->realText(10),
                    'created_at'                 => $post->updated_at,
                ]);
                $teacher = DB::table('teacher')
                    ->where('teacher_id', $post->teacher_id);
                $number_of_reviews = DB::table('post')
                    ->where('teacher_id', $teacher->first()->teacher_id)
                    ->where('post_status_id', 3)
                    ->join('review', 'post.post_id', '=', 'review.post_id')
                    ->count();
                $rating     = (float) $teacher->first()->rating;
                $new_rating = (3 * $rating * ($number_of_reviews - 1) + $score1 + $score2 + $score3) / (3 * $number_of_reviews);
                $teacher->update(array('rating' => $new_rating));
            }
        }
    }
}
