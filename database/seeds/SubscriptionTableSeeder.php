<?php

use Illuminate\Database\Seeder;

class SubscriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 20;

        for ($i = 1; $i <= $limit; $i++) {
            App\Models\Subscription::create([
                'teacher_id'   => $i,
                'frequency_id' => $faker->numberBetween(1, 5),
                'last_sent'    => '2016-07-06 10:00:00',
            ]);
        }
    }
}
