<?php

use App\Models\SubscriptionToTag;
use Illuminate\Database\Seeder;

class SubscriptionToTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 20;

        for ($i = 1; $i <= $limit; $i++) {
            $subscription = DB::table('subscription')->where('subscription_id', $i)->first();

            $teacher_id               = $subscription->teacher_id;
            $teacher_certified_fields = DB::table('teacher_to_certified_field')->where('teacher_id', $teacher_id);
            $number_of_fields         = $teacher_certified_fields->count();
            $fields                   = $teacher_certified_fields->take($faker->numberBetween(1, $number_of_fields))->get();

            foreach ($fields as $field) {
                SubscriptionToTag::firstOrCreate([
                    'subscription_id' => $i,
                    'tag_id'          => $field->certified_field_id,
                ]);
            }
        }
    }
}
