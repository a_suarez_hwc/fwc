<?php

use App\Models\CertifiedField;
use App\Models\Frequency;
use App\Models\PostStatus;
use App\Models\Tag;
use App\Models\TagGroup;
use App\Models\TransactionStatus;
use App\Models\TransactionType;
use Illuminate\Database\Seeder;

class IndexTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->populatePostStatus();
        $this->populateFrequency();
        $this->populateTransactionType();
        $this->populateTransactionStatus();
        $this->populateCertifiedField();
        $this->populateTagGroup();
        $this->populateTag();
    }
    public function populatePostStatus()
    {
        PostStatus::firstOrCreate(['post_status_text' => 'Open']);
        PostStatus::firstOrCreate(['post_status_text' => 'In Progress']);
        PostStatus::firstOrCreate(['post_status_text' => 'Solved']);
        PostStatus::firstOrCreate(['post_status_text' => 'Solution Pending']);
        PostStatus::firstOrCreate(['post_status_text' => 'Review Pending']);
    }
    public function populateFrequency()
    {
        Frequency::firstOrCreate(['frequency_text' => 'Ad hoc']);
        Frequency::firstOrCreate(['frequency_text' => 'Daily']);
        Frequency::firstOrCreate(['frequency_text' => 'Weekly']);
        Frequency::firstOrCreate(['frequency_text' => 'Monthly']);
        Frequency::firstOrCreate(['frequency_text' => 'Never']);
    }
    public function populateTransactionType()
    {
        TransactionType::firstOrCreate(['transaction_type_text' => 'Purchase']);
        TransactionType::firstOrCreate(['transaction_type_text' => 'Redemption']);
        TransactionType::firstOrCreate(['transaction_type_text' => 'Adjustment']);
    }
    public function populateTransactionStatus()
    {
        TransactionStatus::firstOrCreate(['transaction_status_text' => 'Pending']);
        TransactionStatus::firstOrCreate(['transaction_status_text' => 'Complete']);
    }
    public function populateCertifiedField()
    {
        CertifiedField::firstOrCreate(['field_text' => '顧客折衝']);
        CertifiedField::firstOrCreate(['field_text' => 'サイト案件ディレクション']);
        CertifiedField::firstOrCreate(['field_text' => 'スマホコンテンツディレクション']);
        CertifiedField::firstOrCreate(['field_text' => 'DB・ｿｰｼｬﾙﾒﾃﾞｨｱ連動案件ディレクション']);
        CertifiedField::firstOrCreate(['field_text' => '企画・提案']);
        CertifiedField::firstOrCreate(['field_text' => 'モバイルコンテンツディレクション']);
        CertifiedField::firstOrCreate(['field_text' => 'DB・ｼｽﾃﾑ連動ｻｲﾄ案件ディレクション']);
        CertifiedField::firstOrCreate(['field_text' => '画像加工（ﾘｻｲｽﾞ/色調補正）']);
        CertifiedField::firstOrCreate(['field_text' => 'モバイルコンテンツデザイン']);
        CertifiedField::firstOrCreate(['field_text' => 'HTMLメルマガ制作']);
        CertifiedField::firstOrCreate(['field_text' => 'ドット絵']);
        CertifiedField::firstOrCreate(['field_text' => 'TOP・Secondﾍﾟｰｼﾞ・ﾃﾝﾌﾟﾚｰﾄﾃﾞｻﾞｲﾝ']);
        CertifiedField::firstOrCreate(['field_text' => 'バナー作成']);
        CertifiedField::firstOrCreate(['field_text' => '画像加工（トリミング）']);
        CertifiedField::firstOrCreate(['field_text' => 'スマホ対応コンテンツデザイン']);
        CertifiedField::firstOrCreate(['field_text' => 'アイコン作成']);
        CertifiedField::firstOrCreate(['field_text' => 'イラスト作成']);
        CertifiedField::firstOrCreate(['field_text' => 'スマホアプリ用デザイン']);
        CertifiedField::firstOrCreate(['field_text' => 'キャラクターデザイン']);
        CertifiedField::firstOrCreate(['field_text' => 'テキスト入力・流し込み']);
        CertifiedField::firstOrCreate(['field_text' => '更新作業']);
        CertifiedField::firstOrCreate(['field_text' => '仕様書に沿ったコーディング']);
        CertifiedField::firstOrCreate(['field_text' => 'モバイル対応コーディング']);
        CertifiedField::firstOrCreate(['field_text' => 'WP構築（ｲﾝｽﾄｰﾙ・ﾌﾟﾗｸﾞｲﾝ導入・ ﾃﾝﾌﾟﾚ作成）']);
        CertifiedField::firstOrCreate(['field_text' => '他CMS構築（ｲﾝｽﾄｰﾙ・ﾌﾟﾗｸﾞｲﾝ導入・ ﾃﾝﾌﾟﾚ作成）']);
        CertifiedField::firstOrCreate(['field_text' => 'CMS入力']);
        CertifiedField::firstOrCreate(['field_text' => 'ｱｸｾｼﾋﾞﾘﾃｨ･ﾕｰｻﾋﾞﾘﾃｨを意識した ｺｰﾃﾞｨﾝｸﾞ']);
        CertifiedField::firstOrCreate(['field_text' => 'テンプレート作成']);
        CertifiedField::firstOrCreate(['field_text' => 'スマホ対応コーディング']);
        CertifiedField::firstOrCreate(['field_text' => 'MT構築（ｲﾝｽﾄｰﾙ・ﾌﾟﾗｸﾞｲﾝ導入・ ﾃﾝﾌﾟﾚ作成）']);
        CertifiedField::firstOrCreate(['field_text' => 'FBページ制作']);
        CertifiedField::firstOrCreate(['field_text' => 'アプリ企画・設計']);
        CertifiedField::firstOrCreate(['field_text' => 'アプリ開発']);
        CertifiedField::firstOrCreate(['field_text' => 'モバイルコンテンツ運用']);
        CertifiedField::firstOrCreate(['field_text' => 'スマホコンテンツ運用']);
        CertifiedField::firstOrCreate(['field_text' => 'マルチキャリア対応']);
        CertifiedField::firstOrCreate(['field_text' => 'テスト・検証']);
        CertifiedField::firstOrCreate(['field_text' => 'Flashバナー制作']);
        CertifiedField::firstOrCreate(['field_text' => 'Javascriptとの連携']);
        CertifiedField::firstOrCreate(['field_text' => 'eラーニング制作']);
        CertifiedField::firstOrCreate(['field_text' => '3Dアニメーション']);
        CertifiedField::firstOrCreate(['field_text' => 'アイキャッチ作成']);
        CertifiedField::firstOrCreate(['field_text' => 'インタラクティブコンテンツ制作']);
        CertifiedField::firstOrCreate(['field_text' => 'シナリオ作成']);
        CertifiedField::firstOrCreate(['field_text' => 'ASを利用してのアニメーション制御']);
        CertifiedField::firstOrCreate(['field_text' => 'AllFlashサイト制作']);
        CertifiedField::firstOrCreate(['field_text' => '映像配信']);
        CertifiedField::firstOrCreate(['field_text' => 'CGI、PHPとの連携']);
        CertifiedField::firstOrCreate(['field_text' => 'ライティング（メルマガ）']);
        CertifiedField::firstOrCreate(['field_text' => 'ライティング（WEB以外）']);
        CertifiedField::firstOrCreate(['field_text' => 'ライティング（WEBコンテンツ）']);
        CertifiedField::firstOrCreate(['field_text' => '編集（WEB以外）']);
        CertifiedField::firstOrCreate(['field_text' => '編集（WEBコンテンツ）']);
        CertifiedField::firstOrCreate(['field_text' => 'ソーシャルメディア運営']);
        CertifiedField::firstOrCreate(['field_text' => '動画編集']);
        CertifiedField::firstOrCreate(['field_text' => '事務作業・入力作業']);
        CertifiedField::firstOrCreate(['field_text' => '写真撮影']);
        CertifiedField::firstOrCreate(['field_text' => '音源制作']);
        CertifiedField::firstOrCreate(['field_text' => '翻訳']);
        CertifiedField::firstOrCreate(['field_text' => 'インストラクター・講師']);
    }

    public function populateTagGroup()
    {
        TagGroup::firstOrCreate(['tag_group_text' => 'プロデュース・ディレクション']);
        TagGroup::firstOrCreate(['tag_group_text' => 'デザイン制作']);
        TagGroup::firstOrCreate(['tag_group_text' => 'コーディング・プログラミング']);
        TagGroup::firstOrCreate(['tag_group_text' => 'モバイル']);
        TagGroup::firstOrCreate(['tag_group_text' => 'Flash']);
        TagGroup::firstOrCreate(['tag_group_text' => 'マーケティング']);
        TagGroup::firstOrCreate(['tag_group_text' => 'その他']);
    }

    public function populateTag()
    {
        Tag::firstOrCreate(['tag_group_id' => 1, 'tag_text' => '顧客折衝']);
        Tag::firstOrCreate(['tag_group_id' => 1, 'tag_text' => 'サイト案件ディレクション']);
        Tag::firstOrCreate(['tag_group_id' => 1, 'tag_text' => 'スマホコンテンツディレクション']);
        Tag::firstOrCreate(['tag_group_id' => 1, 'tag_text' => 'DB・ｿｰｼｬﾙﾒﾃﾞｨｱ連動案件ディレクション']);
        Tag::firstOrCreate(['tag_group_id' => 1, 'tag_text' => '企画・提案']);
        Tag::firstOrCreate(['tag_group_id' => 1, 'tag_text' => 'モバイルコンテンツディレクション']);
        Tag::firstOrCreate(['tag_group_id' => 1, 'tag_text' => 'DB・ｼｽﾃﾑ連動ｻｲﾄ案件ディレクション']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => '画像加工（ﾘｻｲｽﾞ/色調補正）']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'モバイルコンテンツデザイン']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'HTMLメルマガ制作']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'ドット絵']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'TOP・Secondﾍﾟｰｼﾞ・ﾃﾝﾌﾟﾚｰﾄﾃﾞｻﾞｲﾝ']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'バナー作成']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => '画像加工（トリミング）']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'スマホ対応コンテンツデザイン']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'アイコン作成']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'イラスト作成']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'スマホアプリ用デザイン']);
        Tag::firstOrCreate(['tag_group_id' => 2, 'tag_text' => 'キャラクターデザイン']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'テキスト入力・流し込み']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => '更新作業']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => '仕様書に沿ったコーディング']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'モバイル対応コーディング']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'WP構築（ｲﾝｽﾄｰﾙ・ﾌﾟﾗｸﾞｲﾝ導入・ ﾃﾝﾌﾟﾚ作成）']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => '他CMS構築（ｲﾝｽﾄｰﾙ・ﾌﾟﾗｸﾞｲﾝ導入・ ﾃﾝﾌﾟﾚ作成）']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'CMS入力']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'ｱｸｾｼﾋﾞﾘﾃｨ･ﾕｰｻﾋﾞﾘﾃｨを意識した ｺｰﾃﾞｨﾝｸﾞ']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'テンプレート作成']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'スマホ対応コーディング']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'MT構築（ｲﾝｽﾄｰﾙ・ﾌﾟﾗｸﾞｲﾝ導入・ ﾃﾝﾌﾟﾚ作成）']);
        Tag::firstOrCreate(['tag_group_id' => 3, 'tag_text' => 'FBページ制作']);
        Tag::firstOrCreate(['tag_group_id' => 4, 'tag_text' => 'アプリ企画・設計']);
        Tag::firstOrCreate(['tag_group_id' => 4, 'tag_text' => 'アプリ開発']);
        Tag::firstOrCreate(['tag_group_id' => 4, 'tag_text' => 'モバイルコンテンツ運用']);
        Tag::firstOrCreate(['tag_group_id' => 4, 'tag_text' => 'スマホコンテンツ運用']);
        Tag::firstOrCreate(['tag_group_id' => 4, 'tag_text' => 'マルチキャリア対応']);
        Tag::firstOrCreate(['tag_group_id' => 4, 'tag_text' => 'テスト・検証']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'Flashバナー制作']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'Javascriptとの連携']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'eラーニング制作']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => '3Dアニメーション']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'アイキャッチ作成']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'インタラクティブコンテンツ制作']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'シナリオ作成']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'ASを利用してのアニメーション制御']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'AllFlashサイト制作']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => '映像配信']);
        Tag::firstOrCreate(['tag_group_id' => 5, 'tag_text' => 'CGI、PHPとの連携']);
        Tag::firstOrCreate(['tag_group_id' => 6, 'tag_text' => 'ライティング（メルマガ）']);
        Tag::firstOrCreate(['tag_group_id' => 6, 'tag_text' => 'ライティング（WEB以外）']);
        Tag::firstOrCreate(['tag_group_id' => 6, 'tag_text' => 'ライティング（WEBコンテンツ）']);
        Tag::firstOrCreate(['tag_group_id' => 6, 'tag_text' => '編集（WEB以外）']);
        Tag::firstOrCreate(['tag_group_id' => 6, 'tag_text' => '編集（WEBコンテンツ）']);
        Tag::firstOrCreate(['tag_group_id' => 6, 'tag_text' => 'ソーシャルメディア運営']);
        Tag::firstOrCreate(['tag_group_id' => 7, 'tag_text' => '動画編集']);
        Tag::firstOrCreate(['tag_group_id' => 7, 'tag_text' => '事務作業・入力作業']);
        Tag::firstOrCreate(['tag_group_id' => 7, 'tag_text' => '写真撮影']);
        Tag::firstOrCreate(['tag_group_id' => 7, 'tag_text' => '音源制作']);
        Tag::firstOrCreate(['tag_group_id' => 7, 'tag_text' => '翻訳']);
        Tag::firstOrCreate(['tag_group_id' => 7, 'tag_text' => 'インストラクター・講師']);
    }
}
