<?php

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 200;

        for ($i = 1; $i <= $limit; $i++) {
            $status     = $faker->numberBetween(1, 3);
            $user       = $faker->numberBetween(1, 50);
            $teacher    = $faker->numberBetween(1, 20);
            $multiplier = $faker->numberBetween(1, 10);
            while (DB::table('teacher')->where('teacher_id', $teacher)->first()->user_id == $user) {
                $teacher = $faker->numberBetween(1, 20);
            }
            if ($status == 1) {
                Post::create([
                    'user_id'        => $user,
                    'post_status_id' => $status,
                    'title'          => $faker->realText(32),
                    'body'           => $faker->realText(128),
                    'created_at'     => $faker->dateTimeThisMonth->format('Y-m-d H:i:s'),
                    'updated_at'     => $faker->dateTimeBetween('-1 day', 'now'),
                ]);
            } else if ($status == 2) {
                Post::create([
                    'user_id'        => $user,
                    'teacher_id'     => $teacher,
                    'post_status_id' => $status,
                    'title'          => $faker->realText(32),
                    'body'           => $faker->realText(128),
                    'price'          => 10 * $multiplier,
                    'created_at'     => $faker->dateTimeThisMonth->format('Y-m-d H:i:s'),
                    'updated_at'     => $faker->dateTimeBetween('-1 day', 'now'),
                ]);
            } else {
                Post::create([
                    'user_id'        => $user,
                    'teacher_id'     => $teacher,
                    'post_status_id' => $status,
                    'title'          => $faker->realText(32),
                    'body'           => $faker->realText(128),
                    'solution'       => $faker->realText(512),
                    'price'          => 10 * $multiplier,
                    'created_at'     => $faker->dateTimeThisMonth->format('Y-m-d H:i:s'),
                    'updated_at'     => $faker->dateTimeBetween('-1 day', 'now'),
                ]);
            }
        }
    }
}
