<?php

use App\Models\Transaction;
use Illuminate\Database\Seeder;

class TransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 50;

        for ($i = 1; $i <= $limit; $i++) {
            $point_multiplier = $faker->numberBetween(5, 10);
            $previous_balance = DB::table('wallet')->where('wallet_id', $i)->first()->points;
            DB::table('wallet')->where('wallet_id', $i)->update(array('points' => $previous_balance + 100 * $point_multiplier));
            $new_balance = DB::table('wallet')->where('wallet_id', $i)->first()->points;
            Transaction::create([
                'wallet_id'             => $i,
                'transaction_type_id'   => 1,
                'transaction_status_id' => 2,
                'amount'                => 10.00 * $point_multiplier,
                'points'                => 100 * $point_multiplier,
                'new_wallet_balance'    => $new_balance,
                'created_at'            => $faker->dateTimeBetween('-2 month', '-1 month'),
                'updated_at'            => $faker->dateTimeBetween('-1 month', '-1 week'),
            ]);
        }
    }
}
