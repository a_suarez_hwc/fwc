<?php

use App\Models\Contact;
use Illuminate\Database\Seeder;

class ContactTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contact')->delete();

        $faker = Faker\Factory::create();
        $limit = 50;

        for ($i = 1; $i <= $limit; $i++) {
            $login_credential = DB::table('login_credential')->where('user_id', $i)->first();
            $email            = $login_credential->email;
            Contact::create([
                'user_id'      => $i,
                'email'        => $email,
                'mobile_email' => $faker->unique()->email,
                'phone_number' => $faker->phoneNumber,
                'skype'        => $faker->userName,
            ]);
        }
    }
}
