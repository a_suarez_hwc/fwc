<?php

use Illuminate\Database\Seeder;

class ExchangeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 200;

        for ($i = 1; $i <= $limit; $i++) {
            $post        = DB::table('post')->where('post_id', $i)->first();
            $post_status = $post->post_status_id;

            if ($post_status == 3) {
                $post_id = $post->post_id;

                $user        = $post->user_id;
                $user_wallet = DB::table('wallet')
                    ->where('user_id', $user)
                    ->first()
                    ->wallet_id;

                $teacher = $post->teacher_id;
                $tid     = DB::table('teacher')
                    ->where('teacher_id', $teacher)
                    ->first()
                    ->user_id;
                $teacher_wallet = DB::table('wallet')
                    ->where('user_id', $tid)
                    ->first()
                    ->wallet_id;

                $previous_user_balance = DB::table('wallet')
                    ->where('wallet_id', $user_wallet)
                    ->first()
                    ->points;
                DB::table('wallet')
                    ->where('wallet_id', $user_wallet)
                    ->update(array('points' => $previous_user_balance - $post->price));

                $teacher_percentage       = 0.9;
                $previous_teacher_balance = DB::table('wallet')
                    ->where('wallet_id', $teacher_wallet)
                    ->first()
                    ->points;

                DB::table('wallet')
                    ->where('wallet_id', $teacher_wallet)
                    ->update(array('points' => $previous_teacher_balance + $post->price * $teacher_percentage));

                App\Models\Exchange::create([
                    'paying_wallet_id'             => $user_wallet,
                    'receiving_wallet_id'          => $teacher_wallet,
                    'post_id'                      => $post_id,
                    'points_paid'                  => $post->price,
                    'new_paying_wallet_balance'    => DB::table('wallet')->where('user_id', $user)->first()->points,
                    'points_received'              => $post->price * $teacher_percentage,
                    'new_receiving_wallet_balance' => DB::table('wallet')->where('user_id', $tid)->first()->points,
                ]);
            }
        }

    }
}
