<?php

use App\Models\LoginCredential;
use Illuminate\Database\Seeder;

class LoginCredentialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 50;

        for ($i = 1; $i <= $limit; $i++) {
            LoginCredential::create([
                'user_id'          => $i,
                'email'      => $faker->unique()->email,
                'password'         => Hash::make($faker->randomLetter),
                'status'           => 'ok',
                'reset_token_used' => 0,
                'last_logged_in'   => $faker->dateTimeBetween('-1 day', 'now'),
            ]);
        }
    }
}
