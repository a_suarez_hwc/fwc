<?php

use App\Models\Contact;
use App\Models\LoginCredential;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(IndexTablesSeeder::class);

        $this->call(UserTableSeeder::class);
        $this->call(LoginCredentialTableSeeder::class);
        $this->call(ContactTableSeeder::class);

        $this->call(TeacherTableSeeder::class);
        $this->call(SubscriptionTableSeeder::class);

        $this->call(PostTableSeeder::class);
        $this->call(WalletTableSeeder::class);
        $this->call(TransactionTableSeeder::class);
        $this->call(ExchangeTableSeeder::class);
        $this->call(ReviewTableSeeder::class);

        $this->call(TeacherToCertifiedFieldTableSeeder::class);
        $this->call(PostToTagTableSeeder::class);
        $this->call(SubscriptionToTagTableSeeder::class);
        $this->command->info('DB seeded!');

        $this->assignEmails();
        $this->command->info('user@fwc.com and teacher@fwc.com created');
    }

    public function assignEmails()
    {
        $uid = 1;
        while (DB::table('teacher')->where('user_id', $uid)->exists()) {
            $uid++;
        }
        DB::table('login_credential')->where('user_id', $uid)->update(array('email' => 'user@fwc.com'));
        DB::table('login_credential')->where('user_id', $uid)->update(array('password' => Hash::make('user')));

        $tid = 1;
        if (DB::table('teacher')->where('teacher_id', $tid)->first()->user_id == $uid) {
            $tid++;
        }
        $tuid = DB::table('teacher')->where('teacher_id', $tid)->first()->user_id;
        DB::table('login_credential')->where('user_id', $tuid)->update(array('email' => 'teacher@fwc.com'));
        DB::table('login_credential')->where('user_id', $tuid)->update(array('password' => Hash::make('teacher')));

        DB::table('contact')->where('user_id', $uid)->update(array('email' => 'a_suarez@hwc.jp'));
        DB::table('contact')->where('user_id', $tuid)->update(array('email' => 'aaosuarez@gmail.com'));

        DB::table('user')->where('user_id', $uid)->update(array('family_name_kanji' => 'Suarez '));
        DB::table('user')->where('user_id', $tuid)->update(array('family_name_kanji' => 'Reyes '));

        DB::table('user')->where('user_id', $uid)->update(array('first_name_kanji' => 'Aaron'));
        DB::table('user')->where('user_id', $tuid)->update(array('first_name_kanji' => 'Will'));

        DB::table('user')->where('user_id', $uid)->update(array('family_name_kana' => 'スアレス '));
        DB::table('user')->where('user_id', $tuid)->update(array('family_name_kana' => 'レイエス '));

        DB::table('user')->where('user_id', $uid)->update(array('first_name_kana' => 'アーロン'));
        DB::table('user')->where('user_id', $tuid)->update(array('first_name_kana' => 'ウィル'));

        $faker = Faker\Factory::create();
        $admin = User::create([
            'family_name_kanji' => 'Admin',
            'first_name_kanji'  => '',
            'family_name_kana'  => 'Admin',
            'first_name_kana'   => '',
        ]);

        Contact::create([
            'user_id'      => $admin->user_id,
            'email'        => 'admin@hwc.jp',
            'mobile_email' => $faker->unique()->email,
            'phone_number' => $faker->phoneNumber,
            'skype'        => $faker->userName,
        ]);

        LoginCredential::create([
            'user_id'          => $admin->user_id,
            'email'            => 'admin@hwc.jp',
            'password'         => Hash::make('admin'),
            'status'           => 'ok',
            'reset_token_used' => 0,
            'last_logged_in'   => $faker->dateTimeBetween('-1 day', 'now'),
            'is_admin'         => true,
        ]);

        Wallet::create([
            'user_id' => $admin->user_id,
            'points'  => 0,
        ]);
    }
}
